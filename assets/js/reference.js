var array_definition;
var array_links;
var array_last_links_name;
var length_array;
var numbers_of_result = [];
$(function() {

    //--------------------------------------------------------------------Список
    $(".ul-dropfree").find("li:has(ul)").prepend('<div class="drop"></div>');
    $(".ul-dropfree div.drop").click(function() {
        if ($(this).nextAll("ul").css('display') === 'none') {
            $(this).nextAll("ul").slideDown(400);
            $(this).css({'background-position': "-11px 0"});
        } else {
            $(this).nextAll("ul").slideUp(400);
            $(this).css({'background-position': "0 0"});
        }
    });
    $(".ul-dropfree").find("ul").slideUp(400).parents("li").children("div.drop").css({'background-position': "0 0"});

    //------------------------------------------------------Появление объяснения


    array_links = $("#all_links > li");
    var n = array_links.length;
    for (var i = 0; i < n; i++) {
        //console.log(array_links[i].innerText);
    }
    array_definition = $("#all_links span");
    for (i = 0; i < array_definition.length; i++) {
        //console.log(array_definition[i].innerText);
    }

    $('#all_links > li').click(function() {
        $('.marked').css("color", "#555");
        var i = $('#all_links > li').index(this);
        $(this).css("color", "#76EE00");
        $(this).addClass('marked');
        $('#reference_explanation').html(array_definition[i]);
    });

//-------------------------------------------------------------------------Поиск
    array_last_links_name = $("#list_directory span");
    //var array_list = [];
    length_array = array_last_links_name.length;
    for (var j = 0; j < length_array; j++) {
        //array_list[j] = array_last_links_name[j].innerText;

        //  console.log(array_last_links_name[j].innerText);
        //  console.log(j);
    }

    $('#btn_searching').click(function() {
        var array_result = [];

        var what_looking = "";
        $('#list_directory').children().css("color", "#555");
        console.log($('#list_directory').children());

        $("#all_links > li > span").each(function(i, val) {
            $(this).css("color", "#555");
            $(this).parents().css("color", "#555");
        });

        $('#searching_results').html('');
        what_looking = $('#searching_string').val();
        console.log(what_looking);
        if (what_looking === '') {
            //var what_looking = prompt('Вы ничего не ввели в поле поиска. Пожалуйста, введите необходимую информацию в поле ниже.', '');
            //$('#btn_searching').unbind(click());
            $('#list_directory').css("color", "#555");
            $('.header_list').children().css("color", "#555");
            return 0;
        }

        var regex = RegExp(what_looking);

        //alert(regex);

        var res;
        for (var k = 0; k < length_array; k++) {
            res = array_last_links_name[k].innerText.match(regex);
            //alert(array_list[k]);
            //if (res !== -1) 
            array_result[k] = res;
            //console.log(array_result[k]);
            //var str = array_result[k].input;
            //str.replace(regex, "<p style='color:red'>"+what_looking+"</p>");
            //array_result[k] = str;
        }
        var ind = 0;
        for (k = 0; k < length_array; k++) {
            if (array_result[k] !== null)
            {
                //console.log('Find null');
                numbers_of_result[ind] = k;
                ind++;
                //console.log(k);

                $('#searching_results').html($('#searching_results').html() + "</p>" + array_result[k] + ":  " + array_result[k].input + ";  " + array_result[k].index + "</p>");
                //$('#searching_results').html($('#searching_results').html() + "</p>" + array_result[k]+ "</p>");
            }
        }

        $('#searching_string').val(what_looking);

        $("#all_links > li > span").each(function(i, val) {
            for (var j = 0; j < numbers_of_result.length; j++) {
                if (i === numbers_of_result[j]) {
                    //$(this).css("color", "red");
                    //alert($(this).text());
                    var par1 = $(this).parent();
                    par1.css("color", "blue");
                    var par2 = par1.parent();
                    var par3 = par2.parent();
                    par3.css("color", "blue");
                    par2.css("color", "#555");
                    var par4 = par3.parent();
                    par4.parent().css("color", "blue");
                    par4.css("color", "#555");

                    var parent1 = $(this).parent('li');
                    parent1.css("color", "blue");
                    parent1.parents().css("color", "blue");
                    //$(".header_list").css("color", "blue");

                }
            }
        });
    });
});
<?php
header('Content-type: application/xml; charset=utf-8');

function levenstein_dist ($S1, $S2){                     //--------------------------для кусочка второго слова, по длине, совпадающей с длиной первого--------------------------------------------------------------------------------------
    
    
    if ($S1 == $S2) return 0;

    $S1_length = strlen($S1)/2;
    $S2_length = strlen($S2)/2;
    
    if (!$S1_length) return $S2_length;
    else if (!$S2_length) return $S2_length;
        
    $D = array ();
    $D[0] = array();
    
    for ($i = 0; $i <= $S1_length; ++$i){                                //0-я строка и столбец
        $D[0][$i] = $i;
        $D[$i] = array();
        $D[$i][0] = $i;
    }
    

    for ($i = 1; $i <= $S1_length; ++$i){
        for ($j = 1; $j <= $S1_length; ++$j){
            $mass1 = $S1[($i-1)*2].$S1[($i-1)*2+1];
            $mass2 = $S2[($j-1)*2].$S2[($j-1)*2+1];
            if ($mass1 == $mass2) $D[$i][$j] = $D[$i-1][$j-1];      //при совпадении символов записываем левый верхний
            else{
                $a = $D[$i-1][$j-1] + 1;    // замена
                $b = $D[$i][$j-1] + 1;      // вставка
                $c = $D[$i-1][$j] + 1;      // удаление
                
                $D[$i][$j] = min($a, $b, $c);
            }
        }
    }
    return $D[$S1_length][$S1_length];
}
    
$result_array =  array();
$count = 0;


$dbh = mysql_connect('localhost', 'root', '') or die("Не могу соединиться с MySQL.");
mysql_select_db('interpreter') or die("Не могу подключиться к базе.");
$query1 = "SELECT * FROM  `language` ";
$res1 = mysql_query($query1);
$ex_var1 = $_POST["value"];      //---------------------------------------для команд

$query2 = "SELECT * FROM  `sp_variable` ";
$res2 = mysql_query($query2);      //---------------------------------------для переменных

$list_box = '';

while($row = mysql_fetch_assoc($res1)){
    if(((strlen($ex_var1)/2 == 3) && (levenstein_dist($ex_var1, $row['name']) <= 1))||((strlen($ex_var1)/2 > 3) && (levenstein_dist($ex_var1, $row['name']) <= strlen($ex_var1)*0.15))){
        $list_box = $list_box.'<li>'.$row['name'].'</li>'; 
    }
    
}
while($row1 = mysql_fetch_assoc($res2)){
    if(((strlen($ex_var1)/2 == 3) && (levenstein_dist($ex_var1, $row1['name']) <= 1))||((strlen($ex_var1)/2 > 3) && (levenstein_dist($ex_var1, $row1['name']) <= strlen($ex_var1)*0.15))){
        $list_box = $list_box.'<li>'.$row1['name'].'</li>'; 
    }
    
}
echo $list_box;
?>
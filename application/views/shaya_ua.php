<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>DAR</title>
        <link href="<?= base_url() ?>assets/css/main.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/bootstrap/css/bootstrap.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/bootstrap/css/bootstrap-theme.css" rel="stylesheet">
        <script type="text/javascript" src="<?= base_url() ?>assets/js/jquery-2.0.3.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/main.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/interpreter_ua.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/maybe.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/bootstrap/js/bootstrap.js"></script>
    </head>
    <body class="style-page">
        <div id="container">
            <div class="row">
                <div class="content">
                    <div class="navbar navbar-default">
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav">
                                <a class="navbar-brand" href="<?= base_url() ?>main_ua">ДАР</a>
                                <li><a href="<?= base_url() ?>shaya">Інтерпретатор</a></li>
                                <li><a href="#">Навчання</a></li>
                                <li><a href="#">Колекція прикладів</a></li>
                                <li><a href="#">Виконавці</a></li>
                                <li><a href="<?= base_url() ?>reference">Довідник</a></li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Обрати мову <b class="caret"></b></a>
                                    <ul class="dropdown-menu">    
                                        <a href="<?= base_url() ?>shaya"><img src="<?= base_url() ?>assets/img/Russia.png" width="30" height="30" ></a>
                                        <a href="<?= base_url() ?>shaya_ua"><img src="<?= base_url() ?>assets/img/Ukraine.png" width="30" height="30"></a>      
                                    </ul>
                                </li>
                            </ul>  
                        </div>
                    </div>
                </div>
            </div>
            <div class="content_height">
                <div class="row">
                    <div class="content">
                        <div class="well-smt"></div>                   
                    </div>
                    <div class="content">
                        <div class="form_right_angle ">
                            <span class="glyphicon glyphicon-play"></span>
                            <span class="glyphicon glyphicon-stop"></span>
                            <span class="glyphicon glyphicon-floppy-disk"></span>
                        </div>
                    </div>
                    <div class="content">
                        <div>
                            <div class="column_1">
                                <div class="shell_number">
                                    <div class="form_right_angle number_bar" id="number_bar" contenteditable="false">
                                        <span class="number_style">1</span>                              
                                    </div> 
                                </div>
                                <div class="shell_inter">
                                    <div class="form_right_angle interpreter" id="interpreter" contenteditable="true" spellcheck="false"></div>   

                                </div>
                            </div>
                            <div class="column_2">
                                <div class="form_right_angle windows" contenteditable="true" spellcheck="false"></div>
                                <div class="form_right_angle windows" id='id_error'></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="content">
                        <div class="row_end form_right_angle"></div>               
                    </div>
                </div>
                <div class="row">
                    <div class="content">
                        <div class="well-smb"></div> 
                    </div>
                </div>
                <div class="image"><img src="<?= base_url() ?>assets/img/Robot_Inverse.png"></div>
            </div>
            <div class="row">
                <div class="content">
                    <div class="navbar navbar-default">
                    </div>
                </div>
            </div>
    </body>
</html>

var language = {
    typeVariable: ["цiл", "реч", "лог", "лiт", "сим"],
    structure: ["алг", "поч", "кiн", "використовувати"],
    executor: ["Робот", "Кресляр"],
    parameters: ["арг", "рез", "аргрез"],
    variableName: function(str) {
        return /^[а-яА-Яa-zA-Z][А-Яа-яA-Za-z0-9_]*/.test(str);
    },
    number: function(numb) {
        var result = /[0-9]*\.?[0-9]+/.test(numb);
        if (result === true) {
            if (/^0[0-9]{1,}/.test(numb) === true) {
                return false;
            }
            return true;
        } else {
            return false;
        }
    }
};


var alfabet = ["\n", "алг", "використовувати", "Робот", "Кресляр", "цiл", "реч", "лог", "сим", "лiт", ":=", ";", ",", "(", ")", "арг", "рез", "аргрез", "поч", "кiн", '"', "_|"];
var symbol = ["#S1", "#S0", "#S1_", "#P1", "#P1_", "#B", "#P2", "#P2_", "#T", "#A", "#C", "#C_", "#P3", "#P3_", "#D", "#D_", "#E", "#E_", "#G1", "#G2", "#G3", "#G4", "#I", "#I_", "#L", "#L_", "#N", "#O", "#O_", "#J", "#P", "#P_", "#Q", "#R", "#R_", "#R1_", "#U_", "#U"];

var automatic = [
    [1, 2, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0, 0, 0],
    [5, 6, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 8, 9, 0, 0, 32, 32, 32, 32, 32, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [11, 12, 12, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 12, 0, 0, 0],
    [0, 0, 0, 13, 14, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [15, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [16, 18, 0, 0, 0, 17, 17, 17, 17, 17, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 19, 20, 21, 22, 23, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 24, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 25, 25, 25, 25, 25, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [27, 0, 0, 0, 0, 26, 26, 26, 26, 26, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [28, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [30, 31, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 29, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 33, 0, 0],
    [35, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 34, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 36, 0, 0],
    [38, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 37, 39, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 40, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 41, 0, 0, 0, 42],
    [46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 45, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 44, 0, 0, 43, 0],
    [47, 0, 0, 0, 0, 49, 49, 49, 49, 49, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 48, 0, 0],
    [50, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 51, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 53, 53, 53, 53, 53, 0, 0, 0, 0, 0, 52, 52, 52, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 54, 54, 54, 54, 54, 0, 0, 0, 0, 55, 54, 54, 54, 0, 0, 0, 55, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 56, 57, 58, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 59, 0, 0],
    [0, 0, 0, 0, 0, 61, 61, 61, 61, 61, 0, 0, 60, 0, 0, 61, 61, 61, 0, 0, 0, 61, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 62, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 63, 0, 0, 0, 0, 0, 0],
    [64, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 65, 0, 0, 0, 0, 0, 0, 0, 0, 0, 75, 0, 75, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 77, 0, 77, 66, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 67, 0, 0, 0, 0, 0],
    [68, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0],
    [70, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 71, 0, 0, 0],
    [72, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 73, 0, 0, 0],
    [74, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
];

var stack;

function parse(code1) {
    console.log(code1);
    $("#id_error").html('');
    var code = code1;
    
    stack = [];
    var numberRule,
            admission,
            result,
            number_str = 1,
            indexOfSymbol = 0,
            indexOfAlfabet,
            i = 0,
            test_text,
            error_text = "";

    for (var j = 0; j < code.length; ++j)
        code[j].push("\n");

    test_text = "нач";

    numberRule = 0;
    for (var j = 0; j < alfabet.length; ++j)
    {
        if (code[1][0] === alfabet[j])              //траблы с 0-й строкой, начинаем с первой
        {
            numberRule = automatic[number_str][j];
            result = pravilo(numberRule);
            break;
        }
    }

    if (stack.length === 0)
    {
        result = 2;
    }

    switch (result) {

        case 0:
            admission = 0;
        case 2:
            admission = 1;
        case 1:
            {
                for (; number_str < code.length; ++number_str) {
                    for (i = 0; i < code[number_str].length; ) {
                        if((code[number_str][i] === "") ||(code[number_str][i] === " ")){
                            i++;
                        }

                        if (code[number_str][i] === stack[stack.length - 1]) {
                            numberRule = 76;
                            result = pravilo(numberRule);
                            if (result === 2)
                                admission = 1;
                            ++i;
                        }

                        else {
                            for (j = 0; j < symbol.length; ++j) {
                                if (stack[stack.length - 1] === symbol[j]) {
                                    indexOfSymbol = j;
                                    break;
                                }
                            }

                            for (j = 0; j < alfabet.length; ++j) {
                                if (code[number_str][i] === alfabet[j]) {
                                    indexOfAlfabet = j;
                                    break;
                                }
                                if (j === (alfabet.length - 1)) {
                                    if (language.variableName(code[number_str][i])) {
                                        indexOfAlfabet = alfabet.length;
                                        code[number_str][i] = "#SMBL";
                                       // console.log("слово");
                                    }
                                    else if (language.number(code[number_str][i])) {
                                        indexOfAlfabet = alfabet.length + 1;
                                        code[number_str][i] = "#NMBR";
                                       // console.log("число");
                                    }
                                    else {
                                        indexOfAlfabet = alfabet.length + 2;
                                        code[number_str][i] = "#SMTHNG";
                                       // console.log("какая-то хрень");
                                    }
                                }

                            }
                            numberRule = automatic[indexOfSymbol][indexOfAlfabet];
                            test_text = test_text + "->" + numberRule;
                            result = pravilo(numberRule);
                        }
                        if (result === 0) {
                            admission = 0;
                            break;
                        }
                        if (result === 2) {
                            admission = 1;
                            break;
                        }
                    }

                    if (admission === 0) {
                        $("#id_error").html('помилочка у строчцi ' + number_str);//error_text = error_text + 'упс.ошибка синтаксиса в строке ' +'<br>';//
                        break;
                    }
                    if (admission === 1) {
                        console.log("ура");
                        $("#id_error").html('');
                        break;
                    }
                    if (number_str === code.length - 1) {

                        if (stack.length === 1) {

                                    if((stack[0] === "#R_")||(stack[0] === "#R") ||(stack[0] === "#R1_")||(stack[0] === "#U")||(stack[0] === "#U_")) {
                                        stack.pop();
                                    admission = 1;
                                    console.log("ура");
                                    $("#id_error").html('Вітаємо! Ти створив свій власний алгоритм :)');
                                
                            }

                        }
                        else
                            $("#id_error").html('помилочка у строчцi ' + number_str);

                    }
                }
            }
    }

    console.log(stack);

}

function pravilo(number) {
    var result = 1;
    if (number === 0) {
        result = 0;            // состояние ошибка!
    }
    if (number === 1) {
        stack.push("#S1_");
        stack.push("#P1");
    }
    if (number === 2) {
        stack.push("#J");
        stack.push("#I");
        stack.push("алг");
    }
    if (number === 3) {
        stack.push("#B");
        stack.push("використовувати");
    }
    if (number === 4) {
        result = 1;
    }
    if (number === 5) {
        stack.pop();
        stack.push("#S1_");
        stack.push("#P1");
    }
    if (number === 6) {
        stack.pop();
        stack.push("#J");
        stack.push("#I");
        stack.push("алг");
    }
    if (number === 7) {
        stack.pop();
        stack.push("#B");
        stack.push("використовувати");
    }
    if (number === 8) {
        stack.pop();
        stack.push("#J");
        stack.push("#I");
    }
    if (number === 9) {
        stack.pop();
        stack.push("#B");
        stack.push("використовувати");
    }
    if (number === 10) {
        stack.pop();
        stack.push("#P1_");
        stack.push("\n");
    }
    if (number === 11) {
        stack.pop();
        stack.push("#P1");
    }
    if (number === 12) {
        stack.pop();
    }
    if (number === 13) {
        stack.pop();
        stack.push("#P2");
        stack.push("Робот");
    }
    if (number === 14) {
        stack.pop();
        stack.push("#P2");
        stack.push("Кресляр");
    }
    if (number === 15) {
        stack.pop();
        stack.push("#P2_");
        stack.push("\n");
    }
    if (number === 16) {
        stack.pop();
        stack.push("#P2");

    }
    if (number === 17) {
        stack.pop();
        stack.push("#C");
    }
    if (number === 18) {
        stack.pop();
        stack.push("#A");
    }
    if (number === 19) {
        stack.pop();
        stack.push("ціл");
    }
    if (number === 20) {
        stack.pop();
        stack.push("реч");
    }
    if (number === 21) {
        stack.pop();
        stack.push("лог");
    }
    if (number === 22) {
        stack.pop();
        stack.push("сим");
    }
    if (number === 23) {
        stack.pop();
        stack.push("літ");
    }
    if (number === 24) {
        stack.pop();
        stack.push("#J");
        stack.push("#I");
        stack.push("алг");
    }
    if (number === 25) {
        stack.pop();
        stack.push("#C_");
        stack.push("#E");
        stack.push("#T");
    }
    if (number === 26) {
        stack.pop();
        stack.push("#C");
    }
    if (number === 27) {
        stack.pop();
        stack.push("#P3");
    }
    if (number === 28) {
        stack.pop();
        stack.push("#P3_");
        stack.push("\n");
    }
    if (number === 29) {
        stack.pop();
        stack.push("#D");
    }
    if (number === 30) {
        stack.pop();
        stack.push("#P3");
    }
    if (number === 31) {
        stack.pop();
        stack.push("#A");
    }
    if (number === 32) {
        stack.pop();
        stack.push("#C");
    }
    if (number === 33) {
        stack.pop();
        stack.push("#D_");
        stack.push("#G1");
        stack.push("#SMBL");
    }
    if (number === 34) {
        stack.pop();
        stack.push("#D");
    }
    if (number === 35) {
        stack.pop();
        stack.push("#A");
        stack.push("/n");
    }
    if (number === 36) {
        stack.pop();
        stack.push("#E_");
        stack.push("#SMBL");
    }
    if (number === 37) {
        stack.pop();
        stack.push(";");
    }
    if (number === 38) {
        stack.pop();
        stack.push("\n");
    }
    if (number === 39) {
        stack.pop();
        stack.push("#E");
        stack.push(",");
    }
    if (number === 40) {
        stack.pop();
        stack.push("#G2");
        stack.push(":=");
    }
    if (number === 41) {
        stack.pop();
        stack.push("#G4");
        stack.push('"');
    }
    if (number === 42) {
        stack.pop();
        stack.push('#G3');
        stack.push('#NMBR');
    }
    if (number === 43) {
        stack.pop();
        stack.push('#G4');
        stack.push('#SMTHNG');
    }
    if (number === 44) {
        stack.pop();
        stack.push('#G3');
        stack.push('"');
    }
    if (number === 45) {
        stack.pop();
        stack.push(';');
    }
    if (number === 46) {
        stack.pop();
        stack.push('\n');
    }
    if (number === 47) {
        stack.pop();
        stack.push('\n');
    }
    if (number === 48) {
        stack.pop();
        stack.push('\n');
        stack.push('#SMBL');
    }
    if (number === 49) {
        stack.pop();
        stack.push('#I_');
        stack.push('#SMBL');
        stack.push('#T');
    }
    if (number === 50) {
        stack.pop();
        stack.push('\n');
    }
    if (number === 51) {
        stack.pop();
        stack.push('\n');
        stack.push(')');
        stack.push('#L');
        stack.push('(');
    }
    if (number === 52) {
        stack.pop();
        stack.push('#L_');
        stack.push('#O');
        stack.push('#T');
        stack.push('#N');
    }
    if (number === 53) {
        stack.pop();
        stack.push('#L_');
        stack.push('#O');
        stack.push('#T');
    }
    if (number === 54) {
        stack.pop();
        stack.push('#L');
    }
    if (number === 55) {
        stack.pop();
    }
    if (number === 56) {
        stack.pop();
        stack.push('арг');
    }
    if (number === 57) {
        stack.pop();
        stack.push('рез');
    }
    if (number === 58) {
        stack.pop();
        stack.push('аргрез');
    }
    if (number === 59) {
        stack.pop();
        stack.push('#O_');
        stack.push('#SMBL');
    }
    if (number === 60) {
        stack.pop();
        stack.push('#O');
        stack.push(',');
    }
    if (number === 61) {
        stack.pop();
    }
    if (number === 62) {
        stack.pop();
        stack.push('#R');
        stack.push('#Q');
        stack.push('#P');
    }
    if (number === 63) {
        stack.pop();
        stack.push('#P_');
        stack.push('поч');
    }
    if (number === 64) {
        stack.pop();
        stack.push('\n');
    }
    if (number === 65) {
        stack.pop();
        stack.push(';');
    }
    if (number === 75) {
        stack.pop();
    }
    if (number === 66) {
        stack.pop();
        stack.push('\n');
        stack.push('#SMBL');
    }
    if (number === 67) {
        stack.pop();
        stack.push('#R_');
        stack.push('кін');
    }
    if (number === 68) {
        stack.pop();
        stack.push('#R1_');
        stack.push('\n');
    }
    if (number === 69) {
        stack.pop();
        result = 1;
    }
    if (number === 70) {
        stack.pop();
        stack.push('#U_');
    }
    if (number === 71) {
        stack.pop();
        result = 1;
    }
    if (number === 72) {
        stack.pop();
        stack.push('#U');
    }
    if (number === 73) {
        stack.pop();
        result = 1;
    }
    if (number === 74) {
        stack.pop();
        stack.push('#U_');
        stack.push('\n');
    }
    if (number === 76) {
        // правило для сдвига, в случае, когда входящий элемент совпадает с последним элементом стэка!
        if (stack[stack.length - 1] === '_|')
        {
            stack.pop();
            result = 2;
        }
        else
        {
            stack.pop();
        }
    }
    if (number === 77) {
        stack.pop();
    }
    return result;
}

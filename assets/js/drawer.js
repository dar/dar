$(function() {

    $("#drawer").on('click', function() {
        $('.column_2').css({'display': 'none'});
        $('.drawer').css({'display': 'block'});
        $('.robot').css({'display': 'none'});
        $('.robot-menu').css({'display':'none'});
        $('.image').css({'left': '85%'});

        var drawer = document.getElementById('drawer-pole');
        var context = drawer.getContext('2d');

        for (var x = 0.5; x < drawer.width; x += 5) {
            context.moveTo(x, 0);
            context.lineTo(x, drawer.height);
            if (x <= drawer.width / 2) {
                var sr_zn_x = x;
            }
        }
        for (var y = 0.5; y < drawer.height; y += 5) {
            context.moveTo(0, y);
            context.lineTo(drawer.width, y);
            if (y <= drawer.height / 2) {
                var sr_zn_y = y;
            }
        }
        context.strokeStyle = "#eee";
        context.stroke();
        context.beginPath();
        context.moveTo(sr_zn_x, 0);
        context.lineTo(sr_zn_x, drawer.height);
        context.moveTo(0, sr_zn_y);
        context.lineTo(drawer.width, sr_zn_y);
        context.strokeStyle = "black";
        context.stroke();
        var pencil = document.getElementById('drawer-pencil');
        context.drawImage(pencil, sr_zn_x, sr_zn_y-pencil.height);

    });
});




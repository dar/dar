$(function() {
    
        //--------------------------удаляем все переменные из БД-----------------
        $.ajax({
                url: '/shaya/variable_delete',
                method: 'get',
                dataType: 'json',
                success: function(answer) {
                        //обработчик на ответ сервера
                }
        });
    
    
    var is_debug = 0;
    var is_change = -1;
    var is_error = 0;
    var if_autocomplete = 0;
    
    
    //---------------------------на изменение окна----------------------------
    $(window).resize(function(){                      //-------на изменение окна-----------------------------
                     if($('.vars_list').length > 0 ) {
                            
    }
                     });


   /* $('body').on('click', '.class', function(){    //------------------------------------ATTENTION!!!!!     обработка событий при динамическом добавлении элементов----------------------------------
    })*/
    
    //Редактирование интерпретатора - событие при клике на редактор
    
    var globalCaretPos = 0;

    $('#interpreter').on('click', function() {

        if($('body').find('span[contenteditable="true"]').length > 0) {
            $('body').find('span[contenteditable="true"]').removeAttr('contenteditable');
            $(this).append('<textarea class="green" id="my_textarea"></textarea>');
        } else {
            $('body').find('.green').focus();
        }
        
    });
    
    //----------------------------------------------------------------------
    
    var mainCodeW2 = [];
    var str_number;
    var wrd_number;
    var goToCode = 0;
    mainCode =[];
    mainCode1 = [];

    $.fn.syntax = function() {               //подсветка синтаксиса
        return this.each(function() {
            var jqCode = $(this);
            var code = jqCode.text();
            code = code
            
            var vl = $("#i" + str_number + "j" + (wrd_number - 1)).html();
            
            $("#i" + str_number + "j" + (wrd_number - 1)).removeClass("ifelsewrd"); // удаляем все ключевые классы
            $("#i" + str_number + "j" + (wrd_number - 1)).removeClass("kwrd");
            $("#i" + str_number + "j" + (wrd_number - 1)).removeClass("vwrd");
            $("#i" + str_number + "j" + (wrd_number - 1)).removeClass("inoutwrd");
            $("#i" + str_number + "j" + (wrd_number - 1)).removeClass("exwrd");
            $("#i" + str_number + "j" + (wrd_number - 1)).removeClass("parwrd");
            $("#i" + str_number + "j" + (wrd_number - 1)).removeClass("forswrd");
            
            if(language.structure(vl.slice(" "))) //ключевые слова
                    $("#i" + str_number + "j" + (wrd_number - 1)).addClass("kwrd");

            else if(language.typeVariable(vl.slice(" "))) //переменные
                    $("#i" + str_number + "j" + (wrd_number - 1)).addClass("vwrd");

            else if(language.input_output(vl.slice(" "))) //ввод/вывод
                    $("#i" + str_number + "j" + (wrd_number - 1)).addClass("inoutwrd");

            else if(language.executor(vl.slice(" "))) //исполнители
                    $("#i" + str_number + "j" + (wrd_number - 1)).addClass("exwrd");

            else if(language.parameters(vl.slice(" "))) //параметры
                    $("#i" + str_number + "j" + (wrd_number - 1)).addClass("parwrd");
            else if(language.fors(vl.slice(" "))) //циклы
                    $("#i" + str_number + "j" + (wrd_number - 1)).addClass("forswrd");
            else if(language.ifelse(vl.slice(" "))) //условия
                    $("#i" + str_number + "j" + (wrd_number - 1)).addClass("ifelsewrd");


            jqCode.html(code);
        });
    }
    
/*
 * Функция - устанавливает позицию курсора в конец области
 * @param {type} el
 * @returns {undefined}
 */
    function placeCaretAtEnd(el) {
        el.focus();
        if (typeof window.getSelection != "undefined"
                && typeof document.createRange != "undefined") {
            var range = document.createRange();
            range.selectNodeContents(el);
            range.collapse(false);
            var sel = window.getSelection();
            sel.removeAllRanges();
            sel.addRange(range);
        } else if (typeof document.body.createTextRange != "undefined") {
            var textRange = document.body.createTextRange();
            textRange.moveToElementText(el);
            textRange.collapse(false);
            textRange.select();
        }
    }
    
    /*
     * Функция - возвращает позицию курсора в области
     * @param {type} element
     * @returns {Number}
     */
function getCaretPos(element) {
	element.focus();
	if (document.selection) {
		var sel = document.selection.createRange();
		var clone = sel.duplicate();
		sel.collapse(true);
		clone.moveToElementText(element);
		clone.setEndPoint('EndToEnd', sel);
		return clone.text.length;
	} else {
		return window.getSelection().getRangeAt(0).startOffset;
	}
	return 0;
}
    /*
    * Устанавливает фокус на элементе (textarea, conteneditable)
    * @param {type} div
    * @returns {undefined}
    */
function onfocus(div) {
    window.setTimeout(function() {
        var sel, range;
        if (window.getSelection && document.createRange) {
            range = document.createRange();
            range.selectNodeContents(div);
            range.collapse(true);
            sel = window.getSelection();
            sel.removeAllRanges();
            sel.addRange(range);
        } else if (document.body.createTextRange) {
            range = document.body.createTextRange();
            range.moveToElementText(div);
            range.collapse(true);
            range.select();
        }
    }, 1);
};
/*
 * Изменения id  для блоков содержащих слова
 * @param {type} words
 * @param {type} i
 * @returns {undefined}
 */
    function changeIdWord(words, i) {
        var j1 = 0;
        words.each(function(key, value) {
            $(value).attr('id', 'i' + i + 'j' + j1);
            j1++;
        });
        return;
    }
    
    
   /*
    * Изменение id для строк 
    * @param {type} lines
    * @returns {undefined}
    */
    function changeIdLine(lines) {
        var i1 = 0;
        lines.each(function(key, value) {
            $(value).attr('id', 'i' + i1);
            i1++;
        });
        return;
    }

    /*
     * изменения Id для тега переноса строки
     * @param {type} brs
     * @returns {undefined}
     */
    function changeIdBr(brs) {
        var i1 = 1;
        brs.each(function(key, value) {
            $(value).attr('id', 'b' + (i1));
            i1++;
        });
    }
    
    
    function deleteEmptyNextWord(elem) {
        var id = $(elem).attr('id').replace('i', '');
        id = id.replace('j', ' ');
        var arrij = id.split(' ');
        var i = parseInt(arrij[0]);
        var j = parseInt(arrij[1]);
        var nextWord = $('#i'+i+'j'+(j+1)).html();
        if($('#i'+i+'j'+(j+1)).length>0 && nextWord.replace(' ','') === '') {
            $('#i'+i+'j'+(j+1)).remove();
            mainCodeW2[i].splice(j, 1);
        }
    }
    
    
    //назначаем атрибут редактирования для тега содержащего слово
    $('body').on('click', '.word', function(event) {
            var prevSpan = $('body').find('span[contenteditable="true"]');
            if(prevSpan.length > 0) {
                $(prevSpan).removeAttr('contenteditable', 'true');
            }
            $('.green').remove();
            $(this).attr('contenteditable', 'true');
            $(this).attr('spellcheck', "false");
//            var id = $(this).attr('id');
//            document.getElementById(id).focus();
    });

    $('body').on('blur', '.word', function(event) {
        // $(this).removeAttr('contenteditable');
        // $('#interpreter').append('<textarea class="green" id="my_textarea"></textarea>');
        var id = $(this).attr('id').replace('i', '');
        id = id.replace('j', ' ');
        var arrij = id.split(' ');
        var i = parseInt(arrij[0]);
        var j = parseInt(arrij[1]);

        var line = $('body').find('.strings#' + i);
        if ($(this).html() === '') {
            $(this).remove();

            var words = $(line).find('span.word');
            changeIdWord(words, i);     //переписываем id слов
        }
        
       for (var k = 0; k < mainCodeW2[i][j]; k++) {
            if (mainCodeW2[i][j][k] === '') {
                mainCodeW2.splice(k, 1);
            }
        }
    });
    
    $('body').on('focus', '.word', function(){
        $('#interpreter').find('.green').remove();
        globalCaretPos = getCaretPos(this);
    });
    
    $('body').on('keyup', '.word', function(event)  {
        var id = $(this).attr('id').replace('i', '');
        id = id.replace('j', ' ');
        var arrij = id.split(' ');
        var i = parseInt(arrij[0]);
        var j = parseInt(arrij[1]);
        var word = $(this).html();
        var arrWord = word.split('');
        var line = $('body').find('.strings#' + i);
       var br = $('body').find('#b'+(i+1));
        if(event.keyCode !== 32 && event.keyCode !== 8) {
            mainCodeW2[i][j] === arrWord;
        }
        if(event.keyCode === 8) {
            if (getCaretPos(this) === 0 && word === '') {
                
                  if (j !== 0) {

                    mainCodeW2[i].splice(j, 1);
                    
                    
                    var words = $(line).find('span.word');
                    changeIdWord(words, i);     //переписываем id слов
                    
                    var beforeWord = document.getElementById('i' + i + 'j' + (j - 1));
                    $('.word#i' + i + 'j' + (j - 1)).attr('contenteditable', 'true').attr('spellcheck', "false");
                    placeCaretAtEnd(beforeWord);
                    deleteEmptyNextWord(this);
                    $(this).remove();
                    
                } else if (j === 0) {
                    
                    $(this).remove();
                    
                    var words = $(line).find('span.word');
                    changeIdWord(words, i);     //переписываем id слов
                    
                    if (i !== 0) {
                        var beforeLine = $('body').find('.strings#i' + (i - 1));

                        var lastWord = document.getElementById('i' + (i - 1)).lastElementChild;
                        $(beforeLine).find('.word:last').attr('contenteditable', 'true').attr('spellcheck', "false");
                        placeCaretAtEnd(lastWord);
                        
                    } else {
                        
                        var beforeLine = $('body').find('.strings#i' + (i));

                        var lastWord = document.getElementById('i' + (i)).firstElementChild;
                        var firstWord = $(beforeLine).find('.word:first').attr('contenteditable', 'true').attr('spellcheck', "false");
                        var elemID = $(firstWord).attr('id');
                        var elem = document.getElementById(elemID);
                        onfocus(elem);
                    }
                    
                    if ($(line).html() === '') {
                        
                        $(line).remove();
                        $(br).remove();
                        
                        var number_lines = $('.number_bar').find('.number_style:last');
                        $(number_lines).remove();

                        var lines = $('#interpreter').find('.strings');
                        
                        changeIdLine(lines);    //переписываем id строк
                        
                        var brs = $('#interpreter').find('#b' + (i + 1));
                        
                        changeIdBr(brs);
                    }
                    
                    mainCodeW2.splice(i, 1);
                }
            }
        }
    });
    
    
    
   //обработка ввода текста в span
    $('body').on('keydown', '.word', function(event) {
        
       var word = $(this).html();
       
       if(word === '') {
            var idElem = $(this).attr('id');
            document.getElementById(idElem).focus();
       }
       
       var arrWord = word.split('');
       var id = $(this).attr('id').replace('i', '');
       id = id.replace('j', ' ');
       var arrij = id.split(' ');
       var i = parseInt(arrij[0]);
       var j = parseInt(arrij[1]);
       
       var line = $('body').find('.strings#' + i);
       var br = $('body').find('#b'+(i+1));
       globalCaretPos = getCaretPos(this);
       
        if (event.keyCode === 8) {
            
            if (getCaretPos(this) === 0 && word === '') {
                
                if (j !== 0) {

                    mainCodeW2[i].splice(j, 1);
                    $(this).remove();
                    
                    var words = $(line).find('span.word');
                    changeIdWord(words, i);     //переписываем id слов
                    
                    var beforeWord = document.getElementById('i' + i + 'j' + (j - 1));
                    $('.word#i' + i + 'j' + (j - 1)).attr('contenteditable', 'true').attr('spellcheck', "false");
                    placeCaretAtEnd(beforeWord);
                    
                } else if (j === 0) {
                    
                    $(this).remove();
                    
                    var words = $(line).find('span.word');
                    changeIdWord(words, i);     //переписываем id слов
                    
                    if (i !== 0) {
                        var beforeLine = $('body').find('.strings#i' + (i - 1));

                        var lastWord = document.getElementById('i' + (i - 1)).lastElementChild;
                        $(beforeLine).find('.word:last').attr('contenteditable', 'true').attr('spellcheck', "false");
                        placeCaretAtEnd(lastWord);
                        
                    } else {
                        
                        var beforeLine = $('body').find('.strings#i' + (i));

                        var lastWord = document.getElementById('i' + (i)).firstElementChild;
                        var firstWord = $(beforeLine).find('.word:first').attr('contenteditable', 'true').attr('spellcheck', "false");
                        var elemID = $(firstWord).attr('id');
                        var elem = document.getElementById(elemID);
                        onfocus(elem);
                    }
                    
                    if ($(line).html() === '') {
                        
                        $(line).remove();
                        $(br).remove();
                        
                        var number_lines = $('.number_bar').find('.number_style:last');
                        $(number_lines).remove();

                        var lines = $('#interpreter').find('.strings');
                        
                        changeIdLine(lines);    //переписываем id строк
                        
                        var brs = $('#interpreter').find('#b' + (i + 1));
                        
                        changeIdBr(brs);
                    }
                    
                    mainCodeW2.splice(i, 1);
                }

            } else if (getCaretPos(this) === 0 && word !== '') {
                
                $(this).removeAttr('contenteditable');
                var beforeWord = document.getElementById('i' + i + 'j' + (j - 1));
                
                $('.word#i' + i + 'j' + (j - 1)).attr('contenteditable', 'true').attr('spellcheck', "false");
                placeCaretAtEnd(beforeWord);
                mainCodeW2[i][j] = arrWord;
                
            } else if(getCaretPos(this) !== 0) {
                mainCodeW2[i][j] = arrWord;
            }
        }
        
        if(event.keyCode === 32) {
            
            //console.log(mainCodeW2[i][j]);
        } 
        
        
    });
//   -----------------------------------------------------------------------------------------------

    $("body").on('keypress', '.green', function(event) {
    
        var stringAutocomplete = $("div").find($("#autocomplete_block"));
        if (stringAutocomplete.html() !== '')  $("#autocomplete_block").remove();

        var keyCode = event.which;

        if (keyCode > 32) {

            if (keyCode === 61) {
                $("#i" + str_number + "j" + (wrd_number - 1) + "").syntax();
                if (mainCodeW2[str_number][wrd_number - 1][mainCodeW2[str_number][wrd_number - 1].length - 1 ] === ':') {     //    формируем :=
                    mainCodeW2[str_number][wrd_number - 1].pop();
                    $("#i" + str_number + "j" + (wrd_number - 1) + "").html(mainCodeW2[str_number][wrd_number - 1]);
                    
                    if(!mainCodeW2[str_number][wrd_number -1].length){
                          mainCodeW2[str_number].pop();
                          wrd_number --;
                    }

                        wrd_number ++;
                        mainCodeW2[str_number][wrd_number -1] = [];
                        mainCodeW2[str_number][wrd_number - 1].push(':=');
                        $("#" + str_number).html($("#" + str_number).html() + '<span class = "word sighns" id = "i' + str_number + "j" + (wrd_number - 1) + '"></span>');  //открываем спан для слова			
                        $("#i" + str_number + "j" + (wrd_number - 1) + "").html(mainCodeW2[str_number][wrd_number - 1]);
                    
                }
                else {
                    ++wrd_number;
                    mainCodeW2[str_number][wrd_number - 1] = [];
                    $("#" + str_number).html($("#" + str_number).html() + '<span class = "word sighns" id = "i' + str_number + "j" + (wrd_number - 1) + '"></span>');  //открываем спан для слова					

                    mainCodeW2[str_number][wrd_number - 1].push('=');
                    $("#i" + str_number + "j" + (wrd_number - 1) + "").html(mainCodeW2[str_number][wrd_number - 1]);  // ввожу в спан  полученный символ по коду
                }

                if ($("#" + str_number).is($(".comment")))
                    $("#i" + str_number + "j" + (wrd_number - 1) + "").addClass("comm");

            }

            else {
                if (keyCode === 33) {                                        //знак комментария
                    $("#i" + str_number + "j" + (wrd_number - 1) + "").syntax();
                    $("#" + str_number).addClass("comment");
                    ++wrd_number;
                    mainCodeW2[str_number][wrd_number - 1] = [];
                    $("#" + str_number).html($("#" + str_number).html() + '<span class = "word comm" id = "i' + str_number + 'j' + (wrd_number - 1) + '"></span>');

                }
                else

                if (((keyCode > 39) && (keyCode <= 45))||(keyCode === 47) || (keyCode === 59) || (keyCode === 60) || (keyCode === 62)) {    //отделяем скобки, знаки операций и прочее

                    $("#i" + str_number + "j" + (wrd_number - 1) + "").syntax();
                    ++wrd_number;
                    mainCodeW2[str_number][wrd_number - 1] = [];
                    $("#" + str_number).html($("#" + str_number).html() + '<span class = "word sighns" id = "i' + str_number + 'j' + (wrd_number - 1) + '"></span>');  //открываем спан для слова

                    if ($("#" + str_number).is(".comment"))
                        $("#i" + str_number + "j" + (wrd_number - 1) + "").addClass("comm");     //если комментарий
                    
                    
                }
                else

                if ((keyCode === 34) ||(keyCode === 39)) {    //отделяем кавычки

                    $("#i" + str_number + "j" + (wrd_number - 1) + "").syntax();
                    ++wrd_number;
                    mainCodeW2[str_number][wrd_number- 1] = [];
                    $("#" + str_number).html($("#" + str_number).html() + '<span class = "word sighns" id = "i' + str_number + 'j' + (wrd_number - 1) + '"></span>');  //открываем спан для слова

                }

                else if (((!wrd_number) && (mainCodeW2[str_number][wrd_number].length === 0)) ||
                        ((wrd_number !== 0) && ((mainCodeW2[str_number][wrd_number - 1][mainCodeW2[str_number][wrd_number - 1].length - 1] === ' ') || (mainCodeW2[str_number][wrd_number - 1][mainCodeW2[str_number][wrd_number - 1].length - 1] === '_|'))) ||
                        ($("#i" + str_number + "j" + (wrd_number - 1) + "").attr("class") === "sighns")) {

                    ++wrd_number;
                    mainCodeW2[str_number][wrd_number - 1] = [];
                    $("#" + str_number).html($("#" + str_number).html() + '<span class = "word" id = "i' + str_number + 'j' + (wrd_number - 1) + '"></span>');  //открываем спан для слова
                    if ($("#" + str_number).is($(".comment")))
                        $("#i" + str_number + "j" + (wrd_number - 1) + "").addClass("comm");                   //если комментарий
                }
                var lowcase = String.fromCharCode(keyCode);
                mainCodeW2[str_number][wrd_number - 1].push(lowcase);
                $("#i" + str_number + "j" + (wrd_number - 1) + "").html(mainCodeW2[str_number][wrd_number - 1]);  // ввожу в спан  полученный символ по коду
                
               /* mainCodeW2[str_number][wrd_number - 1].pop();      //------------убираем регистрозависимость
                mainCodeW2[str_number][wrd_number - 1].push(lowcase); */
               
        }
 
              /*  if (mainCodeW2[str_number][wrd_number - 1]. length >= 3){
                    autocomplete_m (mainCodeW2[str_number][wrd_number - 1], str_number, wrd_number-1);    //---------------автодополнение
                    if_autocomplete = 1;
                }*/
        }
        
    return false;
            
    });

    $("body").on("keyup", '.green', function(event) {

        $(this).val('');

        if (event.keyCode === 32 || event.keyCode === 13) {
            
            
            if(if_autocomplete === 1) {
                var str = $("#i" + str_number + "j" + (wrd_number - 1) + "").html();
                mainCodeW2[str_number][wrd_number - 1].length = 0;
                mainCodeW2[str_number][wrd_number - 1] = [];
                for(var i = 0; i < str.length; ++i){
                   mainCodeW2[str_number][wrd_number - 1][i] = str[i]; 
                }
                if_autocomplete = 0;
            }

            $("#i" + str_number + "j" + (wrd_number - 1) + "").syntax();

            if (event.keyCode === 32) {
                
                if(wrd_number !==0){
                    var pop_el = mainCodeW2[str_number][wrd_number - 1]. pop();
                
                    if(pop_el === ';') {  //---------------------------------------случай, когда на входе ";"----------------перенос строки------------------------------------------------------------------
                            
                            mainCodeW2[str_number][wrd_number - 1]. push(pop_el);
                            mainCodeW2[str_number][wrd_number] = [];
                            mainCodeW2[str_number][wrd_number].push('\n');


                            ++str_number;
                            wrd_number = 0;

                            mainCodeW2[str_number] = [];
                            mainCodeW2[str_number][0] = [];

                            $(this).before('<br id = "b' + str_number + '"><div id = "' + str_number + '" class = "strings">')

                            $("#number_bar").empty();      //-------------------номер строки----------
                            for (var i = 1; i <= mainCodeW2.length; i++) {
                                $("#number_bar").append("<span class='number_style'>" + i + "</span>");
                            }

                                /*mainCode = lexc_analize(mainCodeW2);
                                mainCode1 = aux_code(mainCode);
                                console.log (mainCode1);
                                if (mainCode1.length !== 0) {
                                    $("#id_error").html('');
                                    //str = parse(mainCode1, alfabetMain, automaticMain, symbolMain, praviloMain, 0, 81, str_number +1, is_debug);
                                    stringCode = $('#interpreter').find('div');
                                }*/
                    }
                    else{
                        mainCodeW2[str_number][wrd_number - 1]. push(pop_el);
                        mainCodeW2[str_number][wrd_number - 1].push(' ');
                        $("#i" + str_number + "j" + (wrd_number - 1) + "").html(mainCodeW2[str_number][wrd_number - 1]);
                    }
                }
                
            }

            else {

                mainCodeW2[str_number][wrd_number] = [];
                mainCodeW2[str_number][wrd_number].push('\n');


                ++str_number;
                wrd_number = 0;

                mainCodeW2[str_number] = [];
                mainCodeW2[str_number][0] = [];

                $(this).before('<br id = "b' + str_number + '"><div id = "' + str_number + '" class = "strings">')

                $("#number_bar").empty();      //-------------------номер строки----------
                for (var i = 1; i <= mainCodeW2.length; i++) {
                    $("#number_bar").append("<span class='number_style'>" + i + "</span>");
                }
                
                    mainCode = lexc_analize(mainCodeW2);
                    mainCode1 = aux_code(mainCode);
              /*  console.log('---------------------------');
                console.log (mainCode);
                console.log (mainCode1);
                console.log('---------------------------');*/
                
                //-----------------------------------------------добавление переменных в БД/ удаление-------------------------------
                if (mainCode1.length !== 0) {
                    
                        var words = [];
                    
                        for (var i = 0; i < mainCode1.length; ++i){
                            for(var j = 0; j < mainCode1[i].length; ++j){
                                if(mainCode1[i][j] === '#NAME') {
                                    
                                    if(words.length === 0)words.push(mainCode[i][j]);
                                    else
                                    for(var k = 0; k < words.length; ++k){
                                        if(mainCode[i][j] === words[k]) break;
                                        if(k === (words.length -1))words.push(mainCode[i][j]);
                                    }
                                }
                            }
                        }
                        //--------------------------удаляем все-----------------
                        $.ajax({
                            url: '/shaya/variable_delete',
                            method: 'get',
                            dataType: 'json',
                            success: function(answer) {
                            //обработчик на ответ сервера
                            }
                        });
                        
                        //--------------------------------добавляем все-------------------------
                        $.ajax({
                            url: '/shaya/words',
                            method: 'post',
                            data: {'words': words},
                            dataType: 'json',
                            success: function(answer) {
                            //обработчик на ответ сервера
                            }
                        });
                    
                    //--------------------на будущее-------------------------------------------
                       /* $("#id_error").html('');
                        str = parse(mainCode1, alfabetMain, automaticMain, symbolMain, praviloMain, 0, 81, -1);
                        stringCode = $('#interpreter').find('div');*/
                    }
            }
            
        }

    });

    $("body").on("keydown", '.green', function(event) {
    
        var stringAutocomplete = $("div").find($("#autocomplete_block"));
        if (stringAutocomplete.html() !== '')  $("#autocomplete_block").remove();
        
        var keyCode = event.which;

        if (keyCode === 8) {
            

            if ($("div").is($(".strings"))) {

                if (mainCodeW2[str_number].length){
                      if(wrd_number === 0){
                        $("#i" + str_number + "j" + wrd_number).remove();
                        mainCodeW2[str_number].pop();                      
                        }
                        else if(!mainCodeW2[str_number][wrd_number-1].length) {
                                --wrd_number;
                                $("#i" + str_number + "j" + wrd_number).remove();
                                mainCodeW2[str_number].pop();
                            }
                            else{
                                mainCodeW2[str_number][wrd_number - 1].pop();
                                $("#i" + str_number + "j" + (wrd_number - 1) + "").html(mainCodeW2[str_number][wrd_number - 1]);  
                            }                  
                }
                
                if (mainCodeW2[str_number].length === 0) {
                    if (str_number > 0) {
                        $("#" + str_number).remove();
                        $("#b" + str_number).remove();
                        mainCodeW2.pop();
                        console.log(str_number);
                        --str_number;
                        mainCodeW2[str_number].pop();
                        wrd_number = mainCodeW2[str_number].length;

                    }
                    else {
                        $("#" + str_number).remove();
                        mainCodeW2.pop();
                        str_number = 0;
                        wrd_number = 0;
                    }

                }
            }

               /* if (mainCodeW2[str_number][wrd_number - 1]. length >= 3){
                    autocomplete_m(mainCodeW2[str_number][wrd_number - 1], str_number, wrd_number-1);    //---------------автодополнение
                    if_autocomplete = 1;
                }*/
        }

        else {

            if (!$("div").is(".strings")) {//------------------------первое вхождение строки------------------------
                str_number = 0;
                wrd_number = 0;
                mainCodeW2[0] = [];
                mainCodeW2[0][0] = [];
                $(this).before('<div id = "' + str_number + '" class = "strings">');
            }
        }
    });

    var str;
    var stringCode = [];
    
    //-----------------------------------------------------кнопка отладки------------------------------------------

    $("#play_button").on("click", function() {
        
        isRobot = 1;
        //0. ----------------------------------------------удаляем блок автокомплита (надо убрать) -----------------------
        var stringAutocomplete = $("div").find($("#autocomplete_block"));
        if (stringAutocomplete.html() !== '')  $("#autocomplete_block").remove();
        
        //-------------------------------1. Преобразуем массив в необходимую форму----------------------------

        stringCode = $('#interpreter').find('div');

        if (goToCode === 0) {
            mainCode = lexc_analize(mainCodeW2);
            mainCode1 = aux_code(mainCode);
        } 

            console.log(mainCode);
            console.log(mainCode1);
        
            
        $('#my_textarea').prop("disabled", true); // запрещаем редактирование
        
        is_debug = 1;
        
        console.log (mainCode1);
        
        //--------------------------2. синтаксический анализ-----------------------------------------------------------------------
      /* if(goToCode === 0){
            
        if (mainCode1.length === 0) {
            $("#id_error").html($("#id_error").html()+'Программа пуста!');
            goToCode = -1;
        } else 
            if (mainCode1.length !== 0) {
                    $("#id_error").html('');
                    str = parse(mainCode1, alfabetMain, automaticMain, symbolMain, praviloMain, 0, 81, is_debug);
                    stringCode = $('#interpreter').find('div');
                    console.log('parse');
                }

            if(!str.result_func) {
                            $(stringCode).eq(goToCode).removeClass('active-string');
                            $("#id_error").html($("#id_error").html()+'Ошибка синтаксиса в строке ' + (str.str+1)); //выводим ошибку
                            $(stringCode).eq(str.str).addClass('active-string-error'); //подсвечиваем строку красным
                            $("#play_button").prop("disabled", true);        //останавливаем выполнение
                            is_error = str.str;
            }
        }*/
        
       /* if(goToCode === 0 && mainCode1.length === 11) {
                    $("#id_error").html($("#id_error").html()+'Ошибка синтаксиса в строке ' + 11 +'(недопустимый вывод)'); //выводим ошибку
                    $(stringCode).eq(10).addClass('active-string-error'); //подсвечиваем строку красным
                    $("#play_button").prop("disabled", true);        //останавливаем выполнение
                    is_error = 10;      
        
        }*/
        //-------------------------------------3. если все ок - выполнение--------------------------------------------------
        if(!is_error){
            if(mainCode1[goToCode][0])
            if (is_change !== -1){   //---------------------для перескоков----------------------
                $(stringCode).eq(is_change).removeClass('active-string');
                is_change = -1;
                $(stringCode).eq(goToCode).addClass('active-string');
                if ((isRobot) && (is_debug)) {
                    robotWork(mainCode1[goToCode]);
                }
                else{
                    //mainWork(mainCode1[goToCode]);
                }           
            }
            else{
                if (goToCode === 0) {   //-------------------------------подсвечиваем считываемую строку----------------
                    $(".row_end").html('Выполнение началось...<br>');  //--------------------------строка состояния
                    $(stringCode).eq(goToCode).addClass('active-string');
                    if ((isRobot) && (is_debug)) {
                        robotWork(mainCode1[goToCode]);
                    }
                   
                    catch_ifs_and_fors(mainCode); //--------------------------------------отлавливаем все ифы и циклы-----

                }
                if (goToCode > 0) {
                    $(stringCode).eq(goToCode - 1).removeClass('active-string');
                    $(stringCode).eq(goToCode).addClass('active-string');
                    if ((isRobot) && (is_debug)) {
                        robotWork(mainCode1[goToCode]);
                    }
                    else{
                        //mainWork(mainCode1[goToCode]);
                    }
                }
            }
            //------------------------------------выполняем------------------------------------
            var result_play = main_function(mainCode[goToCode], goToCode);

            if(result_play.ifchange === (-1)){     //если не условие и не цикл

            if(result_play.result_play === (-2)) { //-------------------если ошибка выполнения---------------------------------
                
                            $("#id_error").html($("#id_error").html()+'Переменная не описана(строка  ' + (goToCode+1) + ')'); //выводим ошибку
                            $(stringCode).eq(goToCode).addClass('active-string-error'); //подсвечиваем строку красным
                            $("#play_button").prop("disabled", true);        //останавливаем выполнение
                            is_error = goToCode;
            }

            else if(result_play.result_play === (-3)) {
                            $("#id_error").html($("#id_error").html()+'Недопустимое значение для данного типа переменной(строка  ' + (goToCode+1) + ')'); //выводим ошибку
                            $(stringCode).eq(goToCode).addClass('active-string-error'); //подсвечиваем строку красным
                            $("#play_button").prop("disabled", true);        //останавливаем выполнение
                            is_error = goToCode;
            }
             goToCode++;
            }
            else {//--------------------условие или цикл
                is_change = goToCode;
                goToCode = result_play.ifchange;
            }

        }
        
        if(goToCode === mainCodeW2.length) {
            end_of_play();    
        }
    });
    
    //------------------------------------------------------------остановка отладки-----------------------------------------

    $("#stop_button").on('click', function() {
        end_of_play();
    });
 
    function end_of_play(){ //-----------------------конец выполнения
                
        is_debug = 0;
        
        $(".row_end").html('');  //--------------------------строка состояния
        
        for(var i = 0; i < goToCode; ++i){
            $(stringCode).eq(i).removeClass('active-string');
            $(stringCode).eq(i).removeClass('active-string-error');
            $(stringCode).eq(i).removeClass('active-string-in_out');       
        }
        $(stringCode).eq(is_error).removeClass('active-string-error');
        goToCode = 0;
        $('#my_textarea').prop("disabled", false);    //допускаем редактирование
        $('#id_error').html('');
        $("#play_button").prop('disabled', false);
        $('#id_in_out').html('');
        
        number_constructions = 0; //количество конструкций всего
        nesting_level = 0;  //уровень вложенности
        index_Group=0; //индекс группы
        number_groups = 0;
        construction_active = []; //----------------------------------активные конструкции-------------------------------
        construction_active.length = 0;
        construction_active[0] = {index_con: -1, array_of_variables: [], number_of_variables:1};
        construction_active[0].array_of_variables[0] = [];
        construction_active[0].array_of_variables[1] = [];
        construction_active[0].array_of_variables[2] = [];
        number_active_constr = 0;
        construction = [];
        construction.length = 0;
        number_last_constr = []; //--------------------номер последней кострукции
        is_error = 0;
    }
    //-------------------------------------------------------------------------------------------

    function lexc_analize(code) {    //  убираем пробелы и комментарии из массива

        var new_code = [];

        for (var i = 0; i < code.length; ++i) {
            new_code[i] = [];
            for (var j = 0; j < code[i].length; ++j)
                new_code[i][j] = code[i][j];
        }

        for (var i = 0; i < new_code.length; ++i) {
                new_code[i] = space_slice(new_code[i]);
        }


        for (var i = 0; i < new_code.length; ++i)
            for (var j = 0; j < new_code[i].length; ++j)
                if (new_code[i][j] === "!")
                    for (var k = new_code[i].length - 2; k >= j; --k) {
                        new_code[i][k] = new_code[i][k + 1];
                        new_code[i].pop();
                    }
        
    for (var i = 0; i < new_code.length; ++i){                                               // строки
        for (var j = 0; j < new_code[i].length; ++j){
            if((new_code[i][j] === '"')||(new_code[i][j] === "'")){
                
                var number_i = new_code[i].length;
                
                for(var k = j+1; k < number_i; ++k){  //ищем вторую кавычку
                    if((new_code[i][k] !== '"')&&(new_code[i][k] !== "'")&&(k != j+1))new_code[i][j] += ' ';
                    new_code[i][j] += new_code[i][k];
                    if((new_code[i][k] === '"')||(new_code[i][k] === "'")) break;
                }
                
                var t = 0;
                for(var p = k+1; p < number_i; ++p){   //перезаписываем оставшуюся часть строки
                    ++t;
                    new_code[i][j+t] = new_code[i][p];
                }
                for(var p = number_i-1; p >j+t;  --p)  //пушим остатки
                    new_code[i].pop();            
            }
        }
    }

        return new_code;

    }

    function space_slice(enterCode) {               //   отделение пробелов и собирание в строки

        var enterCode2 = [];
        for (var i = 0; i < enterCode.length; ++i) {

            var str = '';
            for (var j = 0; j < enterCode[i].length; ++j) {
                if (enterCode[i][j] !== ' ')
                    str = str + enterCode[i][j];
            }
            enterCode2[i] = str;
        }

        return enterCode2;

    }


    function isEmptyCode(mainCode) {
        for (var i = 0; i < mainCode.length; i++) {
            for (var j = 0; j < mainCode[i].length; j++) {

            }
        }
    }
    
    function aux_code(code){
        
        var new_code = [];
        
        for(var i = 0; i < code.length; ++i){
            new_code[i] = [];
            for(var j = 0; j < code[i].length; ++j){

            if(language.structure(code[i][j])) //ключевые слова
                    new_code[i][j] = code[i][j];

            else if(language.typeVariable(code[i][j])) //переменные
                    new_code[i][j] = code[i][j];

            else if(language.input_output(code[i][j])) //ввод/вывод
                    new_code[i][j] = code[i][j];

            else if(language.executor(code[i][j])) //исполнители
                    new_code[i][j] = code[i][j];

            else if(language.parameters(code[i][j])) //параметры
                    new_code[i][j] = code[i][j];
                
            else if(language.fors(code[i][j])) //параметры
                    new_code[i][j] = code[i][j];
            else if(language.ifelse(code[i][j])) //параметры
                    new_code[i][j] = code[i][j];
            
            else for(var l = 0; l < language.robot.length; ++l)
                    {if(code[i][j] === language.robot[l])
                        {new_code[i][j] = code[i][j];break;}
                     if(l === language.robot.length -1)

            for(var k = 0; k < language.sighns.length; ++k)
                {if(code[i][j] === language.sighns[k])
                        {new_code[i][j] = code[i][j]; break;}
                 if(k === language.sighns.length -1)
            
            if(language.bool_vaiables(code[i][j]))
                    new_code[i][j] = "#BOOL";
            
            else if(language.int_variables(code[i][j]))
                    new_code[i][j] = "#INT";
            else if(language.float_variables(code[i][j]))
                    new_code[i][j] = "#FLOAT";
            else if(language.variableName(code[i][j]))
                    new_code[i][j] = "#NAME";
           
           else if(language.char_variables(code[i][j]))
                    new_code[i][j] = "#CHAR";
           
           else if(language.str_variables(code[i][j]))
                    new_code[i][j] = "#STR";
           else if(code[i][j] !== '' && code[i][j] !== ' ') new_code[i][j] = "#SMTH";
            }
                    }
                }
        }
       
      return new_code;
    }

});

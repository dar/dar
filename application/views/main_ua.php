<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>DAR</title>
        <link href="<?= base_url() ?>assets/css/main.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/bootstrap/css/bootstrap.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/bootstrap/css/bootstrap-theme.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/img/dark-canvas.gif" rel="stylesheet">
        <script type="text/javascript" src="<?= base_url() ?>assets/js/jquery-2.0.3.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/bootstrap/js/bootstrap.js"></script>
    </head>
    <body class="style-page">
        <div class="main_block">
            <div class="main_image"><img src="<?= base_url() ?>assets/img/robot_light.png" height="515px" width="452px"></div>
            <div class="main_button">
                <div class="speech">
                    <p class="greeting">"ДАР" вітає Вас!</p>
                    <p><a class="learn_about" href="#">Познайомитися з програмою</a></p>
                    <br>

                    <form class="form-horizontal" role="form">
                        <div class="form_group_change">
                          <label for="inputEmail3" class="col-sm-4 control-label">Логін</label>
                          <div class="col-sm-4">
                            <input type="email" class="form-control" id="inputEmail3" placeholder="Email">
                          </div>
                        </div>
                        <div class="form_group_change">
                          <label for="inputPassword3" class="col-sm-4 control-label">Пароль</label>
                          <div class="col-sm-4">
                            <input type="password" class="form-control" id="inputPassword3" placeholder="Password">
                          </div>
                        </div>
                        <div class="form_group_change">
                          <div class="col-sm-offset-4 col-sm-4">
                            <div class="checkbox">
                              <label>
                                <input type="checkbox">Запам'ятати мене
                              </label>
                            </div>
                          </div>
                        </div>
                        <div class="form_group_change">
                          <div class="col-sm-offset-4 col-sm-4">
                            <!--<button type="submit" class="btn btn-default">Войти</button>-->
                            <a class="btn btn-primary btn-lg" href="<?= base_url() ?>shaya_ua" role="button">Увійти</a>
                          </div>
                        </div>
                      </form>
                </div>
            </div>
        </div>
    </body>
</html>

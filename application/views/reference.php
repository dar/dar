<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>DAR</title>
        <link href="<?= base_url() ?>assets/css/main.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/css/teaching.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/bootstrap/css/bootstrap.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/bootstrap/css/bootstrap-theme.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/css/robot.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/css/drawer.css" rel="stylesheet">
        <script type="text/javascript" src="<?= base_url() ?>assets/js/jquery-2.0.3.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/main.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/interpreter.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/bootstrap/js/bootstrap.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/reference.js"></script>
    </head>
    <body class="style-page">
        <div id="container">
            <div class="row">
                <div class="content">
                    <div class="navbar navbar-default">
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav">
                                <a class="navbar-brand" href="<?= base_url() ?>">ДАР</a>
                                <li><a href="<?= base_url() ?>shaya">Интерпретатор</a></li>
                                <li><a href="<?= base_url() ?>training">Обучение</a></li>
                                <!--<li><a href="#">Коллекция примеров</a></li>-->
                                <!-- <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Исполнители</a>
                                    <ul class="dropdown-menu">
                                        <button class="btn btn-sm btn-success" id="robot">Робот</button>
                                        <button class="btn btn-sm btn-primary" id="drawer">Чертежник</button>
                                    </ul>
                                </li>-->
                                <li><a href="<?= base_url() ?>reference">Справочник</a></li>
                                <!--<li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Выбрать язык <b class="caret"></b></a>
                                    <ul class="dropdown-menu">    
                                        <a href="<?= base_url() ?>shaya"><img src="<?= base_url() ?>assets/img/Russia.png" width="30" height="30" ></a>
                                        <a href="<?= base_url() ?>shaya_ua"><img src="<?= base_url() ?>assets/img/Ukraine.png" width="30" height="30"></a>      
                                    </ul>
                                </li>-->
                            </ul>  
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="content">
                    <div class="well-smt"></div>                   
                </div>
                <div class="content">
                    <div class="name_searching">
                        Поиск элементов списка
                    </div>
                    <div class="name_list">
                        Выбор элемента справки
                    </div>
                    <div class="name_explanation">
                        Расшифровка элемента
                    </div>
                    <div class="searching">

                        <input type="text" class="searching_string" id="searching_string" placeholder="Строка поиска">                           
                        <button class="btn btn-default" id="btn_searching">Найти</button>
                        <div id="searching_results"></div>

                    </div>
                    <div class="reference_list">
                        <span class="header_list"><img src="../assets/img/Stage1.png" class="image_left_st1">Справочник</span>
                        <ul class="ul-treefree ul-dropfree img" id="list_directory">
                            <li><img src="../assets/img/Stage2.png" class="alignment_icon"> Алгоритм и его общий вид
                                <ul class="ul-treefree ul-dropfree" id="all_links">
                                    <li><div> Алгоритм как основная структура алгоритма</div>
                                        <span>
                                            &emsp;Алгоритм — набор инструкций, описывающих порядок действий исполнителя для достижения результата решения задачи за конечное число действий.<br>
                                            &emsp;Алгоритм является основной структурной единицей школьного алгоритмического языка. В простейшем случае программа состоит из нескольких алгоритмов, следующих один за другим.
                                            Перед первым алгоритмом может располагаться вступление - любая неветвящаяся последовательность команд. Например, это могут быть строки с комментариями, 
                                            описаниями общих величин программы, командами присваивания им начальных значений и пр. После последнего алгоритма могут располагаться описания исполнителей.<br>
                                            &emsp;Алгоритмы в программе должны располагаться вплотную друг к другу, между ними могут быть только пустые строки и строки с комментариями.
                                        </span>
                                    </li>
                                    <li><div> Общий вид алгоритма</div>
                                        <span>&emsp;Общий вид алгоритма имеет вид: <br><br>
                                            <strong>алг</strong> имя (<i>аргументы</i> и <i>результаты</i>)<br>
                                            <strong>дано</strong> <i>условия применимости алгоритма</i><br>
                                            <strong>надо</strong> <i>цель выполнения алгоритма</i><br>
                                            <strong>нач</strong><br>
                                            &emsp;<i>тело алгоритма</i><br>
                                            <strong>кон</strong></span>
                                    </li>
                                </ul>
                            </li>
                            <li><img src="../assets/img/Stage2.png" class="alignment_icon"> Ключевые слова
                                <ul class="ul-treefree ul-dropfree">
                                    <li><img src="../assets/img/Stage3.png" class="alignment_icon image_left_st3"> Конструкции языка
                                        <ul class="ul-treefree ul-dropfree" id="all_links">
                                            <li><div><b> исп</b></div>
                                                <span>&emsp;Команда <strong>исп</strong> используется при описании исполнителя, определяет какого исполнителя будет использовать интерпретатор.</span>
                                            </li>
                                            <li><div><b> кон_исп</b>&nbsp;&nbsp;&nbsp;или&nbsp;&nbsp;&nbsp;<b>кон исп</b></div>
                                                <span>&emsp;Команда <strong>кон_исп</strong> или кон исп обозначает конец использования исполнителя.</span>
                                            </li>
                                            <li><div><b> использовать</b></div>
                                                <span>&emsp;Команда <strong>использовать</strong> определяет какого исполнителя будет использовать интерпретатор.</span>
                                            </li>                                           
                                            <li><div><b> алг</b></div>
                                                <span>&emsp;Команда <strong>алг</strong> означает, что начинается описание алгоритма.</span>
                                            </li>
                                            <li><div><b> нач</b></div>
                                                <span>&emsp;Команда <strong>нач</strong> означает начало алгоритма.</span>
                                            </li>
                                            <li><div><b> кон</b></div>
                                                <span>&emsp;Команда <strong>кон</strong> означает конец алгоритма.</span>
                                            </li>
                                            <li><div><b> дано</b></div>
                                                <span>&emsp;Команда <strong>дано</strong> определяет условия применимости алгорима.</span>
                                            </li>
                                            <li><div><b> надо</b></div>
                                                <span>&emsp;Команда <strong>надо</strong> определяет цель выполнения алгоритма.</span>
                                            </li>
                                            <!--<li><div><b> утв</b></div>
                                                <span>Описание команды <strong>утв</strong> </span>
                                            </li>
                                            <li><div><b> нц</b></div>
                                                <span>Описание команды <strong>нц</strong> </span>
                                            </li>
                                            <li><div><b> кц</b></div>
                                                <span>Описание команды <strong>кц</strong> </span>
                                            </li>
                                            <li><div><b> кц_при</b>&nbsp;&nbsp;&nbsp;или&nbsp;&nbsp;&nbsp;<b>кц при</b></div>
                                                <span>Описание команд кц_при или кц при</span>
                                            </li>
                                            <li><div><b> для</b></div>
                                                <span>Описание команды для</span>
                                            </li>
                                            <li><div><b> раз</b></div>
                                                <span>Описание команды раз</span>
                                            </li>
                                            <li><div><b> пока</b></div>
                                                <span>Описание команды пока</span>
                                            </li>
                                            <li><div><b> от</b></div>
                                                <span>Описание команды от</span>
                                            </li>
                                            <li><div><b> до</b></div>
                                                <span>Описание команды до</span>
                                            </li>
                                            <li><div><b> шаг</b></div>
                                                <span>Описание команды шаг</span>   
                                            </li>
                                            <li><div><b> выход</b></div>
                                                <span>Описание команды выход</span>
                                            </li>
                                            <li><div><b> если</b></div>
                                                <span>Описание команды если</span>
                                            </li>
                                            <li><div><b> то</b></div>
                                                <span>Описание команды то</span>
                                            </li>
                                            <li><div><b> иначе</b></div>
                                                <span>Описание команды иначе</span>
                                            </li>
                                            <li><div><b> все</b>&nbsp;&nbsp;&nbsp;или&nbsp;&nbsp;&nbsp;<b>всё</b></div>
                                                <span>Описание команды все или всё</span>
                                            </li>
                                            <li><div><b> выбор</b></div>
                                                <span>Описание команды выбор</span>
                                            </li>
                                            <li><div><b> ввод</b></div>
                                                <span>Описание команды ввод</span>
                                            </li>
                                            <li><div><b> Фввод</b></div>
                                                <span>Описание команды Фввод</span>
                                            </li>
                                            <li><div><b> Фвывод</b></div>
                                                <span>Описание команды Фвывод</span>
                                            </li>
                                            <li><div><b> нс</b></div>
                                                <span>Описание команды нс</span>
                                            </li>
                                            <li><div><b> не</b></div>
                                                <span>Описание команды не</span>
                                            </li>
                                            <li><div><b> и</b></div>
                                                <span>Описание команды и</span>
                                            </li>
                                            <li><div><b> или</b></div>
                                                <span>Описание команды или</span>
                                            </li>
                                            <li><div><b> рез</b></div>
                                                <span>Описание команды рез</span>
                                            </li>
                                            <li><div><b> аргрез</b></div>
                                                <span>Описание команды аргрез</span>
                                            </li>
                                            <li><div><b> ВКЛЮЧИТЬ</b></div>
                                                <span>Описание команды ВКЛЮЧИТЬ</span>
                                            </li>
                                            <li><div><b> знач</b></div>
                                                <span>Описание команды знач</span>
                                            </li>-->
                                        </ul>
                                    </li>

                                    <!--<li><img src="../assets/img/Stage3.png" class="alignment_icon image_left_st3"> Типы данных
                                        <ul class="ul-treefree ul-dropfree" id="all_links">
                                            <li><div><b> цел</b></div>
                                                <span>Описание команды цел</span>
                                            </li>
                                            <li><div><b> вещ</b></div>
                                                <span>Описание команды вещ</span>
                                            </li>
                                            <li><div><b> сим</b></div>
                                                <span>Описание команды сим</span>
                                            </li>
                                            <li><div><b> лит</b></div>
                                                <span>Описание команды лит</span>
                                            </li>
                                            <li><div><b> лог</b></div>
                                                <span>Описание команды лог</span>
                                            </li>
                                            <li><div><b> целтаб</b></div>
                                                <span>Описание команды целтаб</span>
                                            </li>
                                            <li><div><b> вещтаб</b></div>
                                                <span>Описание команды вещтаб</span>
                                            </li>
                                            <li><div><b> симтаб</b></div>
                                                <span>Описание команды симтаб</span>
                                            </li>
                                            <li><div><b> литтаб</b></div>
                                                <span>Описание команды литтаб</span>
                                            </li>
                                            <li><div><b> логтаб</b></div>
                                                <span>Описание команды логтаб</span>
                                            </li>
                                        </ul>
                                    </li>-->
                                    <li><img src="../assets/img/Stage3.png" class="alignment_icon image_left_st3"> Типы величин
                                        <ul class="ul-treefree ul-dropfree" id="all_links">
                                            <li><div><b> цел</b></div>
                                                <span>&emsp;Тип <strong>цел</strong> является числовым типом. Величина типа <strong>цел</strong> принимает целые значения от -МЦЕЛ до МЦЕЛ, где МЦЕЛ = 2147483647 = 2<sup>31</sup> − 1.</span>
                                            </li>
                                            <li><div><b> вещ</b></div>
                                                <span>&emsp;Тип <strong>вещ</strong> является числовым типом. Величина типа <strong>вещ</strong> принимает вещественные значения между -МВЕЩ до МВЕЩ, где МВЕЩ – это число немного меньшее, чем 2<sup>1024</sup>; МВЕЩ ≈ 1.797693 × 10<sup>308</sup>.</span>
                                            </li>
                                            <li><div><b> лог</b></div>
                                                <span>&emsp;Величины типа <strong>лог</strong> принимает значения да или нет (внутреннее представление – да = 1, нет = 0).</span>
                                            </li>
                                            <li><div><b> сим</b></div>
                                                <span>&emsp;Тип <strong>сим</strong> является текстовым типом. Значением типа <strong>сим</strong> может быть любой литеральный символ (практически любой символ).</span>
                                            </li>
                                            <li><div><b> лит</b></div>
                                                <span>&emsp;Тип <strong>лит</strong> является текстовым типом. Значением типа <strong>лит</strong> может быть строка литеральных символов.</span>
                                            </li>
                                        </ul>
                                    </li>

                                    <!--<li><img src="../assets/img/Stage3.png" class="alignment_icon image_left_st3"> Постоянные величины
                                        <ul class="ul-treefree ul-dropfree" id="all_links">
                                            <li><div><b> да</b></div>
                                                <span>Описание величины да</span>
                                            </li>
                                            <li><div><b> нет</b></div>
                                                <span>Описание величины нет</span>
                                            </li>
                                        </ul>
                                    </li>-->
                                </ul>
                            </li>
                            <li><img src="../assets/img/Stage2.png" class="alignment_icon"> Команды алгоритмического языка
                                <ul class="ul-treefree ul-dropfree" id="all_links">
                                    <!--<li><div> Циклы</div>
                                        <span>Циклы</span>
                                    </li>-->
                                    <li><div> Условные операторы</div>
                                        <span>
                                            Вид 1:<br>
                                            <strong>если</strong> <i>условие</i><br>
                                            <strong>то</strong> <i>серия 1</i><br>
                                            <strong>иначе</strong> <i>серия 2</i><br>
                                            <strong>все</strong><br><br>

                                            Вид 2:<br>    
                                            <strong>выбор</strong> <i>условие</i><br>
                                            <strong>при</strong> <i>условие 1: серия 1</i><br>
                                            <strong>при</strong> <i>условие 2: серия 2</i><br>
                                            . . .
                                            <strong>при</strong> <i>условие n: серия n</i><br>
                                            <strong>иначе</strong> <i>серия n+1</i><br>
                                            <strong>все</strong><br><br>

                                            Вид 3:<br>
                                            <strong>если</strong> <i>условие</i><br>
                                            <strong>то</strong> <i>серия 1</i><br>
                                            <strong>все</strong><br><br>

                                            Вид 4:<br>
                                            <strong>выбор</strong> <i>условие</i><br>
                                            <strong>при</strong> <i>условие 1: серия 1</i><br>
                                            <strong>при</strong> <i>условие 2: серия 2</i><br>
                                            . . .
                                            <strong>при</strong> <i>условие n: серия n</i><br>
                                            <strong>все</strong>

                                        </span>
                                    </li>
                                    <li><div> Вызов</div>
                                        <span>Вызов реализовывается следующим образом:<br>
                                            <strong>имя алгоритма</strong> (аргументы и имена результатов)</span>
                                    </li>
                                    <li><div> Присваивание</div>
                                        <span>Присваивание реализовывается следующим образом:<br>
                                            <strong>имя величины</strong> := выражение</span>
                                    </li>
                                    <li><div> Ввод и вывод</div>
                                        <span><strong>Ввод</strong><br>
                                            <i>Формат вызова: </i><strong>ввод</strong> имя1, имя2, ..., имяN<br>
                                            &emsp;При выполнении этой команды выводится курсор в окно ввода-вывода, программа ждет,
                                            пока пользователь введет соответствующие значения. По окончании введенные значения
                                            присваиваются указанным величинам. В качестве имени величины можно указать имя
                                            простой величины или имя элемента таблицы с указанием значений индексов. Призна-
                                            ком конца ввода служит нажатие на клавишу Enter. При вводе нескольких чисел они
                                            отделяются друг от друга запятой или пробелом.<br><br>

                                            <strong>Вывод</strong><br>
                                            <i>Формат вызова: </i><strong>вывод</strong> выражение1, выражение2, ..., выражениеN<br>
                                            &emsp;Каждое выражение может быть либо арифметическим, логическим или текстовым выражением, либо командой перехода на новую строку (ключевое слово нс). 
                                            Значения выражений выводятся последовательно в строку области ввода-вывода и разделяются пробелом. 
                                            Когда строка полностью заполнена, автоматически происходит переход к началу новой строки.
                                        </span>
                                    </li>
                                    <!--<li><div><b> утв</b></div>
                                        <span>Описание команды утв</span>
                                    </li>
                                    <li><div><b> выход</b></div>
                                        <span>Описание команды выход</span>
                                    </li>-->
                                </ul>
                            </li>
                            <li><img src="../assets/img/Stage2.png" class="alignment_icon"> Виды величин
                                <ul class="ul-treefree ul-dropfree" id="all_links">
                                    <li><div> Аргументы</div>
                                        <span>Аргументы (<strong>арг</strong>) описываются в заголовке алгоритма.</span>
                                    </li>
                                    <li><div> Результаты</div>
                                        <span>Результаты (<strong>рез</strong>) описываются в заголовке алгоритма.</span>
                                    </li>
                                    <li><div> Значения функций</div>
                                        <span>Значения функций (<strong>знач</strong>) описываются указанием типа перед именем алгоритма-функции.</span>
                                    </li>
                                    <li><div> Локальный</div>
                                        <span>Локальные величины описываются в теле алгоритма, между <strong>нач</strong> и <strong>кон</strong>.</span>
                                    </li>
                                    <li><div> Общие</div>
                                        <span>Общие велличины описываются после строки <strong>исп</strong> исполнителя, до первой строки <strong>алг</strong>.</span>
                                    </li>
                                </ul>
                            </li>
                            <li><img src="../assets/img/Stage2.png" class="alignment_icon"> Арифметические операции
                                <ul class="ul-treefree ul-dropfree" id="all_links">
                                    <li><div> Сложение</div>
                                        <span>Операция сложения выполняется таким образом: x + y.</span>
                                    </li>
                                    <li><div> Вычитание</div>
                                        <span>Операция вычитания выполняется таким образом: x - y.</span>
                                    </li>                                
                                    <li><div> Умножение</div>
                                        <span>Операция умножения выполняется таким образом: x * y.</span>
                                    </li>
                                    <li><div> Деление</div>
                                        <span>Операция деления выполняется таким образом: x / y.</span>
                                    </li>
                                    <li><div> Возведение в степень </div>
                                        <span>Операция возведения в степень выполняется таким образом: x ** y.</span>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="reference_explanation" id='reference_explanation'>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="content">
                    <div class="well-smb"></div> 
                </div>
            </div>
            <div class="row">
                <div class="content">
                    <div class="navbar navbar-default">
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
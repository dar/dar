$(function() {
    var Interval_1, Interval_2, Interval_3, Interval_4, Interval_5;
    var array_steps = [];

//------------------------------------------------------------------------------Making array of 'div'
    $("#text_train div").each(function(i, val) {
        array_steps[i] = val;
        console.log(array_steps[i]);
    });

//------------------------------------------------------------------------------Pressing the button "btn_next"
    $('#btn_next').on('click', function() {

        $('.column_1').css("border", "none");
        $('#input_output').css("border", "1px solid #ccc");
        $('#id_error').css("border", "1px solid #ccc");
        $('#end_block').css("border", "1px solid #ccc");
        $('#id_error').css("border", "1px solid #ccc");

//------------------------------------------------------------------------------Making "btn_prev" enabled
        $('#btn_prev').removeClass('disabled');

//------------------------------------------------------------------------------Checking block visibility
        for (var k = 0; k < array_steps.length; k++) {

            $('#end_block').css("height", "100px");

            if (k === (array_steps.length - 2)) {
                $('#btn_next').addClass('disabled');
            }

            if ($(array_steps[k]).hasClass('visible_block')) {

                $(array_steps[k]).removeClass('visible_block').addClass('visible_none');
                $(array_steps[k + 1]).removeClass('visible_none').addClass('visible_block');

//------------------------------------------------------------------------------Fleshing_interpreter_block
                if (k === 0) {
                    $('.column_1').css("border", "3px solid #76EE00");

                    Interval_1 = setInterval(function() {
                        $('.column_1').css("border", "3px solid #ccc");
                        setTimeout(function() {
                            $('.column_1').css("border", "3px solid #76EE00");
                        }, 500);
                    }, 1000);

                    setTimeout(function() {
                        clearInterval(Interval_1);
                    }, 2100);
                }

//------------------------------------------------------------------------------Fleshing_input_output_block
                if (k === 1) {
                    $('.column_1').css("border", "none");
                    //$('.column_1').css("border", "1px solid #ccc");
                    $('#input_output').css("border", "3px solid #76EE00");

                    Interval_2 = setInterval(function() {
                        $('#input_output').css("border", "3px solid #ccc");
                        setTimeout(function() {
                            $('#input_output').css("border", "3px solid #76EE00");
                        }, 500);
                    }, 1000);

                    setTimeout(function() {
                        clearInterval(Interval_2);
                    }, 2100);
                }

//------------------------------------------------------------------------------Fleshing_error_block
                if (k === 2) {
                    $('#input_output').css("border", "1px solid #ccc");
                    $('#id_error').css("border", "3px solid #76EE00");

                    Interval_3 = setInterval(function() {
                        $('#id_error').css("border", "3px solid #ccc");
                        setTimeout(function() {
                            $('#id_error').css("border", "3px solid #76EE00");
                        }, 500);
                    }, 1000);

                    setTimeout(function() {
                        clearInterval(Interval_3);
                    }, 2100);
                }

//------------------------------------------------------------------------------Fleshing_end_block
                if (k === 3) {
                    $('#id_error').css("border", "1px solid #ccc");
                    $('#end_block').css("border", "3px solid #76EE00");

                    Interval_4 = setInterval(function() {
                        $('#end_block').css("border", "3px solid #ccc");
                        setTimeout(function() {
                            $('#end_block').css("border", "3px solid #76EE00");
                        }, 500);
                    }, 1000);

                    setTimeout(function() {
                        clearInterval(Interval_4);
                    }, 2100);
                }

//------------------------------------------------------------------------------Fleshing_buttons_block
                if (k === 4) {
                    $('#end_block').css("border", "1px solid #ccc");

                    $('#begin_block').css("border", "3px solid #76EE00");
                    $('#end_block').css("height", "96px");

                    Interval_5 = setInterval(function() {
                        $('#begin_block').css("border", "3px solid #ccc");
                        setTimeout(function() {
                            $('#begin_block').css("border", "3px solid #76EE00");
                        }, 500);
                    }, 1000);

                    setTimeout(function() {
                        clearInterval(Interval_5);
                    }, 2100);
                }
//------------------------------------------------------------------------------Algorithm                
                if (k === 5) {
                    $('#begin_block').css("border", "1px solid #ccc");
                    $('#end_block').css("height", "100px");
                }

                if (k === 7) {
                    $('#btn_check').removeClass('disabled');
                    $('#btn_check').on('click', function() {
                        if ((mainCode1[0][0] === 'алг') && (mainCode1[1][0] === 'нач') && (mainCode1[2][0] === 'кон')) {
                            console.log('Ты ввел все правильно! Жми "Далее"!');
                            $('#end_block').html('Ты ввел все правильно! Жми "Далее"!');
                        }
                        else
                            $('#end_block').html('Ты допустил ошибку. Попробуй еще раз)');
                    });
                }

                if (k === 8) {
                    $('#btn_check').removeClass('disabled');
                    $('#btn_check').on('click', function() {
                        if ((mainCode1[0][0] === 'алг') && (mainCode1[0][1] !== '') && (mainCode1[1][0] === 'нач') && (mainCode1[2][0] === 'кон')) {
                            $('#end_block').html('Ты ввел все правильно! Жми "Далее"!');
                        }
                        else
                            $('#end_block').html('Ты допустил ошибку. Попробуй еще раз)');
                    });
                }
                if (k === 9) {
                    $('#btn_check').removeClass('disabled');
                    $('#btn_check').on('click', function() {
                        if ((mainCode1[0][0] === 'использовать') && (mainCode1[0][1] === 'Робот') && (mainCode1[1][0] === 'алг') && (mainCode1[2][0] === 'нач') && (mainCode1[2][0] === 'кон')) {
                            $('#end_block').html('Ты ввел все правильно! Молодец!');
                        }
                        else
                            $('#end_block').html('Ты допустил ошибку. Попробуй еще раз)');
                    });
                }

                break;
            }
        }
    });

//------------------------------------------------------------------------------Pressing th button "btn_prev"
    $('#btn_prev').on('click', function() {

//------------------------------------------------------------------------------Checking block visibility
        for (var k = (array_steps.length - 1); k > 0; k--) {
            if ((k - 1) === 0)
                $('#btn_prev').addClass('disabled');
            if (k === (array_steps.length - 1))
                $('#btn_next').removeClass('disabled');
            if ($(array_steps[k]).hasClass('visible_block')) {
                $(array_steps[k]).removeClass('visible_block').addClass('visible_none');
                $(array_steps[k - 1]).removeClass('visible_none').addClass('visible_block');

//------------------------------------------------------------------------------Return_to_first_step
                if (k === 1) {
                    $('.column_1').css("border", "none");
                }
//------------------------------------------------------------------------------Clearing_interpreter_block
                if (k === 2) {
                    $('.column_1').css("border", "3px solid #76EE00");
                    $('#input_output').css("border", "1px solid #ccc");
                }
//------------------------------------------------------------------------------Clearing_input_output_block
                if (k === 3) {
                    $('#input_output').css("border", "3px solid #76EE00");
                    $('#id_error').css("border", "1px solid #ccc");
                }
//------------------------------------------------------------------------------Clearing_error_block
                if (k === 4) {
                    $('#id_error').css("border", "3px solid #76EE00");
                    $('#end_block').css("border", "1px solid #ccc");
                }
//------------------------------------------------------------------------------Clearing_end_block
                if (k === 5) {
                    $('#end_block').css("height", "100px");
                    $('#end_block').css("border", "3px solid #76EE00");
                    $('#begin_block').css("border", "1px solid #ccc");
                }
//------------------------------------------------------------------------------Clearing_buttons_block              
                if (k === 6) {
                    $('#end_block').css("height", "96px");
                    $('#begin_block').css("border", "3px solid #76EE00");
                }

                break;
            }
        }
    });

});
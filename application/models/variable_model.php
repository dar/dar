<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Variable_model extends CI_Model {
    
    public 
    
    function __construct() {
        parent::__construct();
    }
    
    
    public function get() {
        $this->db->select('*');
        $this->db->from('sp_variable');
        $result = $this->db->get();
        return $result->result();
    }
    
    public function save($data) {
        if($this->db->insert_batch('sp_variable', $data)) {
            return true;
        } 
        return false;
    }
    
    public function delete($name) {
        $this->db->where('name', $name);
        if($this->db->delete('sp_variable')) {
            return true;
        } 
        return false;
    }
    
    public function delete_all() {
        if($this->db->empty_table('sp_variable')) {
            return true;
        }
        return false;
    }
}
function robotWork(string) {
    var obstanovka = $('.robot-pole').find('div');
    
    for (var i = 0; i < string.length; i++) {

        if (string[i] === 'Робот') {
            if ($('.robot').css('display') === 'none') {
                $('#robot').trigger('click');
            }
            if($('#interpreter').attr('data-algorithm') === 'wave') {
                reset_the_robot_way();
                reset_progress();
            }
            if($('#interpreter').attr('data-algorithm') === 'energy') {
                reset_the_robot_way();
                reset_progress();
            }
            if($('#interpreter').attr('data-algorithm') === 'paint') {
                reset_count_paint_cell();
                reset_progress();
            }
        }
        if (string[i] === 'вправо') {
            var result = right(obstanovka, numberRobotKletka, numberRobotLine);
            
            if (!result) {
                $('#id_error').append("Хода вправо нет!<br/>");
                error_line();
            } else {
                step_robot();
                $('#id_error').empty();
            }
        }
        if (string[i] === 'влево') {
            var result = left(obstanovka, numberRobotKletka, numberRobotLine);
            
            if (!result) {
                $('#id_error').append("Хода влево нет!<br/>");
                error_line();
            } else {
                step_robot();
                $('#id_error').empty();
            }
        }
        if (string[i] === 'вниз') {
            var result = down(obstanovka, numberRobotKletka, numberRobotLine);
            
            if (!result) {
                $('#id_error').append("Хода вниз нет!<br/>");
                error_line();
            } else {
                step_robot();
                $('#id_error').empty();
            }
        }
        if (string[i] === 'вверх') {
            var result = up(obstanovka, numberRobotKletka, numberRobotLine);
            if (!result) {
                $('#id_error').append("Хода вверх нет!<br/>");
                error_line();
            } else {
                $('#id_error').empty();
                step_robot();
            }
        }
        if (string[i] === 'закрасить') {
            var obs = obst(obstanovka, numberRobotKletka, numberRobotLine);
            var posRobo = robotPosition(obs.kletka);
            $(obs.kletka[posRobo.i][posRobo.j].element).addClass('blue-kletka');
            count_paint_cell($(obs.kletka[posRobo.i][posRobo.j].element).attr('data-cell'));
        }
        
        if(string[i] === 'кон') {
            if($('#interpreter').attr('data-algorithm') === 'wave') {
                rating_task_for_way();
            }
            if($('#interpreter').attr('data-algorithm') === 'energy') {
                rating_task_for_way();
            }
            if($('#interpreter').attr('data-algorithm') === 'paint') {
                rating_task_for_paint();
            }
        }
    }
}


function isWall(condition) {
    if(condition.length !== 0) {
        var obstanovka = $('.robot-pole').find('div');
        var cond = condition.join(' ');
        var result;
        if(cond === 'слева свободно') {
            return isLeftWall(obstanovka, numberRobotKletka, numberRobotLine);
        }
        if(cond === 'справа свободно') {
            return isRightWall(obstanovka, numberRobotKletka, numberRobotLine);
        }
        if(cond === 'снизу свободно') {
            return isDownWall(obstanovka, numberRobotKletka, numberRobotLine);
        }
        if(cond === 'cверху свободно') {
            return isUpWall(obstanovka, numberRobotKletka, numberRobotLine);
        }
        if(cond === 'cлева стена') {
            result = isLeftWall(obstanovka, numberRobotKletka, numberRobotLine);
            return result === true ? false : true;
        }
        if(cond === 'справа стена') {
            result = isRightWall(obstanovka, numberRobotKletka, numberRobotLine);
            return result === true ? false : true;
        }
        if(cond === 'снизу стена') {
            result = isDownWall(obstanovka, numberRobotKletka, numberRobotLine);
            return result === true ? false : true;
        }
        if(cond === 'сверху стена') {
            result = isUpWall(obstanovka, numberRobotKletka, numberRobotLine);
            return result === true ? false : true;
        }
        if(cond === 'клетка закрашена') {
            var obs = obst(obstanovka, numberRobotKletka, numberRobotLine);
            var posRobo = robotPosition(obs.kletka);
            if($(obs.kletka[posRobo.i][posRobo.j].element).hasClass('blue-kletka')) {
                return true;
            } else {
                return false;
            }
        }
        
        if(cond === 'клетка чистая') {
            var obs = obst(obstanovka, numberRobotKletka, numberRobotLine);
            var posRobo = robotPosition(obs.kletka);
            if($(obs.kletka[posRobo.i][posRobo.j].element).hasClass('blue-kletka')) {
                return false;
            } else {
                return true;
            }
        }
    } 
    
    return false;
}

/*
 * Функция возвращает температуру клетки
 * @returns {Number}
 */
function temperature() {
    var obstanovka = $('.robot-pole').find('div');
    var obs = obst(obstanovka, numberRobotKletka, numberRobotLine);
    var posRobo = robotPosition(obs.kletka);
    var result = $(obs.kletka[posRobo.i][posRobo.j].element).attr('data-temperature');
    if(result === undefined) {
        return 0;
    } else {
        return parseFloat(result);
    }
}

function radiation() {
    var obstanovka = $('.robot-pole').find('div');
    var obs = obst(obstanovka, numberRobotKletka, numberRobotLine);
    var posRobo = robotPosition(obs.kletka);
    var result = $(obs.kletka[posRobo.i][posRobo.j].element).attr('data-radiation');
    if(result === undefined) {
        return 0;
    } else {
        return parseFloat(result);
    }
}

/**
 * Функции генерации матрицы смежности для обстановки робота
 * @param {object} elem элемент содержащий обстановку
 * @returns {mixed} false если элемент не существует 
 *                  array если элемент существует и обстановка задана
 */
function get_matrix_for_the_environment(elem) {
    if ($(elem).length > 0) {
        var size = $(elem).find('.robot-kletka').length;
        var matrix = [];
        var i, k;
        for (i = 1; i <= size; i++) {
            matrix[i] = [];
            for (k = 1; k <= size; k++) {
                matrix[i][k] = 0;
                if (i === k) {
                    matrix[i][k] = 1;
                }
            }
        }
        var walls = $(elem).find('.robot-stena-vert');
        var walls2 = $(elem).find('.robot-stena-hor');
        [].push.apply(walls, walls2);
        walls.each(function(key, value) {
            var jump = $(value).attr('data-jump').split('_');
            if (!$(value).hasClass('red-stena')) {
                i = parseInt(jump[0]);
                k = parseInt(jump[1]);
                matrix[i][k] = 1;
                matrix[k][i] = 1;
            }
        });

        return matrix;
    }

    return false;
}

/**
 * 
 * Функция для задания матрицы весов текущей обстановки поля
 * 
 * @param {object} elem объект содержит данные поля
 * @returns {mixed} matrix or false матрицу весов или ошибку
 */
function get_matrix_for_the_energy_task(elem) {
    var adjacency_matrix = get_matrix_for_the_environment(elem);
    var i = 1, k = 1, cell = 1;

    var weight_matrix = get_matrix_for_the_environment(elem);
    while (i < adjacency_matrix.length) {
        k = 1;
        while (k < adjacency_matrix[i].length) {
            if (adjacency_matrix[i][k] === 1 && i !== k) {
                weight_matrix[i][k] = $(elem).find('[data-cell=' + k + ']').attr('data-temperature');
                $(elem).find('[data-cell=' + k + ']').attr('title', weight_matrix[i][k]);
            }
            if (adjacency_matrix[i][k] !== 1 && i !== k) {
                weight_matrix[i][k] = Number.POSITIVE_INFINITY;
            }
            if (i === k) {
                weight_matrix[i][k] = 0;
                $(elem).find('[data-cell=' + k + ']').attr('title', weight_matrix[i][k]);
            }
            k++;
        }
        i++;
    }
    return weight_matrix;
}
/**
 * 
 * Функция, формирующая матрицу для текущей обстановки поля
 * 
 * @param {object} elem объект содержащий данные о текущем поле Робота
 * @returns {Array} духмерный массив - матрица задающая текущую обстановку поля
 */
function get_matrix_for_the_paint_task(elem) {
    var wells = $(elem).find('.robot-stena-hor');
    var well_value = $(wells[0]).attr('data-jump');
    well_value = well_value.split('_');
    var M = parseInt(well_value[1]) - 1; // количество столбцов
    well_value = $(wells[wells.length - 1]).attr('data-jump');
    well_value = well_value.split('_');
    var N = parseInt(well_value[1])/M; // количество строк
    
    var cells = $(elem).find('.robot-kletka');
    var index = 0;
    var matrix = [];
    for(var i = 0; i<N; i++) {
        matrix[i] = [];
        for(var j = 0; j<M; j++) {
            if($(cells[index]).hasClass('blue-kletka')) {
                matrix[i][j] = 1;
            } else {
                matrix[i][j] = 0;
            }
            index++;
        }
    }
    
    return matrix;
}

/**
 * 
 * Функции генерации обстановки для задачи нахождения кратчайшего пути
 * 
 * @param {object} elem объект содержащий данные поля
 * @returns {object} Начальную и конечную точку пути
 */
function generation_environment(elem) {
    //получаем матрицу задающую текущую обстановку Робота
    var matrix = get_matrix_for_the_environment(elem);
    var i = 1, k = 1, cells = [];
    
    //устанавливаем стенки на поле
    while (i < matrix.length) {
        k = 1;
        while (k < matrix[i].length) {
            if (matrix[i][k] === 1 && k !== i) {
                cells[cells.length] = k;
            }
            k++;

        }
        if (cells.length > 1) {
            var cell = Math.floor(Math.random() * cells.length);
            var value = cells[cell];
            matrix[i][value] = 0;
            matrix[value][i] = 0;
            $(elem).find('[data-jump="' + value + '_' + i + '"]').addClass('red-stena');
        }
        cells = [];
        i++;
    }
    
    //выбираем стартовую позицию Робота
    var A = Math.floor(Math.random() * ((matrix.length - 1) / 3));
    A = (A === 0) ? 1 : A;
    
    //выбираем финишную позицию Робота
    var B = matrix.length - 1 - Math.floor(Math.random() * ((matrix.length - 1) / 3));
    var img = $(elem).find('.img-robo');
    
    //перемещаем Робота в стартовую позицию и обозначаем финишную
    $(img).removeClass('img-robo');
    $(elem).find('[data-cell="' + A + '"]').addClass('img-robo');
    $(elem).find('[data-cell="' + B + '"]').html('B');

    return {'start': A, 'end': B}
}

/**
 * 
 * Функция генерация обстановки для задачи нохождения КП с заданной температурой клетки
 *
 * @param {object} elem объект содержащий массив с данными о клетках поля 
 * @param {object} box объект содержащий данные об элементе поля
 * @returns {оbject} {'start': A, 'end': B} стартовая и финишная позиция пути Робота
 */
function generation_environment_energy_task(elem, box) {
    
    //задаем стартовую позицию Робота
    var A = Math.floor(Math.random() * ((elem.length - 1) / 3));
    A = (A === 0) ? 1 : A; 
    //задаем финишную позицию Робота
    var B = elem.length - 1 - Math.floor(Math.random() * ((elem.length - 1) / 3));
    //перемещаем Робота в стартовую позицию и закрашиваем клетку обозначающую финишную позицию
    var img = $(box).find('.img-robo');
    $(img).removeClass('img-robo');
    $(box).find('[data-cell="' + A + '"]').addClass('img-robo');
    $(box).find('[data-cell="' + B + '"]').append('B').css('background-color', '#ccc');
    
    //для каждой клетки поля задаем температуру в диапозоне от 1 до 100
    elem.each(function(key, value) {
        var temperature = Math.floor(Math.random() * 99) + 1;
        $(value).attr('data-temperature', temperature);
        if (!$(value).hasClass('img-robo')) {
            $(value).append(temperature);
        }
    });
    return {'start': A, 'end': B}
}


/**
 * 
 * Функци задает текущую обстановку для задачи о заливке
 * 
 * @param {object} elem Объект содержащий данные о поле
 * @returns {generation_enviroment_paint_box.Anonym$4} start - стартовую клетку робота
 */
function generation_enviroment_paint_box(elem) {
    
    //определяем количество столбцов
    var wells = $(elem).find('.robot-stena-hor');
    var well_value = $(wells[0]).attr('data-jump');
    well_value = well_value.split('_');
    var M = parseInt(well_value[1]) - 1;
    //определяем количество строк
    well_value = $(wells[wells.length - 1]).attr('data-jump');
    well_value = well_value.split('_');
    var N = parseInt(well_value[1])/M;
    
    //задаем число для генерации обстановки
    var min_N_M = Math.min((M-2), (N-2));
    var k = Math.floor(Math.random()*(min_N_M-1))+1;
    
    //закрашиваем граничные клетки поля 
    var cells = $(elem).find('.robot-kletka');
    cells.each(function(key,value){
        if((key >= 0 && (key) <= M-1) 
                || (key >= (M*N-M) && key <= (M*N-1))
                || (key%M === 0 || (key+1)%M === 0)) {
            $(value).addClass('blue-kletka');
        }
    });
    
    //определяем оставшиеся не закрашенные клетки поля
    cells = $('.robot-kletka:not(.blue-kletka)');
    var i = 0;
    var index = 0;
    
    //закрашивеам k клеток через k
    while(i<cells.length) {
        for(var j = i; j<index*2*k+k; j++) {
            $(cells[j]).addClass('blue-kletka');
        }
        i = i + 2*k;
        index++;
    }
    cells = $('.robot-kletka:not(.blue-kletka)');
    
    //выбираем не закрашенную клетку
    var cell = Math.floor(Math.random()*(cells.length-1));
    
    //перемещаем в не закрашенную клетку Робота
    var img = $(elem).find('.img-robo');
    $(img).removeClass('img-robo');
    cell = $(cells[cell]).attr('data-cell');
    $('[data-cell="'+cell+'"]').addClass('img-robo');
    
    return {'start': cell}
}

/**
 * Функция реализующая волновой алгоритм
 * 
 * @param array matrix двумерный массив задающий смежную матрицу графа
 * @param int A номер клетки - начало пути
 * @param int B номер клетки - конец пути
 * @returns {object}
 */
function wave_algorithm(matrix, A, B) {
    var vertices = [];
    var i, j;
    //Начальная инициализация
    for (i = 1; i < matrix.length; i++) {
        vertices[i] = -1;
    }
    var oldFront = [], newFront = [];
    var t = 0;
    oldFront[oldFront.length] = A;
    vertices[A] = 0;


    //Собственно сам алгоритм
    var result;
    var the_end = false;
    while (the_end != true) {
        for (i = 0; i < oldFront.length; i++) {
            for (j = 1; j < matrix.length; j++) {
                if (matrix[oldFront[i]][j] === 1 && oldFront[i] !== j) {
                    if (vertices[j] === -1) {
                        vertices[j] = t + 1;
                        newFront[newFront.length] = j;
                    }
                }
            }
        }
        if (newFront.length === 0) {
            result = false;
            the_end = true;
            break;
        } else {
            var the_end = false;
            for (i = 0; i < newFront.length; i++) {

                if (newFront[i] === B) {
                    var ways = [];
                    var way = 1;
                    ways[way] = [];
                    var result = {};
                    t = t + 1;
                    result.length_way = t;

                    the_end = true;

                    var k = B, l = 1;
                    var back_step = [];
                    back_step[back_step.length] = B;
                    var vertices2 = vertices;
                    var back_step2 = [];
                    ways[way][t] = B;
                    while (back_step[0] !== A) {
                        for (var m = 0; m < back_step.length; m++) {
                            k = back_step[m];
                            for (l = 1; l < matrix[k].length; l++) {
                                if (matrix[k][l] === 1 && vertices[l] === (t - 1)) {
                                    back_step2[back_step2.length] = l;
                                    if (typeof ways[back_step2.length] === 'undefined') {
                                        ways[back_step2.length] = [];
                                    }
                                    ways[back_step2.length][t - 1] = l;

                                    vertices[l] = -1;
                                }
                            }

                        }
                        t = t - 1;
                        back_step = back_step2;
                        back_step2 = [];
                    }
                    t = result.length_way;
                    colors = [];
                }
            }
            if (the_end) {
                break;
            }
            oldFront = newFront;
            newFront = [];
            t = t + 1;
        }

    }
    //возвращаем длину найденного пути
    return result;

}

/**
 * 
 * Функция - реализация алгоритма Дейкстра
 * 
 * @param {array} matrix двумерный массив матрицы весов
 * @param {type} A номер клетки начала пути
 * @param {type} B номер клетки конца пути
 * @returns {object} result кратчайшие пути из точки A во все остальные и их длины
 */
function algorithm_dijkstra(matrix, A, B) {
    var visited = {}; //объект, содержащий посещенные вершины и их метку
    var vertices = []; //массив вершин
    var i, k;
    
    //начальная инициализация
    for (i = 1; i < matrix.length; i++) {
        if (i === A) {
            vertices[i] = 0;
        } else {
            vertices[i] = Number.POSITIVE_INFINITY;
        }
    }

    i = 1;

    var the_end = false;

    var min = 0;

    //реализация алгоритма
    while (!the_end) {


        min = min_mark(vertices, visited);

        k = 1;

        while (k < matrix[min.key].length) {
            var matrix_value = matrix[min.key][k];

            if (matrix[min.key][k] !== Number.POSITIVE_INFINITY && min.key !== k) {

                if (visited[k] === undefined) {
                    var new_mark = min.mark + parseInt(matrix[min.key][k]);
                    if (vertices[k] > new_mark) {
                        vertices[k] = new_mark;
                    }
                }
            }
            k++;
            if (k >= matrix[min.key].length) {

                visited[min.key] = vertices[min.key];
            }
        }//while k

        if (object_count(visited) >= (matrix.length - 1)) {

            the_end = true;

        }

    }
    var back_way = []; //массив востановления кратчайшего пути
    
    var index_back = 0;
    
    var last_visited = B;
    var related = [];
    the_end = false;
    
    while(!the_end) {
        
        k = 1;
        
        while(k < matrix[last_visited].length) {
           if(matrix[last_visited][k] !== Number.POSITIVE_INFINITY && k !== last_visited) {
               related[related.length] = k;
           } 
           k++;
        }
        var min_value = visited[last_visited];
        for(i = 0; i<related.length; i++) {
                if(visited[related[i]] < min_value) {
                    min_value = visited[related[i]];
                    last_visited = related[i];
                }
        }
        back_way[back_way.length] = last_visited;
        related = [];
        if(last_visited === A) {
            the_end = true;
        }
    }
    
    //возвращаем длину пути,
    //найденный путь,
    //все полученные метки для каждой вершины
    var result = {
        length_way: visited[B],
        back_way : back_way,
        visited: visited
    }
    return result;
}

/**
 * 
 * Функция алгоритма заливки
 * 
 * @param {array} matrix двумерный массив задающий текущуб обстановку поля для задачи о заливке
 * @param {integer} A номер стартовой клетки Робота
 * @returns {algorithm_paint.result} новую матрицу и количество закрашенных клеток
 */
function algorithm_paint(matrix, A) {
    
    //получаем матрицу задающую текущую обстановку поля Робота
    var new_matrix = matrix;
    
    var Q = [];//массив для требующих изменения элементов
    
    //получаем количество строк
    var N = matrix.length;
    //получаем количество столбцов
    var M = matrix[0].length;
    
    //получаем координаты стартовой позиции Робота
    var Ai = parseInt(A/M);
    var Aj = parseInt(A-Ai*M-1);
    
    //реализация алгоритма
    if(matrix[Ai][Aj] == 1) {
        return false; //Робот находится на закрашенной клетке
    } else {
        var cell = A;
        var Ci, Cj; 
        Q[Q.length] = A;
        var k = 0;
        var count = 0;
        while(k<Q.length) {
            Ci = parseInt(Q[k]/M);
            Cj = parseInt(Q[k]-Ci*M-1);
            new_matrix[Ci][Cj] = 1;
            
            count++;
            Q.splice(k, 1);
            k++;
            if(k>=Q.length && Q.length !== 0) {
                k = 0;
            }
            if(new_matrix[Ci+1][Cj] === 0) {
                if ($.inArray(((Ci + 1) * M + Cj + 1), Q) === -1) {
                    Q[Q.length] = (Ci + 1) * M + Cj + 1; //добавляем номер клетки
                    k = 0;
                }
            }
            if(new_matrix[Ci-1][Cj] === 0) {
                if ($.inArray(((Ci - 1) * M + Cj + 1), Q) === -1) {
                    Q[Q.length] = (Ci - 1) * M + Cj + 1; //добавляем номер клетки
                    k = 0;
                }
            }
            if(new_matrix[Ci][Cj+1] === 0) {
                if ($.inArray((Ci * M + (Cj + 1) + 1), Q) === -1) {
                    Q[Q.length] = Ci * M + (Cj + 1) + 1; //добавляем номер клетки
                    k = 0;
                }
            }
            if(new_matrix[Ci][Cj-1] === 0) {
                if ($.inArray((Ci * M + (Cj - 1) + 1), Q) === -1) {
                    Q[Q.length] = Ci * M + (Cj - 1) + 1; //добавляем номер клетки
                    k = 0;
                }
            }
        }
    }
    
    
    var result = {
        matrix: new_matrix,
        count_cell_paint: count
    }
    
    return result;
}

/**
 * 
 * Функция, которая формирует оценку и отображает строку прогресса
 * для задачи о нахождении кратчайшего пути
 */
function rating_task_for_way() {
    
    //получаем матрицу текущей обстановки поля
    var matrix = get_matrix_for_the_environment($('.robot-pole'));
    
    if($('#interpreter').attr('data-algorithm') === 'energy') {
        matrix = get_matrix_for_the_energy_task($('.robot-pole'));
    }
    
    var presentWay = {};
    
    //получаем положение Робота и положени финишной клетки
    presentWay.start = parseInt(find_cell_robot());
    presentWay.end = parseInt(searchWay.end);
    
    //получаем системное решение задачи
    var system_solve = wave_algorithm(matrix, searchWay.start, searchWay.end);
    if($('#interpreter').attr('data-algorithm') === 'energy') {
        system_solve = algorithm_dijkstra(matrix, searchWay.start, searchWay.end);
    }
    
    var solve_length = robotWay.length;
    if($('#interpreter').attr('data-algorithm') === 'energy') {
        solve_length = 0;
        for(var i=0; i<robotWay['way'].length; i++) {
            var temperature = $('[data-cell="'+robotWay['way'][i]+'"]').attr('data-temperature');
            solve_length = solve_length + parseInt(temperature);
        }
        
    }
    
    //формируем оценки и сообщения в зависимости от полученного решения
    var rating = {};
    if(presentWay.start === presentWay.end && system_solve.length_way === solve_length) {
        rating.mark = 100;
        rating.deflection = 0;
        rating.message = "<span class='text-success'><b>Молодец! Кратчайший путь найден!</b></span></br>";
    }
    
    var mistake, deflection;
    
    if(presentWay.start === presentWay.end && system_solve.length_way !== solve_length) {
        mistake = Math.abs(solve_length - system_solve.length_way);
        deflection = Math.round((mistake/system_solve.length_way)*100);
        rating.mark = 100 - deflection;
        if(rating.mark >= 100) {
            rating.mark = 100 - Math.floor((Math.random())*10+1);
        }
        rating.deflection = deflection;
        rating.message = "<span class='text-warning'><b>Молодец! Ты нашел путь, но он не самый короткий!</b></span><br/>";
    }
    
    if(presentWay.start !== presentWay.end) {
        var restWay = wave_algorithm(matrix, presentWay.start, searchWay.end);
        if ($('#interpreter').attr('data-algorithm') === 'energy') {
            restWay = algorithm_dijkstra(matrix, searchWay.start, searchWay.end);
        }
        mistake = Math.abs(solve_length - (system_solve.length_way - restWay.length_way));
        deflection = Math.round((mistake/Math.abs((system_solve.length_way - restWay.length_way)))*100);
        if(deflection !== 0) {
            rating.mark = (100 - deflection)/2;
            rating.deflection = deflection;
            if(rating.mark >=70 || rating.mark <=0) {
                rating.mark = Math.floor((Math.random())*50+25);
            }
            rating.message = "<span class='text-danger'><b>Робот не дошел до назначенного пункта и прошел путь, который не является коротким!</b></span><br/>";
        } else {
            rating.mark = ((1 - (solve_length - system_solve.length_way)) * 100);
            if(rating.mark >=90) {
                rating.mark = 100 - Math.floor((Math.random())*20+5);
            }
            rating.deflection = 100 - rating.mark;
            rating.message = "<span class='text-warning'><b>Робот не дошел до назначенного пункта, но идет по верному пути!</b></span><br/>";
        }
    }
    
    show_progress_solve_task(rating); //отображаем строку прогресса

}

/**
 * Функция построения оценки для задачи о заливке
 * 
 */
function rating_task_for_paint() {
    //матрица текущей обстановки поля, полученой после решения задачи
    var matrix = get_matrix_for_the_paint_task($('.robot-pole')); 
    var solve = algorithm_paint(startEnvironmentPaint, searchWay.start);
    var verification = true;
    var sum1 = 0;
    var sum2 = 0;
    for(var i=0;i<matrix.length;i++) {
        for(var j=0; j<matrix[i].length; j++) {
            sum1 = sum1 + startEnvironmentPaint[i][j];
            sum2 = sum2 + matrix[i][j];
            if(matrix[i][j] !== startEnvironmentPaint[i][j]) {
                verification = false;
            }
        }
    }
    
    var count = 0;
    for(var key in countPaintCell) {
        count = count + countPaintCell[key];
    }
    
    var rating = {};
    //получаем оценки в зависимости от результатов решения задачи
    if(solve.count_cell_paint === count && verification === true) {
        rating.mark = 100;
        rating.deflection = 0;
        rating.message = "<span class='text-success'><b>Молодец! Поле закрашено!</b></span></br>";
    }
    
    var mistake, deflection;
    
    if(verification === true && solve.count_cell_paint !== count) {
        mistake = Math.abs(count - solve.countPaintCell);
        deflection = (mistake/solve.count_cell_paint)*100;
        rating.mark = 100-parseInt(deflection/2);
        if(rating.mark >= 100) {
            rating.mark = 100 - 15;
        }
        rating.deflection = parseInt(deflection);
        rating.message = "<span class='text-warning'><b>Поле закрашено! Но алгоритм является не оптимизированным, так как закрашивает одну и ту же клетку подряд.</b></span></br>";
    }
    
    if(verification === false) {
        mistake = Math.abs(sum2 - sum1);
        deflection = (mistake/sum1)*100;
        rating.mark = 100-parseInt(deflection);
        rating.deflection = parseInt(deflection);
        if(rating.mark <= 0) {
            rating.mark = 10;
        }
        if(rating.mark >= 70) {
            rating.mark = 45;
        }
        rating.message = "<span class='text-danger'><b>Область закрашена не полностью!</b></span></br>";
    }
    
    show_progress_solve_task(rating); //отображаем строку прогресса
}

/**
 * Функция отображающая прогресс решения задачи
 * @param {object} rating оценка и откланение
 * @returns {undefined}
 */
function show_progress_solve_task(rating) {
        var length_section = {};
        var one_section = 100/3;
        console.log(rating);
    if(rating.mark < one_section) {
        length_section.first = rating.mark;
        length_section.second = 0;
        length_section.third = 0;
    } else if(rating.mark < 2*one_section) {
        length_section.first = one_section;
        length_section.second = rating.mark - one_section;
        length_section.third = 0;
    } else if(rating.mark < 100){
        length_section.first = one_section;
        length_section.second = one_section;
        length_section.third = rating.mark - 2*one_section;
    } else {
        length_section.first = one_section;
        length_section.second = one_section;
        length_section.third = one_section;
    }
    
    var progress = $('#rating-for-task');
    $(progress).find('#rating-danger').css('width', length_section.first+'%');
    $(progress).find('#rating-warning').css('width', length_section.second+'%');
    $(progress).find('#rating-success').css('width', length_section.third+'%');
    $('.row_end').append(rating.message);
}

/**
 * Функция поиска минимальной метки в массиве
 * 
 * @param {array} vertices массив вершин
 * @returns {min_mark.Anonym$8} ключ и минимальное значение массива
 */
function min_mark(vertices, visited) {
    var min = Number.POSITIVE_INFINITY;
    var key = 1;
    for (var i = 1; i < vertices.length; i++) {
        if (vertices[i] < min) {
            if (visited[i] === undefined) {
                min = vertices[i];
                key = i;
            }
        }
    }
    return {key: key, mark: min}
}

/**
 * 
 * Функция подсчета количества элементов в объекте
 * 
 * @param {object} object 
 * @returns {Number}
 */
function object_count(object) {
    var count = 0;
    var value;
    for (value in object) {
        count++;
    }
    return count;
}



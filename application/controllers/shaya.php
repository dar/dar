<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Shaya extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('variable_model');
    }

    public function index() {
        $this->load->view('shaya');
    }

    public function words() {
        $words = $this->input->post('words');
        if (count($words != 0)) {
            $is_words = $this->variable_model->get();
            foreach ($is_words as $key => $value) {
                foreach ($words as $key2 => $word) {
                    if ($value->name == $word) {
                        unset($words[$key2]);
                    }
                }
            }
            sort($words);
            $data = array();
            foreach ($words as $word) {
                $result = array(
                    'name' => $word,
                    'id_type' => 1,
                    'id_algorithm' => 1
                );
                array_push($data, $result);
            }

            if (count($data) == 0) {
                $result = array(
                    'status' => 'ok',
                    'message' => 'Данных для добавления нет!'
                );
                echo json_encode($result);
                return;
            }

            if ($this->variable_model->save($data)) {
                $result = array(
                    'status' => 'ok',
                    'message' => 'Данные успешно добавлены'
                );
                echo json_encode($result);
                return;
            } else {
                $result = array(
                    'status' => 'error',
                    'message' => 'Ошибка сохранения!'
                );
                echo json_encode($result);
                return;
            }
        }
    }

    public function variable_delete($var) {
        if (!empty($var)) {
            if ($this->variable_model->delete($var)) {
                $result = array(
                    'status' => 'ok',
                    'message' => 'Переменная успешно удалена'
                );
            } else {
                $result = array(
                    'statsu' => 'error',
                    'message' => 'Ошибка удаления!'
                );
            }
            echo json_encode($result);
            return;
        } else {
            if ($this->variable_model->delete_all()) {
                $result = array(
                    'status' => 'ok',
                    'message' => 'Переменная успешно удалена'
                );
            } else {
                $result = array(
                    'statsu' => 'error',
                    'message' => 'Ошибка удаления!'
                );
            }
            echo json_encode($result);
            return;
        }
    }

}

//------------------------------------для условий и циклов--------------------------------
var number_constructions = 0; //количество конструкций всего
var nesting_level = 0;  //уровень вложенности
var index_Group=0; //индекс группы
var number_groups = 0;
var construction_active = []; //----------------------------------активные конструкции-------------------------------
    construction_active[0] = {index_con: -1, array_of_variables: [], number_of_variables:1};
    construction_active[0].array_of_variables[0] = [];
    construction_active[0].array_of_variables[1] = [];
    construction_active[0].array_of_variables[2] = [];
var number_active_constr = 0;
var construction = [];
var number_last_constr = []; //--------------------номер последней кострукции
//----------------------------------------------------------------------------------------------------

function main_function(Code, str_number){
    
    var n = end_search(Code);

    console.log('--------------------команды------------------------');
    console.log(Code);
    //--------------------------------------описание-------------------------------------
    if(language.typeVariable(Code[0])) {

        var numberVars = number_of_vars(Code);
        var num = 1;
        
        for(var j = 0; j <= numberVars; ++j){
            
            var variable_save = -1;
            
            for(var i = 0; i < construction_active.length; ++i){                     //куда записываем
                var variable_save =agreement_search(construction_active[i].array_of_variables[0], Code[num]);
                if(variable_save !== -1) break;     
            }
            
            if(variable_save!==(-1)) {
                $("#id_error").html($("#id_error").html()+'<span class = "active-string">Предупреждение:</span> такое имя переменной уже используется:' + Code[1] + '<br>');
                construction_active[number_active_constr].array_of_variables[1][variable_save] = Code[0];
            }
            else {

                construction_active[number_active_constr].array_of_variables[0][construction_active[number_active_constr].number_of_variables-1] = Code[num];
                construction_active[number_active_constr].array_of_variables[1][construction_active[number_active_constr].number_of_variables-1] = Code[0];
                construction_active[number_active_constr].number_of_variables ++;
                
                $(".row_end").html($(".row_end").html() + 'Описана переменная "' + Code[num] +'" '+ get_nameType(Code[0])+' типа <br> ');  //--------------------------строка состояния
            }
            
            num+=2;
        }
    }
    
    //--------------------------------------присваивание----------------------------------
    if(Code[1] === ':='){
        var variable_save = {row: 0, cell: -1}, str_for_save = '';       
        for(var j = 0; j < construction_active.length; ++j){                     //куда записываем
            variable_save.row = j; variable_save.cell =  agreement_search(construction_active[j].array_of_variables[0], Code[0]);
            if(variable_save.cell !== -1) break;     
        }
        
        if(variable_save.cell === (-1)) return {ifchange: -1, result_play: -2}
        
        else{
            
            var eval_str;
            if(Code[2] === "температура") {   //----------------температура, робот
                eval_str = temperature();
            }
            
            else if (Code[2] === "радиация"){   //----------------радиация, робот
                eval_str = radiation();
            }
            
            else {
                var variable_find = {row: 0, cell: -1};
        
                for (var i = 2; i < n; ++i){
                    if (language.variableName(Code[i])) {
                        for(var j = 0; j < construction_active.length; ++j){               
                            variable_find.row = j; variable_find.cell = agreement_search(construction_active[j].array_of_variables[0], Code[i]);
                            if(variable_find.cell  !== -1) break;     
                        }
                        str_for_save += construction_active[variable_find.row].array_of_variables[2][variable_find.cell];
                    }
                    else str_for_save += Code[i];
                }

                eval_str = eval(str_for_save);
                    if(!eval_str) eval_str = str_for_save;  //-----попытка для строчных
            
            }        
            
        var is_types = is_type(construction_active[variable_save.row].array_of_variables[1][variable_save.cell], type_variable(eval_str));//---согласование типов
            if(is_types === 1)  
                    construction_active[variable_save.row].array_of_variables[2][variable_save.cell] = eval_str; 
            else if(is_types === 2) construction_active[variable_save.row].array_of_variables[2][variable_save.cell] = ''+eval_str+'';
                else return {ifchange: -1, result_play: -3}
        }
    
    }
    
        //--------------------------------------ввод-------------------------------------
    
    //-------------------------еще осталось:
    //----------------1) согласование типов
    //----------------2) проверка существования переменной------------
    if(Code[0] === 'ввод') {
        
        //------------------------------подсвечиваем и блокируем--------------------
        stringCode = $('#interpreter').find('div');
        $(stringCode).eq(str_number).removeClass('active-string');
        $(stringCode).eq(str_number).addClass('active-string-in_out'); //подсвечиваем строку бледным
        $("#play_button").prop("disabled", true);        //останавливаем выполнение
        
        var numberVars = number_of_vars(Code);
        var num = 1;
        
        $("#id_in_out").append("<span class = 'comm'>!ввод значений(строка "+ (str_number+1) + ")</span><br/>");
        
         i_var_save = {row: 0, cell: -1}
        
        for(var j = 0; j <=numberVars; ++j){
            
            var variable_save = {row: 0, cell: -1}, str_for_save = '';       
            for(var i = 0; i < construction_active.length; ++i){                     //куда записываем
                variable_save.row = i; variable_save.cell =  agreement_search(construction_active[i].array_of_variables[0], Code[num]);
                if(variable_save.cell !== -1) break;     
            }

            if(variable_save.cell === (-1)) return {ifchange: -1, result_play: -2}

            else{
                        if(i_var_save.cell === -1) {
                            i_var_save.cell = variable_save.cell;
                            i_var_save.row = variable_save.row;
                        }
                        $("#id_in_out").append("<input type = 'text' class='vvod' id = 'input_in_i" + (str_number) + 'j'+j+"'/><br/>");
                    }
            
            num+=2;
        }
        $("#input_in_i" + (str_number)+ 'j'+ i_var_save.cell).focus();
    }
    
            //--------------------------------------вывод-------------------------------------

    if(Code[0] === 'вывод') {

            var numberVars = number_of_vars(Code);
            var num = 1;

            $("#id_in_out").append("<span class = 'comm'>!вывод значений(строка "+ (str_number+1) + ")</span><br/>");

            
            var code_work = [];
        
        var variable_find = {row: 0, cell: -1};
        
        for (var i = 1; i < n; ++i){
            if (language.variableName(Code[i])) {
                for(var j = 0; j < construction_active.length; ++j){               
                    variable_find.row = j; variable_find.cell = agreement_search(construction_active[j].array_of_variables[0], Code[i]);
                    if(variable_find.cell  !== -1) break;     
                }
                code_work[i] = construction_active[variable_find.row].array_of_variables[2][variable_find.cell];
            }
            else  code_work[i] = Code[i];
        }
        
        
        console.log('--------------------------');
        console.log(code_work);
            for(var j = 0; j <=numberVars; ++j){
                
                //------1.Строка---------------
                if(Code[num][0] === '"') {
                    $("#id_in_out").append("<input value = "+Code[num]+" type = 'text' class='vivod' id = 'input_out_"+j+"'/><br/>");
                    num+=2;
                }
                
                else{
                //---------2. Операция/ переменная--------------
                    
                    var eval_str = '';

                    
                    for(var i = num; i < n; ++i){
                        if (code_work[i] === ',') break;
                        else {
                            eval_str += code_work[i];
                        }
                    }
                    eval_str = eval(eval_str);
                    
                    $("#id_in_out").append("<input value = "+eval_str+" type = 'text' class='vivod' id = 'input_out_"+j+"'/><br/>");
                    
                    num+=i;
                    
                }

            }
    }
    
    
    
    //--------------------------------------условия-------------------------------------
    
    if(Code[0] === 'если'){
        //------------------------------------добавляем в активные-------------------------------
        for(var i = 0; i < construction.length; ++i) 
            if(construction[i].begining === str_number) {
                number_active_constr++;
                construction_active[number_active_constr] = {index_con: i, array_of_variables: [], number_of_variables:1};
                construction_active[number_active_constr].array_of_variables[0] = [];
                construction_active[number_active_constr].array_of_variables[1] = [];
                construction_active[number_active_constr].array_of_variables[2] = [];
                break;
            }
        //-------------------проверка выполнимости условия-----------------------
            var eval_str;
        
        eval_str = if_ifs_and_wiles_true(construction[construction_active[number_active_constr].index_con].condition);//eval(eval_str);
        
        //-----------------------------------------------если все ок - выполняем следующую строку----------------
        if(eval_str) {
            construction[construction_active[number_active_constr].index_con].isChange = -1;
            if (Code.length !== 2+construction[construction_active[number_active_constr].index_con].condition.length){   //------если написано прям на этой же строке------
                    var Code1 = [], j =0;
                    for (var i =1+ construction[construction_active[number_active_constr].index_con].condition.length; i < Code.length; ++i, ++j){
                        Code1[j] = Code[i];
                    }
                console.log('new code-------------------------');
                console.log(Code1);
                    
                    return main_function(Code1, str_number);
                }
                else
                    return {ifchange: str_number +1, result_play: 1}
            }
        
        //---------------------------если не ок - кидаем на строку с "иначе" и проверяем "иначе"
        else {
            construction[construction_active[number_active_constr].index_con].isChange = 1;
            if(construction[construction_active[number_active_constr].index_con].index_change !== -1) return {ifchange: construction[construction_active[number_active_constr].index_con].index_change, result_play: 1}
            else return {ifchange: construction[construction_active[number_active_constr].index_con].ending, result_play: 1}
        }
    }
    
    
    if(Code[0] === 'иначе'){
        //-------------------проверка того, нужно ли выполнять этот блок-----------------------
            if (construction[construction_active[number_active_constr].index_con].isChange === 1){
                //-------------------------------------если нужно---------------
                if (Code.length !== 2){   //------если написано прям на этой же строке------
                    var Code1 = [];
                    for (var i = 1; i < Code.length; ++i){
                        Code1[i-1] = Code[i];
                    }
                    
                    return main_function(Code1, str_number);
                }
                else
                    return {ifchange: -1, result_play: 1}
            }
        //----------------------------------если не нужно-----------------------------
            else {
                var stringCodeHere = $('#interpreter').find('div');
                $(stringCodeHere).eq(str_number).removeClass('active-string');
                $(stringCodeHere).eq(construction[construction_active[number_active_constr].index_con].ending).addClass('active-string');
                return {ifchange: construction[construction_active[number_active_constr].index_con].ending, result_play: 1}
            }
    }
    
    //----------------------------все--------------------------------
      if(Code[0] === 'все'){
            //-------------------------------------удаляем из активных----------------------
            construction_active.pop();
            number_active_constr--;
                    return {ifchange: -1, result_play: 1}
        }
    
    //-------------------------то---------------------------------
    if(Code[0] === 'то'){

                if (Code.length !== 2){   //------если написано прям на этой же строке------
                    var Code1 = [];
                    for (var i = 1; i < Code.length; ++i){
                        Code1[i-1] = Code[i];
                    }
                    
                    return main_function(Code1, str_number);
                }
                else
                    return {ifchange: -1, result_play: 1}
    }
    
    //---------------------------------------------------------------------циклы-----------------------------------------------------------
    
    if(Code[0] === 'нц'){
        //--------------------0) Проверяем, есть ли уже в актиных данный цикл:
        //------------------------а) Если да - инкремируем переменную;-------------------------      +
        //------------------------б) Если нет - добавляем в активные;---------------------------      +
        //--------------------1) Проверяем условие выполнимости:
        //------------------------а) Если да - продолжаем выполнение, ifChnge = 1;
        //------------------------б) Если нет - ifChange = -1, идем на кц
        //--------------------2) в текущем уровне сохраняем переменную цикла---------
        //--------------------3) Выполняем--------------------------------------------------------------
        //--------------------4) Когда кц:
        //------------------------а) 
         //------------------------------------добавляем в активные-------------------------------
        //-------------------------------------------нц---------------------------------
        
        var var_this = -1;
        for(var i = 0; i < construction.length; ++i){
            if(construction[i].begining === str_number){
                var_this = i;
                break;
            }
        }
        
        if(construction_active[number_active_constr].index_con !== var_this){
            for(var i = 0; i < construction.length; ++i) 
                if(construction[i].begining === str_number) {
                    number_active_constr++;
                    construction_active[number_active_constr] = {index_con: i, array_of_variables: [], number_of_variables:1};
                    construction_active[number_active_constr].array_of_variables[0] = [];
                    construction_active[number_active_constr].array_of_variables[1] = [];
                    construction_active[number_active_constr].array_of_variables[2] = [];
                    if(construction[construction_active[number_active_constr].index_con].typeConstruction === 1){ //-------------для-----------
                        construction_active[number_active_constr].number_of_variables = 2;
                        construction_active[number_active_constr].array_of_variables[0][0] = construction[construction_active[number_active_constr].index_con].condition[0];

                        construction_active[number_active_constr].array_of_variables[1][0] = 'цел';
                        construction_active[number_active_constr].array_of_variables[2][0] = construction[construction_active[number_active_constr].index_con].index_change;
                    }
                    break;
                }
        }
        else {
            if(construction[construction_active[number_active_constr].index_con].typeConstruction === 1){//-------------для-----------
                construction[construction_active[number_active_constr].index_con].index_change = eval(construction[construction_active[number_active_constr].index_con].index_change)+1;
                construction_active[number_active_constr].array_of_variables[2][0] = construction[construction_active[number_active_constr].index_con].index_change;
            }
        }
        //-------------------проверка выполнимости условия-----------------------
            var eval_str = '';
 
        eval_str = if_ifs_and_wiles_true(construction[construction_active[number_active_constr].index_con].condition);//eval(eval_str);
        
        //-----------------------------------------------Если да - продолжаем выполнение, ifChange = 1;----------------
        if(eval_str) {
            construction[construction_active[number_active_constr].index_con].isChange = 1;
            return {ifchange: str_number +1, result_play: 1}
        }
        
        //------------------------------------------------Если нет - ifChange = -1, идем на кц---------------------------------
        else {
            construction[construction_active[number_active_constr].index_con].isChange = -1;
            return {ifchange: construction[construction_active[number_active_constr].index_con].ending, result_play: 1}
        }
    }
    
    //----------------------------------------------------кц-------------------------------------------------------------
    
    if(Code[0] === 'кц'){
            //-------------------------------------удаляем из активных----------------------
        if(construction[construction_active[number_active_constr].index_con].isChange === -1){
            construction[construction_active[number_active_constr].index_con].index_change = construction[construction_active[number_active_constr].index_con].nesting_level;
            construction_active.pop();
            number_active_constr--;
            return {ifchange: -1, result_play: 1}          
        }
        else {
            return {ifchange: construction[construction_active[number_active_constr].index_con].begining, result_play: 1}
        }

    }
           
    //------------------------------------------
    console.log('---------------------------constructions------------------------');
    console.log(construction);
    console.log('---------------------------construction_active------------------------');
    console.log(construction_active); 
    
    return {ifchange: -1, result_play: 1}
}

 //------------------------------поиск необходимого имени в массиве переменных-----------------------------------------
function agreement_search (array_search, element_search){ 
    for(var i = 0; i < array_search.length; ++i){
        if(array_search[i] === element_search) {
            return i;
        }
    }
    return -1;
}

//-----------------------------------------поиск ";" или "_|"------------------------
function end_search (array_search){
    for(var i = 0; array_search.length-1; ++i){
        if(array_search[i] === ';' || array_search[i] === '\n') {
            return i;
        }
    }
}

function type_variable(str_analize){   //--------------------анализ типа получаемых данных
    if (language.int_variables(str_analize)) {return 'цел';}
    if (language.float_variables(str_analize)) {return 'вещ';}
    if (language.bool_vaiables(str_analize)) {return 'лог';}
    if (language.char_variables(str_analize)) {return 'лит';}
    if (language.str_variables(str_analize)) {return 'сим';}
    return -1;
}


function is_type (type_1, type_2){   //------------------------?согласование типов
    if(type_1 === type_2) return 1;
    if(type_1 === 'вещ' && type_2 === 'цел') return 1;
    if(type_1 === 'сим') return 2;
}

function number_of_vars(str_analize){ //---------------------------запятые---------------
    var number = 0;
    for(var i = 0; i < str_analize.length; ++i){
        if(str_analize[i][0] == ',') ++number;
    }
    
    return number;
}
    $(function(){

        $('body').on('keypress', 'input.vvod', function(event){
            if(event.keyCode==13){
                
                $(this).prop("disabled", true);
                var i = $(this).attr('id');//input_in_
                var n = '';
                var m = '';
                for(var j = 10; j < i.length; ++j){
                    if(i[j] === 'j') break;
                        else n += i[j];
                }
                for(var k = j+1; k < i.length; ++k){
                    m+= i[k];
                }
                construction_active[number_active_constr].array_of_variables[2][m] = $(this).val();
                var stringCode = $('#id_in_out').find("#input_in_i"+(n)+"j"+(++m));
                if (stringCode.html()==='') $("#input_in_i"+(n)+"j"+(m)).focus();
                else {
                    stringCode = $('#interpreter').find('div');
                    $(stringCode).eq(n).removeClass('active-string-in_out');
                    $(stringCode).eq(n).addClass('active-string'); //подсвечиваем строку бледным
                    $("#play_button").prop("disabled", false);        //останавливаем выполнение
                }
            }
        });
        
    });
            
function get_nameType(typeName){   //--------------------для вывода---------------------------------------------
    if(typeName === 'цел') return 'целого';
    if(typeName === 'вещ') return 'вещественного';
    if(typeName === 'лог') return 'логического';
    if(typeName === 'лит') return 'литерального';
    if(typeName === 'сим') return 'символьного';
}

function if_ifs_and_wiles_true(condition){   //---------------------------формирование и проверка условия---------------------------
    
    ifRobots = 0, eval_str = '';
    for (var i = 0; i < condition.length; ++i){
        
        if(!ifRobots){
                //-----------------------------------------------проверяем вхождения переменных---------------------              //----------------------осталось: объединение условий обычных и от робота-------------------------------
                
            var variable_find = {row: 0, cell: -1};
        
                for(var j = 0; j < construction_active.length; ++j){               
                        variable_find.row = j; variable_find.cell = agreement_search(construction_active[j].array_of_variables[0], condition[i]);
                        if(variable_find.cell  !== -1) break;     
                }
                if(variable_find.cell !== (-1))eval_str += construction_active[variable_find.row].array_of_variables[2][variable_find.cell];

                else
                    //-----------------------------------------------заменяем "и", "или"---------------------------------------
                    if(condition[i] === 'и') eval_str += '&&';
                    else if (condition[i] === 'или') eval_str += '||';
                        //-----------------------------------------------------или записываем то, что есть-----------
                         else {
                            if ((type_variable(condition[i]) === -1) && (!language.sighns(condition[i]))) {
                                ifRobots = 1;
                                return isWall(condition);
                            }
                            eval_str += condition[i];
                         }
        }
        else eval_str += condition[i];
    }
    return eval(eval_str);
}


//---------------------------------------------отлавливаем все условия и циклы------------------------------------
//-------------------------------условия----------------------------------------------------------------------------------------
function catch_ifs_and_fors (code){
    for( var i =0; i < code.length; ++i){
        if(code[i][0] === 'если') {
            
            if((nesting_level === 0) && (number_constructions !== 0)){
                index_Group ++;
                number_groups ++;
            }
            //------------------1. формируем объект------------------------------------------------
            ++number_constructions;  //----------увеличиваем счетчик конструкций
            ++nesting_level; //---------------------увеличиваем уровень вложенности
            construction[number_constructions-1] = {typeConstruction: 0, begining: i, index_change: -1, ending: -1, condition: [], nesting_level: nesting_level-1, indexOfGroup: index_Group};

                //------------------2. сохраняем условие------------------
                var p = 0;
                for (var k = 1; k < (code[i].length-1); ++k, ++p){
                    if (code[i][k] === 'то') break;
                    construction[number_constructions-1].condition[p] = code[i][k];
                }
            number_last_constr.push(number_constructions-1);
        }
        else if(code[i][0] ==='иначе'){
            //------------------1. сохраняем индекс-----------------------------------
            
            for(var k = 0; k < number_constructions; ++k){
                if ((construction[k].nesting_level === (nesting_level-1)) && (construction[k].indexOfGroup === index_Group) &&(construction[k].typeConstruction === 0)){
                    construction[k].isChange =1;
                    construction[k].index_change = i;
                    break;
                }
            }

        }
        
        else if(code[i][0] === 'все'){
            
            //------------------1. сохраняем индекс-----------------------------------
                    construction[number_last_constr[number_last_constr.length-1]].ending =i;
            //------------------------2. уменьшаем уровень вложенности--------------
            --nesting_level;
            number_last_constr.pop();
        }
    
    //-----------------------------------------------циклы-------------------------------------------
    
    else if(code[i][0] === 'нц') {
            
            if((nesting_level === 0) && (number_constructions !== 0)){
                index_Group ++;
                number_groups ++;
            }
            //------------------1. формируем объект------------------------------------------------
            ++number_constructions;  //----------увеличиваем счетчик конструкций
            ++nesting_level; //---------------------увеличиваем уровень вложенности
            number_last_constr.push(number_constructions-1);
        
        //-------------------------для -------------------------------
        if(code[i][1] === 'для'){
            construction[number_constructions-1] = {typeConstruction: 1, begining: i, index_change: code[i][4], ending: -1, condition: [], nesting_level: code[i][4], indexOfGroup: index_Group};

                //------------------ сохраняем условие------------------

                    construction[number_constructions-1].condition[0] = code[i][2];
                    construction[number_constructions-1].condition[1] = '<=';
                    construction[number_constructions-1].condition[2] = code[i][6];
                }
        
        //---------------------------пока--------------------------------
        else if (code[i][1] === 'пока'){
                        construction[number_constructions-1] = {typeConstruction: 2, begining: i, index_change: -1, ending: -1, condition: [], nesting_level: -1, indexOfGroup: index_Group};
                //------------------ сохраняем условие------------------
                var p = 0;
                for (var k = 2; k < (code[i].length-1); ++k, ++p){
                    construction[number_constructions-1].condition[p] = code[i][k];
                } 
        }
    }
        else if(code[i][0] === 'кц'){
            
            //------------------сохраняем индекс-----------------------------------
            
                    construction[number_last_constr[number_last_constr.length-1]].ending =i;
            //------------------------2. уменьшаем уровень вложенности--------------
            --nesting_level;  
            number_last_constr.pop();
        }
    }
    
    
    console.log('construction---------------------------------------------');
    console.log(construction);
    console.log(number_last_constr);
}
 var numberRobotKletka = 15;
 var numberRobotLine = 10;
 var robotWay = {
     way: [],
     length: 0,
 };
 var startEnvironmentPaint;
 var countPaintCell = {};
 var searchWay = {
     start: 1,
     end: 2
 };
$(function() {

    numberRobotKletka = 9;
    numberRobotLine = 8;

    $("#robot").on('click', function() {
        $(".column_2").css({'display': 'none'});
        $(".robot").css({'display': 'block'});
        $('.drawer').css({'display': 'none'});
        $('.robot-menu').css({'display': 'block'});
        $('.robo-pult').css({'display':'inline'});
        var otstupLeft = 0;
        var otstupTop = 0;
        var otstup = parseInt($('.robot-kletka').css('height'));
        var widthPole = numberRobotKletka * otstup + 4;
        var heightPole = numberRobotLine * otstup + 4;
        $(".image").css({'left': '85%'});
        $(".robot").empty();
        $(".robot").append('<div class="robot-pole"></div>');
        $('.robot-pole').css({'width': widthPole});
        $('.robot-pole').css({'height': heightPole});
        var i = 1, k = 1, j = 1, numberCell = 1;
        while (i <= numberRobotLine) {
            while (j <= numberRobotKletka) {
                if (j !== numberRobotKletka) {
                    $('.robot-pole').append('<div class="robot-stena-vert" data-jump="'+numberCell+'_'+(numberCell+1)+'"  style="left: ' + (otstupLeft + otstup) + 'px; top:' + otstupTop + 'px"></div>');
                }
                if (i === 1 && j === 1) {
                    $('.robot-pole').append('<div class="robot-kletka img-robo" data-cell="'+numberCell+'"  style="left: ' + otstupLeft + 'px; top:' + otstupTop + 'px"></div>');

                } else {
                    $('.robot-pole').append('<div class="robot-kletka" data-cell="'+numberCell+'" style="left: ' + otstupLeft + 'px; top:' + otstupTop + 'px"></div>');
                }
                otstupLeft = otstupLeft + otstup;
                j++;
                numberCell++;
            }
            j = 1;
            otstupLeft = 0;
            otstupTop = otstupTop + otstup;
            if (i !== numberRobotLine) {
                while (k <= numberRobotKletka) {

                    $('.robot-pole').append('<div class="robot-stena-hor" data-jump="'+(numberCell-numberRobotKletka+(k-1))+'_'+(numberCell+k-1)+'" style="left: ' + otstupLeft + 'px; top:' + otstupTop + 'px"></div>');
                    otstupLeft = otstupLeft + otstup;

                    k++;
                }

            }
            k = 1;
            otstupLeft = 0;
            i++;
        }

        var obstanovka = $('.robot-pole').find('div');

        var width_pole = $('.robot-pole').outerWidth();
        var width_pole_block = $('.robot').outerWidth();
        $('.column_2').css({'display': 'block', 'z-index': 99, 'position': 'absolute', 'right': 0, 'width': width_pole_block-width_pole});
        $('.windows').css({'height': '115px'});
        
        $('#go-left').on('click', function(){
            left(obstanovka, numberRobotKletka, numberRobotLine); 
        });
        
         $('#go-right').on('click', function(){
            right(obstanovka, numberRobotKletka, numberRobotLine); 
        });
        $('#go-up').on('click', function(){
            up(obstanovka, numberRobotKletka, numberRobotLine); 
        });
        $('#go-down').on('click', function(){
            down(obstanovka, numberRobotKletka, numberRobotLine); 
        });
        $('#paint').on('click', function(){
            var obs = obst(obstanovka, numberRobotKletka, numberRobotLine);
            var posRobo = robotPosition(obs.kletka);
            $(obs.kletka[posRobo.i][posRobo.j].element).css({'background-color': '#0099cc'});
            $(obs.kletka[posRobo.i][posRobo.j].element).addClass('blue-kletka');
        });
    });
    
    
        $("body").on('click', '#change-obstanovka', function() {
            $('#save-obstanovka').css({'display': 'block'});
            $('.robot-pole').css({'border-color': '#f0ad4f'});

            $(".robot-stena-hor").on('click', function() {
                if ($(this).hasClass('red-stena')) {
                    $(this).css({'background-color': '#404040'});
                    $(this).removeClass('red-stena');
                } else {
                    $(this).css({'background-color': 'red'});
                    $(this).addClass('red-stena');
                }
            });
            $(".robot-stena-vert").on('click', function() {
                if ($(this).hasClass('red-stena')) {
                    $(this).css({'background-color': '#404040'});
                    $(this).removeClass('red-stena');
                } else {
                    $(this).css({'background-color': 'red'});
                    $(this).addClass('red-stena');
                }
            });

            $(".robot-kletka").on('click', function() {
                if ($(this).hasClass('blue-kletka')) {
                    $(this).css({'background-color': '#41e0d0'});
                    $(this).removeClass('blue-kletka');
                } else {
                    $(this).addClass('blue-kletka');
                    $(this).css({'background-color': '#0099cc'});
                }
            });
            
            $("#save-obstanovka").on('click', function(){
                 $('#save-obstanovka').css({'display': 'none'});
                 $('.robot-pole').css({'border-color':'#0099cc'});
                 obstanovka = $('.robot-pole').find('div');
                 $(".robot-kletka").unbind('click');
                 $(".robot-stena-hor").unbind('click');
                 $(".robot-stena-vert").unbind('click');
            });

        });
    
    var way = {}; //обстановка на поле для тренировочных заданий
    
    $('body').on('click', '#solve-task-way', function(){
        $('#robot').trigger('click');
        $('#interpreter').attr('data-algorithm','wave');
        way = generation_environment($('.robot-pole')); 
        searchWay = way;
        var matrix = get_matrix_for_the_environment($('.robot-pole'));
        var progress = $('#progress-bar-template').html();
        $('.robot').append(progress);
    });
    
    $('body').on('click', '#save-energy', function(){
       $('#robot').trigger('click');
       $('#interpreter').attr('data-algorithm','energy');
        way = generation_environment_energy_task($('.robot-pole').find('.robot-kletka'), $('.robot-pole'));
        
        searchWay = way;
        var progress = $('#progress-bar-template').html();
        $('.robot').append(progress);
    });
    
    $('body').on('click', '#paint-box', function(){
        $('#robot').trigger('click');
        $('#interpreter').attr('data-algorithm','paint');
        way = generation_enviroment_paint_box($('.robot-pole'));
        searchWay = way;
        startEnvironmentPaint = get_matrix_for_the_paint_task($('.robot-pole'));
        var progress = $('#progress-bar-template').html();
        $('.robot').append(progress);
    });
    
    $('#play').bind('click', function() {
        var line = $('#interpreter').find('div.active-string').attr('id');
        
        if (line === '0') {
            if($('#interpreter').attr('data-algorithm') === 'wave') {
                move_the_robot(way);
            }
            if($('#interpreter').attr('data-algorithm') === 'energy') {
                move_the_robot(way);
            }
            if($('#interpreter').attr('data-algorithm') === 'paint') {
                move_the_robot(way);
            }
            var error_string = $('#interpreter').find('.error-string');
            
            if (error_string.length > 0) {
                error_string.each(function(key, value) {
                    $(value).removeClass('error-string');
                });
            }
        }
    });
    
});

function move_the_robot(way) {
    var img = $('.robot-pole').find('.img-robo');
    $(img).removeClass('img-robo');
    $('.robot-pole').find('[data-cell="' + way.start + '"]').addClass('img-robo');
}

// Robot go to right
function right(obstanovka, numberX, numberY) {
    var obstPole = obst(obstanovka, numberX, numberY);
    var position = robotPosition(obstPole.kletka);
    if (position.j === obstPole.kletka[position.i].length - 1) {
        return false;
    } else
    if ($(obstPole.vert[position.i][position.j].element).hasClass('red-stena')) {
        return false;
    } else {
        $(obstanovka).eq(position.number).removeClass('img-robo');
        $(obstPole.kletka[position.i][position.j + 1].element).addClass('img-robo');
        return true;
    }

}

//Есть ли стена справа
function isRightWall(obstanovka, numberX, numberY) {
    var obstPole = obst(obstanovka, numberX, numberY);
    var position = robotPosition(obstPole.kletka);
    if (position.j === obstPole.kletka[position.i].length - 1) {
        return false;
    } else
    if ($(obstPole.vert[position.i][position.j].element).hasClass('red-stena')) {
        return false;
    } else {
        return true;
    }

}

//Robot go to left
function left(obstanovka, numberX, numberY) {
    var obsPole = obst(obstanovka, numberX, numberY);
    var position = robotPosition(obsPole.kletka);
    if (position.j === 0) {
        return false;
    } else
    if ($(obsPole.vert[position.i][position.j - 1].element).hasClass('red-stena')) {
        return false;
    } else {
        $(obstanovka).eq(position.number).removeClass('img-robo');
        $(obsPole.kletka[position.i][position.j - 1].element).addClass('img-robo');
        return true;
    }
}

//Есть ли слева стена 
function isLeftWall(obstanovka, numberX, numberY) {
    var obsPole = obst(obstanovka, numberX, numberY);
    var position = robotPosition(obsPole.kletka);
    if (position.j === 0) {
        return false;
    } else
    if ($(obsPole.vert[position.i][position.j - 1].element).hasClass('red-stena')) {
        return false;
    } else {
        return true;
    }
}


//Robot go to down
function down(obstanovka, numberX, numberY) {
    var obsPole = obst(obstanovka, numberX, numberY);
    var position = robotPosition(obsPole.kletka);
    if (position.i === obsPole.kletka.length - 1) {
        return false;
    } else
    if ($(obsPole.hor[position.i][position.j].element).hasClass('red-stena')) {
        return false;
    } else {
        $(obstanovka).eq(position.number).removeClass('img-robo');
        $(obsPole.kletka[position.i + 1][position.j].element).addClass('img-robo');
        return true;
    }
}

//Есть ли снизу стена
function isDownWall(obstanovka, numberX, numberY) {
    var obsPole = obst(obstanovka, numberX, numberY);
    var position = robotPosition(obsPole.kletka);
    if (position.i === obsPole.kletka.length - 1) {
        return false;
    } else
    if ($(obsPole.hor[position.i][position.j].element).hasClass('red-stena')) {
        return false;
    } else {
        return true;
    }
}

//Robot go to up
function up(obstanovka, numberX, numberY) {
    var obsPole = obst(obstanovka, numberX, numberY);
    var position = robotPosition(obsPole.kletka);
    if (position.i === 0) {
        return false;
    } else
    if ($(obsPole.hor[position.i - 1][position.j].element).hasClass('red-stena')) {
        return false;
    } else {
        $(obstanovka).eq(position.number).removeClass('img-robo');
        $(obsPole.kletka[position.i - 1][position.j].element).addClass('img-robo');
        return true;
    }
}

//Есть ли сверху стена 
function isUpWall(obstanovka, numberX, numberY) {
    var obsPole = obst(obstanovka, numberX, numberY);
    var position = robotPosition(obsPole.kletka);
    if (position.i === 0) {
        return false;
    } else
    if ($(obsPole.hor[position.i - 1][position.j].element).hasClass('red-stena')) {
        return false;
    } else {
        return true;
    }
}

//Find Robot position
function robotPosition(kletka) {
    var position = {};
    for (var i = 0; i < kletka.length; i++) {
        for (var j = 0; j < kletka[i].length; j++) {
            if ($(kletka[i][j].element).hasClass('img-robo')) {
                position.i = i;
                position.j = j;
                position.number = kletka[i][j].number;
                break;
            }
        }
    }
    return position;
}

//Create an array for the situation in the field 
function obst(obstanovka, numberX, numberY) {
    var kletka = [];
    var hor = [];
    var vert = [];
    for (var i = 0; i < numberY; i++) {
        kletka[i] = [];
        hor[i] = [];
        vert[i] = [];
    }
    for (var i = 0; i < numberY - 1; i++) {
        hor[i] = [];
    }

    var element = [];
    $(obstanovka).each(function(i, val) {
        element[i] = val;
    });
    var k = 0, i = 0, j = 0;
    while (i < numberY) {
        while (j < numberX) {
            if ($(element[k]).hasClass('robot-kletka')) {
                kletka[i][j] = {element: element[k], number: k};
                j++;
            }
            k++;
        }
        j = 0;
        i++;
    }
    i = 0;
    j = 0;
    k = 0;
    while (i < numberY) {
        while (j < numberX - 1) {
            if ($(element[k]).hasClass('robot-stena-vert')) {
                vert[i][j] = {element: element[k], number: k};
                j++;
            }
            k++;
        }
        j = 0;
        i++;
    }
    i = 0;
    j = 0;
    k = 0;
    while (i < numberY - 1) {
        while (j < numberX) {
            if ($(element[k]).hasClass('robot-stena-hor')) {
                hor[i][j] = {element: element[k], number: k};
                j++;
            }
            k++;
        }
        j = 0;
        i++;
    }
    var obstanovka = {};
    obstanovka.kletka = kletka;
    obstanovka.vert = vert;
    obstanovka.hor = hor;
    return obstanovka;
}


/**
 * 
 * Окрашивает текущую строку работы интерпретатора в красный цвет при ошибке
 * @returns {undefined}
 */
function error_line() {
    $('.interpreter').find('.active-string').addClass('error-string');
}


function reset_the_robot_way() {
    robotWay.way = [];
    robotWay.length = 0;
}

function reset_count_paint_cell() {
    for(var key in countPaintCell) {
        $('[data-cell="'+key+'"]').removeClass('blue-kletka');
    }
    countPaintCell = {};
}

function reset_progress() {
    $('#rating-danger').css('width','1%');
    $('#rating-warning').css('width','0%');
    $('#rating-success').css('width','0%');
}

function step_robot() {
    var cell = find_cell_robot();
    var route = robotWay.way;
    robotWay.way[route.length] = cell;
    robotWay.length++;
}

function count_paint_cell(cell) {
    if(countPaintCell[cell] === undefined) {
        countPaintCell[cell] = 1;
    } else {
        countPaintCell[cell]++;
    }
}

function find_cell_robot() {
    var robot = $('.robot-pole').find('.img-robo');
    var cell = $(robot).attr('data-cell');
    return cell;
}
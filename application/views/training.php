<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>DAR</title>
        <link href="<?= base_url() ?>assets/css/main.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/css/teaching.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/bootstrap/css/bootstrap.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/bootstrap/css/bootstrap-theme.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/css/robot.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/css/drawer.css" rel="stylesheet">
        <script type="text/javascript" src="<?= base_url() ?>assets/js/jquery-2.0.3.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/main.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/robot_work.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/interpreter.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/bootstrap/js/bootstrap.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/robot.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/drawer.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/training_code.js"></script>
    </head>
    <body class="style-page">
        <div id="container">
            <div class="row">
                <div class="content">
                    <div class="navbar navbar-default">
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav">
                                <a class="navbar-brand" href="<?= base_url() ?>">ДАР</a>
                                <li><a href="<?= base_url() ?>shaya">Интерпретатор</a></li>
                                <li><a href="<?= base_url() ?>training">Обучение</a></li>
                                <!--<li><a href="#">Коллекция примеров</a></li>-->
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Исполнители</a>
                                    <ul class="dropdown-menu">
                                        <button class="btn btn-sm btn-success" id="robot">Робот</button>
                                        <button class="btn btn-sm btn-primary" id="drawer">Чертежник</button>
                                    </ul>
                                </li>
                                <li><a href="<?= base_url() ?>reference">Справочник</a></li>
                                <!--<li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Выбрать язык <b class="caret"></b></a>
                                    <ul class="dropdown-menu">    
                                        <a href="<?= base_url() ?>shaya"><img src="<?= base_url() ?>assets/img/Russia.png" width="30" height="30" ></a>
                                        <a href="<?= base_url() ?>shaya_ua"><img src="<?= base_url() ?>assets/img/Ukraine.png" width="30" height="30"></a>      
                                    </ul>
                                </li>-->
                            </ul>  
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="content">
                    <div class="well-smt"></div>                   
                </div>
            </div>

            <div class="row">
                <div class="content">
                    <div class="train">
                        <div class="robot_train"><img src="<?= base_url() ?>assets/img/robot.png"></div>
                        <div class="speech_train" id='text_train'>
                            <div class="visible_block" id="step_0"><p>Привет, новичок!</p>
                                &emsp;Давай знакомится! Я Роби! Я научу тебя создавать собственные алгоритмы, которые в дальнейшем составят твою первую программу!
                                Я научу тебя думать, как думает компьютер. По-моему, звучит круто) Ты готов стать гуру программирования?)<br>
                                &emsp;Если ты готов, то нажми кнопочку "Далее"!
                                &emsp;Если ты что-то пропустишь, ты всегда сможешь вернуться "Назад"!</div>
                            <div class="visible_none" id="step_1">&emsp;Выделенное поле является <i>рабочей областью</i>, где располагается основная программа,
                                то есть данное поле может содержать только твои команды, другими словами - твой исходный код.<br>
                                &emsp;Всё понятно?)</div>
                            <div class="visible_none" id="step_2">&emsp;В разработке программы тебе понадобится вводить какие-то данные с клавиатуры для того, чтобы интерпретатор в составленных алгоритмах мог их обработать и выдать результат.<br> 
                                &emsp;Подсвеченное поле является <i>полем ввода-вывода</i>, которое служит для ввода аргументов алгоритма и вывода его результата.</div>
                            <div class="visible_none" id="step_3">&emsp;Это поле скажет тебе, <i>если ты совершил ошибку</i>, а так же подскажет в какой она строке. Кроме того, если ты все сделал правильно, ты получишь радостное сообщение.
                                <br>&emsp;Я постараюсь помочь тебе не совершать ошибки) Ты согласен?)</div>
                            <div class="visible_none" id="step_4">&emsp;В этом поле будет показывается <i>ход работы программы</i>. Здесь ты увидишь, какая строка обрабатывается в данный момент.</div>
                            <div class="visible_none" id="step_5">&emsp;Это <i>поле содержит кнопки</i>, которые тебе понядобятся для работы программы. <br>
                                &emsp;Для того, чтобы запустить построчое выполнение программы, тебе нужно нажать кнопочку <span class="glyphicon glyphicon-play"></span>.<br>
                                &emsp;Если ты вдруг захочешь проверить свой алгоритм заново, кнопка <span class="glyphicon glyphicon-stop"></span> позволяет остановиться и, после повторного нажатия кнопки запуска, проверка начнется сначала.</div>
                            <div class="visible_none" id="step_6">&emsp;Теперь, когда ты знаешь интерфейс нашего обработчика, мы можем приступить к обучению школьному алгоритмическому языку. <br>&emsp;Основной структурной единицей данного языка является алгоритм. Поэтому ты должен знать, что это такое.
                                <br>&emsp;<strong>Алгоритм</strong> - это последовательность действий приводящих к решению поставленной задачи за конечное число шагов.</div>
                            <div class="visible_none" id="step_7">В структуре языка алгоритм описывается в следующем виде:<br><br>
                                <strong>алг</strong> имя (<i>аргументы</i> и <i>результаты</i>)<br>
                                <strong>дано</strong> <i>условия применимости алгоритма</i><br>
                                <strong>надо</strong> <i>цель выполнения алгоритма</i><br>
                                <strong>нач</strong><br>
                                &emsp;<i>тело алгоритма</i><br>
                                <strong>кон</strong><br>
                                Жми "Дальше" и мы попробуем его создать.
                            </div>
                            <div class="visible_none" id="step_8">Введи в рабочее поле команды в таком виде:<br><br>
                                <strong>алг</strong><br>
                                <strong>нач</strong><br>
                                <strong>кон</strong><br>
                                Это простейший алгоритм. Запусти интерпретатор, он найдет ошибки, если они есть. А затем нажми кнопку "Проверить", чтобы убедиться, что ты написал структуру точно.
                            </div>
                            <div class="visible_none" id="step_9">Теперь попробуем задать алгоритму имя, написав его в таком виде:<br><br>
                                <strong>алг</strong> <i>имя</i><br>
                                <strong>нач</strong><br>
                                <strong>кон</strong><br>
                                Запусти интерпретатор, он найдет ошибки, если они есть. А затем нажми кнопку "Проверить", чтобы убедиться, что ты написал структуру точно.
                            </div>
                            <div class="visible_none" id="step_10">Теперь попробуем задать алгоритму имя, написав его в таком виде:<br><br>
                                <strong>алг</strong> <i>имя</i><br>
                                <strong>нач</strong><br>
                                <strong>кон</strong><br>
                                Запусти интерпретатор, он найдет ошибки, если они есть. А затем нажми кнопку "Проверить", чтобы убедиться, что ты написал структуру точно.
                            </div>
                            <div class="visible_none" id="step_10">Теперь я обучу тебя работе с исполнителем.<br>Для того, чтобы использовать исполнителя, напиши такую структуру:<br>
                                <i>использовать Робот</i><br>
                                <strong>алг</strong><br>
                                <strong>нач</strong><br>
                                <strong>кон</strong><br>
                                Добавить поле робота можно с помощью кнопки "Исполнители" на главной панели, затем выбрав "Робот".
                            </div>
                        </div>
                        <button type="button" class="btn btn-info btn_position_check disabled" id='btn_check'>Проверить</button>
                        <button type="button" class="btn btn-info btn_position_next" id='btn_next'>Далее</button> 
                        <button type="button" class="btn btn-info btn_position_prev disabled" id='btn_prev'>Назад</button>
                    </div>
                </div>
            </div>

            <!--------------------------------------------------------------------------------------------------------> 
            <div class="train_interpreter">
                <div class="row">
                    <div class="content">
                        <div class="form_right_angle" id="begin_block">
                            <span class="glyphicon glyphicon-play" id = "play"></span>
                            <span class="glyphicon glyphicon-stop" id = "stop"></span>
                            <div class="btn-group robot-menu">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    Робот
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <!--  <li><a href="#">Изменить поле робота</a></li> -->
                                    <li><a href="#" id="change-obstanovka">Изменить обстановку робота</a></li>
                                </ul>
                                <button type="button" class="btn btn-success" id="save-obstanovka">Coхранить</button>
                            </div>
                            <div class="robo-pult">
                                <span class="glyphicon glyphicon-arrow-left" id="go-left"></span>
                                <span class="glyphicon glyphicon-arrow-right" id="go-right"></span>
                                <span class="glyphicon glyphicon-arrow-up" id="go-up"></span>
                                <span class="glyphicon glyphicon-arrow-down" id="go-down"></span>
                                <span class="glyphicon glyphicon-tint" id="paint"></span>
                            </div>
                        </div>
                    </div>

                    <div class="content">
                        <div>
                            <div class="column_1">
                                <div class="shell_number">
                                    <div class="form_right_angle number_bar" id="number_bar" contenteditable="false">
                                        <span class="number_style">1</span>                              
                                    </div> 
                                </div>
                                <div class="shell_inter">
                                    <div class="form_right_angle interpreter" id="interpreter" >
                                        <textarea class = "green" autofocus ></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="column_2">
                                <div class="form_right_angle windows" contenteditable="true" spellcheck="false" id="input_output"></div>
                                <div class="form_right_angle windows" id='id_error'></div>
                            </div>
                            <!-- Блок для исполнителя робот -->
                            <div class="robot">
                                <div class="robot-pole">
                                    <div class="robot-kletka"></div>
                                </div>
                            </div>
                            <!-- ============================ -->
                            <!-- Блок для исполнителя чертежник -->
                            <div class="drawer">
                                <img src="<?= base_url() ?>assets/img/pencil.png" id="drawer-pencil">
                                <canvas id="drawer-pole"></canvas>
                            </div>
                            <!-- ============================ -->
                        </div>
                    </div>
                    <div class="content">
                        <div class="row_end form_right_angle" id="end_block"></div>               
                    </div>
                </div>                  
            </div>
            <!-------------------------------------------------------------------------------------------------------->            
            <div class="row">
                <div class="content">
                    <div class="well-smb"></div> 
                </div>
            </div>

            <div class="row">
                <div class="content">
                    <div class="navbar navbar-default">
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>

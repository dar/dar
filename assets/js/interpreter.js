var language = {
    typeVariable: function(str){
        return /^(вещ|лит|сим|цел|лог)$/.test(str);
    },
    
    structure: function(str){
        return /^(алг|нач|кон|использовать)$/.test(str); 
    },
    
    executor: function(str){
        return /^(Робот|Чертежник)$/.test(str);
    },
    
    parameters: function(str){
        return /^(арг|рез|аргрез)$/.test(str);       
    },
    
    input_output: function(str){
        return /^(вывод|ввод)$/.test(str);        
    },
    ifelse: function(str){
        return /^(если|то|иначе|все)$/.test(str); 
    },
    fors: function(str){
        return /^(нц|кц|пока|для|от|до)$/.test(str); 
    },
    
    sighns: function(str){
        return /^(\)|\(|\*|\+|-|\/|:=|=|>|<|>=|<=|)$/.test(str); 
    },
    
    robot: ["влево", "вправо", "вверх", "вниз", "закрасить", "если", "выбор", "при", "иначе", "все", "слева", "справа", "сверху", "снизу", "клетка", "свободно", "стена", "закрашена", "чистая", "то"],
    
    variableName: function(str) {
        return /^[а-яА-Яa-zA-Z][А-Яа-яA-Za-z0-9_]*$/.test(str);
    },
    
    str_variables: function(str){
        return /^("[^"]*"|'[^']*')$/.test(str);
    },
    
    char_variables: function(str){
        return /^("[^"]"|'[^']')$/.test(str);
    },
    
    float_variables: function(str){
        return /^(\d*\.\d*|\d+)$/.test(str);
    },
    int_variables: function(str){
        return /^[0-9]+$/.test(str);
    },
    bool_vaiables: function(str){
        return /^(true|false)$/.test(str);
    }
};

var isRobot = false;
var alfabetMain = ["\n", "алг", "использовать", "Робот", "Чертежник", "цел", "вещ", "лог", "сим", "лит", ":=", ";", ",", "(", ")", "арг", "рез", "аргрез", "нач", "кон", '"', "_|", "#NAME", "#INT", "#FLOAT", "#INT", "#CHAR", "#STR", "#BOOL" ];
var alfabetRobot = ["влево", "вправо", "вверх", "вниз", "закрасить", "если", "выбор", "при", "иначе", "все", "слева", "справа", "сверху", "снизу", "клетка", "свободно", "стена", "закрашена", "чистая", "то", "\n", ";", "_|"];
/*var alfabetBody = ["\n", "вещ", "цел", "сим", "лит", "лог", "ввод", "вывод", "#NAME", ",", ";", ":=", "#FLOAT", "#INT", "#CHAR", "#STR", "#BOOL", "+", "-", "*", "/", "_|"];*/

var alfabetBody = ["\n", ";", ",", "цел", "вещ", "лит", "сим", "лог", "#NAME", "#INT", "#FLOAT", "#BOOL", "#STR", "#CHAR", ":=", "=", ">", "<", ">=", "<=", "если", "то", "иначе", "ввод", "вывод", "(", ")", "и", "или", "+", "-", "*", "/", "_|"];

var symbolMain = ["#S1", "#S0", "#S1_", "#P1", "#P1_", "#B", "#P2", "#P2_", "#T", "#A", "#C", "#C_", "#P3", "#P3_", "#D", "#D_", "#E", "#E_", "#G1", "#G2", "#G3", "#G4", "#I", "#I_", "#L", "#L_", "#N", "#O", "#O_", "#J", "#P", "#P_", "#Q", "#R", "#R_", "#R1_", "#U_", "#U"];
var symbolRobot = ["#Q0", "#Q", "#Q1", "Q2", "#CONT", "#CONT1", "#CONT11", "#SP", "#TH", "#TH1", "#CONT2", "#CONT22", "#CONT23", "#LIST", "#LIST1", "#LIST11", "#LIST12", "#CONT3", "#CONT33", "#CONT34", "#SHIFT", "#SHIFT1", "#SPACE", "#SPACE1", "#CONT4", "#CONT44", "#CONT45", "ELSE", "TH11"];

var symbolBody = ["#R", "#T","#T_", "#E", "#C", "#E_", "#E1", "#K1", "#J", "#B_", "#B1", "#B2", "#K1_", "#K2", "#K2_", "#D", "#EQ", "#N", "#S", "#F_", "#F1", "#IF", "#IF_", "#EF", "#SF" ];

var automaticMain = [
    [1, 2, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0, -1, 0, 0, 0, 0, 0],
    [5, 6, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0],
    [0, 8, 9, 0, 0, 32, 32, 32, 32, 32, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0],
    [10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
    [11, 12, 12, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 12, 0, -1, 0, 0, 0, 0, 0],
    [0, 0, 0, 13, 14, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0],
    [15, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0],
    [16, 18, 0, 0, 0, 17, 17, 17, 17, 17, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 19, 20, 21, 22, 23, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0],
    [0, 24, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 25, 25, 25, 25, 25, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0],
    [27, 0, 0, 0, 0, 26, 26, 26, 26, 26, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0],
    [28, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0],
    [30, 31, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 29, -1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 33, -1, 0, 0, 0, 0, 0],
    [35, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 34, -1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 36, -1, 0, 0, 0, 0, 0],
    [38, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 37, 39, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 40, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 41, 0, 0, -1, 42, 76, 77, 78, 79],
    [46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 45, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 44, 0, 0, -1, 0, 43, 0, 0, 0],
    [47, 0, 0, 0, 0, 49, 49, 49, 49, 49, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 48, -1, 0, 0, 0, 0, 0],
    [50, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 51, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 53, 53, 53, 53, 53, 0, 0, 0, 0, 0, 52, 52, 52, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 54, 54, 54, 54, 54, 0, 0, 0, 0, 55, 54, 54, 54, 0, 0, 0, 55, 0, -1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 56, 57, 58, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 59, -1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 61, 61, 61, 61, 61, 0, 0, 60, 0, 0, 61, 61, 61, 0, 0, 0, 61, 0, -1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 62, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 63, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0],
    [64, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 65, 0, 0, 0, 0, 0, 0, 0, 0, 0, 75, 0, -1, 0, 75, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 77, 0, 77, 66, -1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 67, 0, 0, 0, -1, 0, 0, 0, 0, 0],
    [68, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 0, -1, 0, 0, 0, 0, 0],
    [70, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 71, 0, -1, 71, 71, 71, 71, 71],
    [72, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 73, 0, -1, 73, 73, 73, 73, 73],
    [74, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0]
];

var automaticRobot = [
    [70, 70, 70, 70, 70, 70, 70, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 71],
    [1, 2, 3, 4, 5, 6, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8],
    [0, 0, 0, 0, 0, 0, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 9, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 11, 12, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 13, 14, 15, 16, 17, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 18, 19, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 20, 21, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23, 22, 22, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 24, 0, 0, 0],
    [26, 26, 26, 26, 26, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 25, 25, 0],
    [28, 27, 29, 30, 31, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 32, 32, 33],
    [34, 34, 34, 34, 34, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 35],
    [0, 0, 0, 0, 0, 0, 0, 36, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [38, 38, 38, 38, 38, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 37, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 40, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 39, 39, 41],
    [0, 0, 0, 0, 0, 0, 0, 42, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 43],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 44, 45, 46, 47, 48, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 49, 50, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 51, 52, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 53, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 54, 0, 55],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 56, 57, 0],
    [59, 59, 59, 59, 59, 59, 0, 59, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 58, 58, 59],
    [61, 60, 62, 63, 64, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 65, 65, 66],
    [67, 67, 67, 67, 67, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 68],
    [0, 0, 0, 0, 0, 0, 0, 0, 69, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 72, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 73]
];

var automaticBody = [
    [0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 65],
    [0, 0, 0, 4, 4, 4, 4, 4, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 0, 0, 5, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 9, 9, 9, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 11, 13, 15, 14, 12, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [17, 16, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [19, 0, 0, 0, 0, 0, 0, 0, 18, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 20, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 21, 22, 25, 24, 23, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [28, 27, 26, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 30, 30, 30, 30, 30, 29, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [33, 0, 0, 32, 32, 32, 32, 32, 31, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [35, 36, 34, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 38, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 37, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [40, 41, 39, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 42, 42, 42, 42, 42, 42, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 42, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 43, 43, 43, 43, 43, 43, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 44, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 45, 45, 45, 45, 45, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 47, 48, 49, 50, 0],
    [52, 52, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 51, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [53, 54, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 55, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 58, 0, 0, 0, 0, 0, 56, 57, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 59, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 64, 60, 61, 62, 63 , 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
];


/*
function parse(code, alfabet, automatic, symbol, pravilo, number_str_in, ruls_number, number_strings, is_debug) {
    
    if(is_debug) {
        if ($("#id_error").html() !== '') return 0;
    }
    
    var stack = [];
    stack[0] = '_|';
    var numberRule,
            admission = 2,
            result,
            number_str = number_str_in,
            indexOfSymbol = 0,
            indexOfAlfabet,
            i = 0;

    numberRule = 0;
    for (var j = 0; j < alfabet.length; ++j)
    {
        if (code[number_str][0] === alfabet[j])
        {
            numberRule = automatic[number_str][j];
            result = pravilo(numberRule, stack);
            break;
        }
    }


    if (stack.length === 1)
    {
        result = 2;
    }

    for (; number_str < code.length; ++number_str) {
        console.log('codelength = ' + code.length + '; numberstringth = ' + number_strings);
        if(number_str === number_strings) break;              //для парсинга построчно

        else
        for (i = 0; i < code[number_str].length; ) {

            if (stack[stack.length - 1] === "нач") {
                $("#id_error").html($("#id_error").html()+'<span class = "active-string">Предупреждение:</span> отсутствует оператор "кон" (строка ' + (number_str +1) + ')');
            }
            if (code[number_str][0] === 'кон') {
               if( $("#id_error").html() === '') {
                   ($("#id_error").html()+'<span class = "active-string">Предупреждение:</span> отсутствует оператор "нач"');
                   console.log('кон');
               }
                else {
                    $("#id_error").html('');
                }
            }
            if (stack[stack.length - 1] === "#body") {
                stack.pop();
                var code1 = [];
                
                var k = number_strings - number_str;
                    code1[k] = [];
                    for (var m = 0; m < k; ++m){
                        code1[m] = [];
                        for (var l = 0; l < code[m + number_str].length; l++) {
                            console.log(code[m + number_str]);
                            code1[m][l] = code[m + number_str][l];
                        }
                    }
                        var res;
                        console.log(code1);
                           if(isRobot) res = parse(code1, alfabetRobot, automaticRobot, symbolRobot, praviloRobot, 0, 75, k, is_debug);
                           else res = parse(code1, alfabetBody, automaticBody, symbolBody, praviloBody, 0, 41, k, is_debug);
                        if (res === (-1)) {
                            i = 0;
                            number_str = number_str + code1.length;
                        }
                
            }
            if ((code[number_str][i] === "") || (code[number_str][i] === " ")) {
                ++i;
            }
            if (i === code[number_str].length)
                break;

            if (code[number_str][i] === stack[stack.length - 1]) {
                numberRule = (ruls_number - 1);
                result = pravilo(numberRule, stack);
                if (result === 2)
                    admission = 1;
                ++i;
            }

            else {
                for (j = 0; j < symbol.length; ++j) {
                    if (stack[stack.length - 1] === symbol[j]) {
                        indexOfSymbol = j;
                        break;
                    }
                }
                for (j = 0; j < alfabet.length; ++j) {
                    if (code[number_str][i] === alfabet[j]) {
                        indexOfAlfabet = j;
                        break;
                    }

                }
                if (j === alfabet.length)
                    result = 0;
                else {
                    numberRule = automatic[indexOfSymbol][indexOfAlfabet];
                    result = pravilo(numberRule, stack);
                }
            }
            if (result === 0) {
                admission = 0;
                break;
            }
            if (result === 2) {
                admission = 1;
                break;
            }
            
       /*     if (result === (-1)){
                $("#id_error").html($("#id_error").html()+'Кажется, ты ввел слово, которого не должно здесь быть (строка  ' + (number_str) + ')');
            return (-1);
            }*/

    /*    }

       if (admission === 0) {
            console.log(stack);
            return 0;
        }
        if (admission === 1) {
            return (1);
        }
        
    }
    --number_str;
    if (((result !== 2) || (result !== 0)) && (number_strings >= (code.length - 1))) {
        while (1) {
            if (stack.length === 1)
                numberRule = (ruls_number - 1);
            else {
                for (j = 0; j < symbol.length; ++j) {
                    if (stack[stack.length - 1] === symbol[j]) {
                        indexOfSymbol = j;
                        break;
                    }
                }
                numberRule = automatic[indexOfSymbol][automatic[0].length - 1];
            }
            console.log(stack);
            result = pravilo(numberRule, stack);
            if (result === 0) {
                admission = 0;
                break;
            }
            if (result === 2) {
                admission = 1;
                break;
            }
            
        }
    }
    if (admission === 0) {
            console.log(stack);
          //  $("#id_error").html($("#id_error").html()+'Ошибка синтаксиса в строке: ' + (number_str+1) + '! :)');
            return 0;
    }
    if (admission === 1) {
        $("#id_error").html('');
        return (1);
    }
    if(admission === 2){
        return 2;
    }

}*/
        
function parse(code, alfabet, automatic, symbol, pravilo, number_str_in, ruls_number, is_debug) {
    

    $("#id_error").html('');

    var stack = [];
    stack[0] = '_|';
    var numberRule,
            admission,
            result,
            number_str = number_str_in,
            indexOfSymbol = 0,
            indexOfAlfabet,
            i = 0;

    numberRule = 0;
    for (var j = 0; j < alfabet.length; ++j)
    {
        if (code[number_str][0] === alfabet[j])
        {
            numberRule = automatic[number_str][j];
            result = pravilo(numberRule, stack);
            break;
        }
    }


    if (stack.length === 1)
    {
        result = 2;
    }

    for (; number_str < code.length; ++number_str) {
        for (i = 0; i < code[number_str].length; ) {

            if (stack[stack.length - 1] === "#body") {
                stack.pop();
                var code1 = [];
                
                for (var k = 0; k < code.length; k++) {
                    if (code[k + number_str][0] === 'кон') {
                        break;
                    }
                    code1[k] = [];
                    for (var l = 0; l < code[k+number_str].length; l++) {
                        code1[k][l] = code[k + number_str][l];
                    }
                }
                if(code1.length !== 0) {
                   var res = {};
                   if(isRobot) res = parse(code1, alfabetRobot, automaticRobot, symbolRobot, praviloRobot, 0, 75, is_debug);
                   else res = parse(code1, alfabetBody, automaticBody, symbolBody, praviloBody, 0, 66, is_debug);
                
                if (res.result_func === 0) {
                    number_str = number_str + res.str;
                    return {result_func: 0, str: number_str}
                }
                else {
                    i = 0;
                    number_str = number_str + code1.length;

                }
                }
                
            }
            if ((code[number_str][i] === "") || (code[number_str][i] === " ")) {
                ++i;
            }
            if (i === code[number_str].length)
                break;

            if (code[number_str][i] === stack[stack.length - 1]) {
                numberRule = (ruls_number - 1);
                result = pravilo(numberRule, stack);
                if (result === 2)
                    admission = 1;
                ++i;
            }

            else {
                for (j = 0; j < symbol.length; ++j) {
                    if (stack[stack.length - 1] === symbol[j]) {
                        indexOfSymbol = j;
                        break;
                    }
                }
                for (j = 0; j < alfabet.length; ++j) {
                    if (code[number_str][i] === alfabet[j]) {
                        indexOfAlfabet = j;
                        break;
                    }

                }
                if (j === alfabet.length)
                    result = 0;
                else {
                    numberRule = automatic[indexOfSymbol][indexOfAlfabet];
                    result = pravilo(numberRule, stack);
                }
            }

            if (result === 0) {
                admission = 0;
                return {result_func: 0, str: number_str}
                //break;
            }
            if (result === 2) {
                admission = 1;
                break;
            }

        }

        if (admission === 0) {
            console.log(stack);
            return {result_func: 0, str: number_str}
        }
        if (admission === 1) {
            console.log("ура");
            return {result_func: 1, str: number_str}
        }
    }

    --number_str;
    if ((result !== 2) || (result !== 0)) {
        while (1) {
            if (stack.length === 1)
                numberRule = (ruls_number - 1);
            else {
                for (j = 0; j < symbol.length; ++j) {
                    if (stack[stack.length - 1] === symbol[j]) {
                        indexOfSymbol = j;
                        break;
                    }
                }
                numberRule = automatic[indexOfSymbol][automatic[0].length - 1];
            }
            console.log(stack);
            result = pravilo(numberRule, stack);

            if (result === 0) {
                admission = 0;
                break;
            }
            if (result === 2) {
                admission = 1;
                break;
            }
        }
    }

    if (admission === 0) {
        console.log(stack);
        // $("#id_error").html('упс.ошибка синтаксиса в строке ' + (number_str + 1));
        return {result_func: 0, str: number_str}
    }
    if (admission === 1) {
        console.log("ура");
        $("#id_error").html('');
        return {result_func: 1, str: number_str}
    }





}

function praviloMain(number, stack) {
    var result = 1;

    if (number === 0) {
        result = 0;            // состояние ошибка!
    }
    
    if (number === -1) {
        result = -1;            // состояние ошибка - введена кривота!
    }
    if (number === 1) {
        stack.push("#S1_");
        stack.push("#P1");
    }
    if (number === 2) {
        stack.push("#J");
        stack.push("#I");
        stack.push("алг");
    }
    if (number === 3) {
        stack.push("#B");
        stack.push("использовать");
    }
    if (number === 4) {
        result = 1;
    }
    if (number === 5) {
        stack.pop();
        stack.push("#S1_");
        stack.push("#P1");
    }
    if (number === 6) {
        stack.pop();
        stack.push("#J");
        stack.push("#I");
        stack.push("алг");
    }
    if (number === 7) {
        stack.pop();
        stack.push("#B");
        stack.push("использовать");
    }
    if (number === 8) {
        stack.pop();
        stack.push("#J");
        stack.push("#I");
        stack.push("алг");
    }
    if (number === 9) {
        stack.pop();
        stack.push("#B");
        stack.push("использовать");
    }
    if (number === 10) {
        stack.pop();
        stack.push("#P1_");
        stack.push("\n");
    }
    if (number === 11) {
        stack.pop();
        stack.push("#P1");
    }
    if (number === 12) {
        stack.pop();
    }
    if (number === 13) {
        stack.pop();
        stack.push("#P2");
        stack.push("Робот");
        isRobot = true;
    }
    if (number === 14) {
        stack.pop();
        stack.push("#P2");
        stack.push("Чертежник");
    }
    if (number === 15) {
        stack.pop();
        stack.push("#P2_");
        stack.push("\n");
    }
    if (number === 16) {
        stack.pop();
        stack.push("#P2");

    }
    if (number === 17) {
        stack.pop();
        stack.push("#C");
    }
    if (number === 18) {
        stack.pop();
        stack.push("#A");
    }
    if (number === 19) {
        stack.pop();
        stack.push("цел");
    }
    if (number === 20) {
        stack.pop();
        stack.push("вещ");
    }
    if (number === 21) {
        stack.pop();
        stack.push("лог");
    }
    if (number === 22) {
        stack.pop();
        stack.push("сим");
    }
    if (number === 23) {
        stack.pop();
        stack.push("лит");
    }
    if (number === 24) {
        stack.pop();
        stack.push("#J");
        stack.push("#I");
        stack.push("алг");
    }
    if (number === 25) {
        stack.pop();
        stack.push("#C_");
        stack.push("#E");
        stack.push("#T");
    }
    if (number === 26) {
        stack.pop();
        stack.push("#C");
    }
    if (number === 27) {
        stack.pop();
        stack.push("#P3");
    }
    if (number === 28) {
        stack.pop();
        stack.push("#P3_");
        stack.push("\n");
    }
    if (number === 29) {
        stack.pop();
        stack.push("#D");
    }
    if (number === 30) {
        stack.pop();
        stack.push("#P3");
    }
    if (number === 31) {
        stack.pop();
        stack.push("#A");
    }
    if (number === 32) {
        stack.pop();
        stack.push("#C");
    }
    if (number === 33) {
        stack.pop();
        stack.push("#D_");
        stack.push("#G1");
        stack.push("#NAME");
    }
    if (number === 34) {
        stack.pop();
        stack.push("#D");
    }
    if (number === 35) {
        stack.pop();
        stack.push("#A");
        stack.push("/n");
    }
    if (number === 36) {
        stack.pop();
        stack.push("#E_");
        stack.push("#NAME");
    }
    if (number === 37) {
        stack.pop();
        stack.push(";");
    }
    if (number === 38) {
        stack.pop();
        stack.push("\n");
    }
    if (number === 39) {
        stack.pop();
        stack.push("#E");
        stack.push(",");
    }
    if (number === 40) {
        stack.pop();
        stack.push("#G2");
        stack.push(":=");
    }
    if (number === 41) {
        stack.pop();
        stack.push("#G4");
        stack.push('"');
    }
    if (number === 42) {
        stack.pop();
        stack.push('#G3');
        stack.push('#INT');
    }
    if (number === 43) {
        stack.pop();
        stack.push('#G4');
        stack.push('#FLOAT');
    }
    if (number === 44) {
        stack.pop();
        stack.push('#G3');
        stack.push('"');
    }
    if (number === 45) {
        stack.pop();
        stack.push(';');
    }
    if (number === 46) {
        stack.pop();
        stack.push('\n');
    }
    if (number === 47) {
        stack.pop();
        stack.push('\n');
    }
    if (number === 48) {
        stack.pop();
        stack.push('\n');
        stack.push('#NAME');
    }
    if (number === 49) {
        stack.pop();
        stack.push('#I_');
        stack.push('#NAME');
        stack.push('#T');
    }
    if (number === 50) {
        stack.pop();
        stack.push('\n');
    }
    if (number === 51) {
        stack.pop();
        stack.push('\n');
        stack.push(')');
        stack.push('#L');
        stack.push('(');
    }
    if (number === 52) {
        stack.pop();
        stack.push('#L_');
        stack.push('#O');
        stack.push('#T');
        stack.push('#N');
    }
    if (number === 53) {
        stack.pop();
        stack.push('#L_');
        stack.push('#O');
        stack.push('#T');
    }
    if (number === 54) {
        stack.pop();
        stack.push('#L');
    }
    if (number === 55) {
        stack.pop();
    }
    if (number === 56) {
        stack.pop();
        stack.push('арг');
    }
    if (number === 57) {
        stack.pop();
        stack.push('рез');
    }
    if (number === 58) {
        stack.pop();
        stack.push('аргрез');
    }
    if (number === 59) {
        stack.pop();
        stack.push('#O_');
        stack.push('#NAME');
    }
    if (number === 60) {
        stack.pop();
        stack.push('#O');
        stack.push(',');
    }
    if (number === 61) {
        stack.pop();
    }
    if (number === 62) {
        stack.pop();
        stack.push('#R');
        stack.push('#body');
        stack.push('#P');
    }
    if (number === 63) {
        stack.pop();
        stack.push('#P_');
        stack.push('нач');
    }
    if (number === 64) {
        stack.pop();
        stack.push('\n');
    }
    if (number === 65) {
        stack.pop();
        stack.push(';');
    }
    if (number === 75) {
        stack.pop();
    }
    if (number === 66) {
        stack.pop();
        stack.push('#body');
    }
    if (number === 67) {
        stack.pop();
        stack.push('#R_');
        stack.push('кон');
    }
    if (number === 68) {
        stack.pop();
        stack.push('#R1_');
        stack.push('\n');
    }
    if (number === 69) {
        stack.pop();
        result = 1;
    }
    if (number === 70) {
        stack.pop();
        stack.push('#U_');
    }
    if (number === 71) {
        stack.pop();
        result = 1;
    }
    if (number === 72) {
        stack.pop();
        stack.push('#U');
    }
    if (number === 73) {
        stack.pop();
        result = 1;
    }
    if (number === 74) {
        stack.pop();
        stack.push('#U_');
        stack.push('\n');
    }
    
    if(number === 76){
        stack.pop();
        stack.push('#G4');
        stack.push('#INT');
    }
    if(number === 77){
        stack.pop();
        stack.push('#G4');
        stack.push('#CHAR');
    }
    if(number === 78){
        stack.pop();
        stack.push('#G4');
        stack.push('#STR');
    }
    
    if(number === 79){
        stack.pop();
        stack.push('#G4');
        stack.push('#BOOL');
    }
    if (number === 80) {
        // правило для сдвига, в случае, когда входящий элемент совпадает с последним элементом стэка!
        if (stack[stack.length - 1] === '_|')
        {
            stack.pop();
            result = 2;
        }
        else
        {

            stack.pop();
        }
    }
    if (number === 81) {

        stack.pop();
    }
    return result;
}

function praviloRobot(number, stack) {
    var result = 1;
    if (number === 0) {
        result = 0;
    }
    if (number === 1) {
        stack.pop();
        stack.push("#Q");
        stack.push("#SPACE");
        stack.push("влево");
    }
    if (number === 2) {
        stack.pop();
        stack.push("#Q");
        stack.push("#SPACE");
        stack.push("вправо");
    }
    if (number === 3) {
        stack.pop();
        stack.push("#Q");
        stack.push("#SPACE");
        stack.push("вверх");
    }
    if (number === 4) {
        stack.pop();
        stack.push("#Q");
        stack.push("#SPACE");
        stack.push("вниз");
    }
    if (number === 5) {
        stack.pop();
        stack.push("#Q");
        stack.push("#SPACE");
        stack.push("закрасить");
    }
    if (number === 6) {
        stack.pop();
        stack.push("#Q");
        stack.push("#SPACE");
        stack.push("все");
        stack.push("#CONT");
        stack.push("если");
    }
    if (number === 7) {
        stack.pop();
        stack.push("#Q1");
        stack.push("выбор");
    }
    if (number === 8) {
        stack.pop();
        result = 1;
    }
    if (number === 9) {
        stack.pop();
        stack.push("#Q2");
        stack.push("#LIST");
        stack.push("#SPACE");
    }
    if (number === 10) {
        stack.pop();
        stack.push("#Q2");
        stack.push("#LIST");
    }
    if (number === 11) {
        stack.pop();
        stack.push("#Q");
        stack.push("все");
        stack.push("#ELSE");
    }
    if (number === 12) {
        stack.pop();
        stack.push("#Q");
        stack.push("все");
    }
    if (number === 13) {
        stack.pop();
        stack.push("#CONT1");
        stack.push("слева");
    }
    if (number === 14) {
        stack.pop();
        stack.push("#CONT1");
        stack.push("справа");
    }
    if (number === 15) {
        stack.pop();
        stack.push("#CONT1");
        stack.push("сверху");
    }
    if (number === 16) {
        stack.pop();
        stack.push("#CONT1");
        stack.push("снизу");
    }
    if (number === 17) {
        stack.pop();
        stack.push("#CONT11");
        stack.push("клетка");
    }
    if (number === 18) {
        stack.pop();
        stack.push("#SP");
        stack.push("свободно");
    }
    if (number === 19) {
        stack.pop();
        stack.push("#SP");
        stack.push("стена");
    }
    if (number === 20) {
        stack.pop();
        stack.push("#SP");
        stack.push("закрашена");
    }
    if (number === 21) {
        stack.pop();
        stack.push("#SP");
        stack.push("чистая");
    }
    if (number === 22) {
        stack.pop();
        stack.push("#TH");
        stack.push("#SPACE");
    }
    if (number === 23) {
        stack.pop();
        stack.push("#TH");
    }
    if (number === 24) {
        stack.pop();
        stack.push("#TH1");
        stack.push("то");
    }
    if (number === 25) {
        stack.pop();
        stack.push("#TH11");
        stack.push("#CONT2");
        stack.push("#SPACE");
    }
    if (number === 26) {
        stack.pop();
        stack.push("#TH11");
        stack.push("#CONT2");
    }
    if (number === 27) {
        stack.pop();
        stack.push("#CONT22");
        stack.push("вправо");
    }
    if (number === 28) {
        stack.pop();
        stack.push("#CONT22");
        stack.push("влево");
    }
    if (number === 29) {
        stack.pop();
        stack.push("#CONT22");
        stack.push("вверх");
    }
    if (number === 30) {
        stack.pop();
        stack.push("#CONT22");
        stack.push("вниз");
    }
    if (number === 31) {
        stack.pop();
        stack.push("#CONT22");
        stack.push("закрасить");
    }
    if (number === 32) {
        stack.pop();
        stack.push("#CONT23");
        stack.push("#SPACE");
    }
    if (number === 33) {
        stack.pop();
        result = 1;
    }
    if (number === 34) {
        stack.pop();
        stack.push("#CONT2");
    }
    if (number === 35) {
        stack.pop();
        result = 1;
    }
    if (number === 36) {
        stack.pop();
        stack.push("#LIST1");
        stack.push(":");
        stack.push("#CONT3");
        stack.push("при");
    }
    if (number === 37) {
        stack.pop();
        stack.push("#LIST11");
        stack.push("#CONT4");
        stack.push("#SHIFT");
    }
    if (number === 38) {
        stack.pop();
        stack.push("#LIST11");
        stack.push("#CONT4");
    }
    if (number === 39) {
        stack.pop();
        stack.push("#LIST12");
        stack.push("#SPACE");
    }
    if (number === 40) {
        stack.pop();
        stack.push("#LIST");
    }
    if (number === 41) {
        stack.pop();
        result = 1;
    }
    if (number === 42) {
        stack.pop();
        stack.push("#LIST");
    }
    if (number === 43) {
        stack.pop();
        result = 1;
    }
    if (number === 44) {
        stack.pop();
        stack.push("#CONT33");
        stack.push("слева");
    }
    if (number === 45) {
        stack.pop();
        stack.push("#CONT33");
        stack.push("справа");
    }
    if (number === 46) {
        stack.pop();
        stack.push("#CONT33");
        stack.push("сверху");
    }
    if (number === 47) {
        stack.pop();
        stack.push("#CONT33");
        stack.push("снизу");
    }
    if (number === 48) {
        stack.pop();
        stack.push("#CONT34");
        stack.push("клетка");
    }
    if (number === 49) {
        stack.pop();
        stack.push("свободно");
    }
    if (number === 50) {
        stack.pop();
        stack.push("стена");

    }
    if (number === 51) {
        stack.pop();
        stack.push("закрашен");

    }
    if (number === 52) {
        stack.pop();
        stack.push("чистая");

    }
    if (number === 53) {
        stack.pop();
        stack.push("#SHIFT1");
        stack.push("\n");
    }
    if (number === 54) {
        stack.pop();
        stack.push("#SHIFT");
    }
    if (number === 55) {
        stack.pop();
        result = 1;
    }
    if (number === 56) {
        stack.pop();
        stack.push("#SPACE1");
        stack.push("\n");
    }
    if (number === 57) {
        stack.pop();
        stack.push("#SPACE1");
        stack.push(";");
    }
    if (number === 58) {
        stack.pop();
        stack.push("#SPACE1");
        stack.push("\n");
    }
    if (number === 59) {
        stack.pop();
        result = 1;
    }
    if (number === 60) {
        stack.pop();
        stack.push("вправо");
    }
    if (number === 61) {
        stack.pop();
        stack.push("влево");

    }
    if (number === 62) {
        stack.pop();
        stack.push("вверх");

    }
    if (number === 63) {
        stack.pop();
        stack.push("вниз");

    }
    if (number === 64) {
        stack.pop();
        stack.push("закрасить");
    }
    if (number === 65) {
        stack.pop();
        stack.push("#CONT45");
        stack.push("#SPACE");
    }
    if (number === 66) {
        stack.pop();
        result = 1;
    }
    if (number === 67) {
        stack.pop();
        stack.push("#CONT4");
    }
    if (number === 68) {
        stack.pop();
        result = 1;
    }
    if (number === 69) {
        stack.pop();
        stack.push("#CONT4");
        stack.push("иначе");
    }

    if (number === 70) {
        stack.push("#Q");
    }
    if (number === 71) {
        result = 1;
    }
    if(number === 72) {
        stack.pop();
        stack.push("#CONT2");
        stack.push("иначе");
    }
    if(number === 73) {
        stack.pop();
        result = 1;
    }
    if (number === 74) {
        // правило для сдвига, в случае, когда входящий элемент совпадает с последним элементом стэка!
        if (stack[stack.length - 1] === '_|')
        {
            stack.pop();
            result = 2;
        }
        else
        {
            stack.pop();
        }
    }

    return result;
}

function praviloBody(number, stack) {
    var result = 1;

    if (number === 0) {
        result = 0;            // состояние ошибка!
        return result;
    }
    if (number === 1) {
        stack.push("#R");
        stack.push("#T");
    }
    if (number === 2) {
        result = 1;
        return result;
    }
    if (number === 3) {
        stack.pop();
        stack.push("#T_");
        stack.push("#NAME");
    }
    if (number === 4) {
        stack.pop();
        stack.push("#B_");
        stack.push("#NAME");
        stack.push("#J");
    }
    if (number === 5) {
        stack.pop();
        stack.push("#K1");
        stack.push("ввод");
    }
    if (number === 6) {
        stack.pop();
        stack.push("#K1");
        stack.push("вывод");
    }
    if (number === 7) {
        stack.pop();
        stack.push("#F1");
        stack.push("то");
        stack.push("#IF");
        stack.push("если");
        return result;
    }
    if (number === 8) {
        stack.pop();
        stack.push("#E_");
        stack.push("#C");
        stack.push(":=");
    }
    if (number === 9) {
        stack.pop();
        stack.push("#EQ");
        stack.push("#S");
    }
    if (number === 10) {
        stack.pop();
        stack.push("#E_");
        stack.push("#C");
        stack.push(":=");
        stack.push("#NAME");
    }
    if (number === 11) {
        stack.pop();
        stack.push("#INT");
    }
    if (number === 12) {
        stack.pop();
        stack.push("#CHAR");
    }
    if (number === 13) {
        stack.pop();
        stack.push("#FLOAT");
    }
    if (number === 14) {
        stack.pop();
        stack.push("#STR");
    }
    if (number === 15) {
        stack.pop();
        stack.push("#BOOL");
    }
    if (number === 16) {
        stack.pop();
        stack.push("#E1");
        stack.push("#;");
    }
    if (number === 17) {
        stack.pop();
        stack.push("\n");
    }
    if (number === 18) {
        stack.pop();
        stack.push("#E");
    }
    if (number === 19) {
        stack.pop();
        stack.push("\n");
    }
    if (number === 20) {
        stack.pop();
        stack.push("#K1_");
        stack.push("#NAME");
    }
    if (number === 21) {
        stack.pop();
        stack.push("цел");
    }
    if (number === 22) {
        stack.pop();
        stack.push("вещ");
    }
    if (number === 23) {
        stack.pop();
        stack.push("лог");
    }
    if (number === 24) {
        stack.pop();
        stack.push("сим");
    }
    if (number === 25) {
        stack.pop();
        stack.push("лит");
    }
    if (number === 26) {
        stack.pop();
        stack.push("#B1");
        stack.push(",");
    }
    if (number === 27) {
        stack.pop();
        stack.push("#B2");
        stack.push(";");
    }
    if (number === 28) {
        stack.pop();
        stack.push("\n");
    }
    if (number === 29) {
        stack.pop();
        stack.push("#B_");
        stack.push("#NAME");
    }
    if (number === 30) {
        stack.pop();
        stack.push("#B_");
        stack.push("#NAME");
        stack.push("#J");
    }
    if (number === 31) {
        stack.pop();
        stack.push("#B_");
        stack.push("#NAME");
    }
    if (number === 32) {
        stack.pop();
        stack.push("#B_");
        stack.push("#NAME");
        stack.push("#J");
    }
    if (number === 33) {
        stack.pop();
        stack.push("\n");
    }
    if (number === 34) {
        stack.pop();
        stack.push("#K1");
        stack.push(",");
    }
    if (number === 35) {
        stack.pop();
        stack.push("\n");
    }
    if (number === 36) {
        stack.pop();
        stack.push("\n");
        stack.push(";");
    }
    if (number === 37) {
        stack.pop();
        stack.push("#K2_");
        stack.push(")");
        stack.push("#EQ");
        stack.push("(");
    }
    if (number === 38) {
        stack.pop();
        stack.push("#K2_");
        stack.push("#NAME");
    }
    if (number === 39) {
        stack.pop();
        stack.push("#K2");
        stack.push(",");
    }
    if (number === 40) {
        stack.pop();
        stack.push("\n");
    }
    if (number === 41) {
        stack.pop();
        stack.push("\n");
        stack.push(";");
    }
    if (number === 42) {
        stack.pop();
        stack.push("#EQ");
        stack.push("#S");
        stack.push("#EQ");
    }
    if (number === 43) {
        stack.pop();
        stack.push("#N");
    }   
    if (number === 44) {
        stack.pop();
        stack.push(")");
        stack.push("#D");
        stack.push("(");
    }
    if (number === 45) {
        stack.pop();
        stack.push("#C");
    }
    if (number === 46) {
        stack.pop();
        stack.push("#NAME");
    }
    if (number === 47) {
        stack.pop();
        stack.push("+");
    }
    if (number === 48) {
        stack.pop();
        stack.push("-");
    }
    if (number === 49) {
        stack.pop();
        stack.push("*");
    }
    if (number === 50) {
        stack.pop();
        stack.push("/");
    }
    if (number === 51) {
        stack.pop();
        stack.push("#F1");
        stack.push("#T");
        stack.push("иначе");
    }
    if (number === 52) {
        stack.pop();
        stack.push("#F1");
    }
    if (number === 53) {
        stack.pop();
        stack.push("\n");
    }
    if (number === 54) {
        stack.pop();
        stack.push("\n");
        stack.push(";");
    }
    if (number === 55) {
        stack.pop();
        stack.push("#IF_");
        stack.push("#EF");
    }
    if (number === 56) {
        stack.pop();
        stack.push("#EF");
        stack.push("и");
    }
    if (number === 57) {
        stack.pop();
        stack.push("#EF");
        stack.push("или");
    }
    if (number === 58) {
        stack.pop();
        result = 1;
    }
    if (number === 59) {
        stack.pop();
        stack.push("#N");
        stack.push("#SF");
        stack.push("#NAME");
    }
    if (number === 60) {
        stack.pop();
        stack.push(">");
    }
    if (number === 61) {
        stack.pop();
        stack.push("<");
    }
    if (number === 62) {
        stack.pop();
        stack.push(">=");
    }
    if (number === 63) {
        stack.pop();
        stack.push("<=");
    }
    if (number === 64) {
        stack.pop();
        stack.push("=");
    }
    if (number === 65) {
        // правило для сдвига, в случае, когда входящий элемент совпадает с последним элементом стэка!
        if (stack[stack.length - 1] === '_|')
        {
            stack.pop();
            result = 2;
        }
        else
        {
            stack.pop();
        }
    }

    return result;
}